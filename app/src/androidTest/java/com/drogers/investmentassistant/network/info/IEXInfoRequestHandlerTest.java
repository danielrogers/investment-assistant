package com.drogers.investmentassistant.network.info;

import android.content.Context;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.drogers.investmentassistant.DataHelper;
import com.drogers.investmentassistant.InvestmentAssistantApplication;
import com.drogers.investmentassistant.db.RoomDb;
import com.drogers.investmentassistant.db.entity.Asset;
import com.drogers.investmentassistant.db.entity.AssetDAO;
import com.drogers.investmentassistant.network.AssetParseException;

import org.apache.commons.lang3.NotImplementedException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.concurrent.TimeUnit;

import androidx.test.core.app.ApplicationProvider;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.UnicastSubject;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

@RunWith(MockitoJUnitRunner.class)
public class IEXInfoRequestHandlerTest extends IEXTestBase {

    private AssetDAO assetDAO;

    @Before
    public void doTestSetup(){
        super.doTestSetup();
        assetDAO = RoomDb.getDatabase(ApplicationProvider.getApplicationContext()).assetDAO();
    }

    @Test
    public void testMakeRequestAssetFound(){
        UnicastSubject<Asset> subject = new IEXInfoRequest(queue, apiKey, validTicker, dummyExchange, context).makeRequest();
        subject
                .subscribeOn(Schedulers.computation())
                .test()
                .awaitDone(2, TimeUnit.SECONDS)
                .assertNoErrors()
                .assertComplete();
        //DB will have been empty before this was called
        Asset a = assetDAO.getAnyAsset();
        assertNotNull(a);
        assertEquals("gwre", a.symbol);
    }

    @Test
    public void testMakeRequestAssetNotFound(){
        UnicastSubject<Asset> subject = new IEXInfoRequest(queue, apiKey, "akfhawieflhwufhaslefno", dummyExchange, context).makeRequest();
        subject
                .subscribeOn(Schedulers.computation())
                .test()
                .awaitDone(2, TimeUnit.SECONDS)
                .assertError(VolleyError.class);
        Asset a = assetDAO.getAnyAsset();
        assertNull(a);
    }

    @Test
    public void testMakeRequestInvalidAPIKey(){
        UnicastSubject<Asset> subject = new IEXInfoRequest(queue, "badAPIKey", validTicker, dummyExchange, context).makeRequest();
        subject
                .subscribeOn(Schedulers.computation())
                .test()
                .awaitDone(2, TimeUnit.SECONDS)
                .assertError(VolleyError.class);
        Asset a = assetDAO.getAnyAsset();
        assertNull(a);
    }

    @Test
    public void testMakeRequestInvalidSearchTerm(){
        UnicastSubject<Asset> subject = new IEXInfoRequest(queue, apiKey, "?!*", dummyExchange, context).makeRequest();
        subject
                .subscribeOn(Schedulers.computation())
                .test()
                .awaitDone(2, TimeUnit.SECONDS)
                .assertError(Exception.class)
                .assertError(throwable -> throwable.getMessage().equals("Request string is invalid!"));
        Asset a = assetDAO.getAnyAsset();
        assertNull(a);
    }

    @Test
    public void testMakeRequestJSONParsingFailure(){
        //need to query for a real asset, so that response parsing is invoked
        IEXInfoRequest handlerMock = spy(new IEXInfoRequest(queue, apiKey, validTicker, dummyExchange, context));
        doReturn(null).when(handlerMock).parseAssetFromResponse(anyString());
        UnicastSubject<Asset> subject = handlerMock.makeRequest();
        subject
                .subscribeOn(Schedulers.computation())
                .test()
                .awaitDone(2, TimeUnit.SECONDS)
                .assertError(AssetParseException.class);
        Asset a = assetDAO.getAnyAsset();
        assertNull(a);
    }

    @Test
    public void testMakeRequestNoConnection_manual(){
        //leaving this as a reminder to manually test it
        throw new NotImplementedException("Manually test me!");
    }
}
