package com.drogers.investmentassistant.network.info;

import android.content.Context;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.drogers.investmentassistant.DataHelper;
import com.drogers.investmentassistant.InvestmentAssistantApplication;
import com.drogers.investmentassistant.network.AssetParseException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

import androidx.test.core.app.ApplicationProvider;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.UnicastSubject;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

@RunWith(MockitoJUnitRunner.class)
public class IEXLatestClosingPriceRequestHandlerTest extends IEXTestBase{


    @Test
    public void testMakeRequestAssetFound(){
        BigDecimal result = new IEXLatestClosingPriceRequest(queue, apiKey, dummyExchange, validTicker).makeRequest();
        assertNotNull(result);
        assertTrue(result.compareTo(BigDecimal.ZERO) > 0);
    }

    @Test
    public void testMakeRequestAssetNotFound(){
        BigDecimal result = new IEXLatestClosingPriceRequest(queue, apiKey, dummyExchange, "akfhawieflhwufhaslefno").makeRequest();
        assertNull(result);
    }

    @Test
    public void testMakeRequestJSONParsingFailure(){
        //need to query for a real asset, so that response parsing is invoked
        IEXLatestClosingPriceRequest handlerMock = spy(new IEXLatestClosingPriceRequest(queue, apiKey, dummyExchange, validTicker));
        doReturn(null).when(handlerMock).parseResponse(anyString());
        BigDecimal result = handlerMock.makeRequest();
        assertNull(result);
    }
}
