package com.drogers.investmentassistant.network.info.exchanges;

import com.drogers.investmentassistant.DataHelper;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;
import com.drogers.investmentassistant.network.info.IEXTestBase;

import org.junit.Test;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import io.reactivex.subjects.UnicastSubject;

import static junit.framework.TestCase.assertNotNull;

public class IEXSupportedExchangesRequestTest extends IEXTestBase {
    @Test
    public void testSimpleRequest(){
        UnicastSubject<List<Exchange>> result = new IEXSupportedExchangesRequest(queue, apiKey, context).makeRequest();
        result
                .test()
                .awaitDone(5, TimeUnit.SECONDS)
                .assertNoErrors()
                .assertComplete()
                .assertValue(exchanges -> exchanges != null && !exchanges.isEmpty());
        assertNotNull(new DataHelper().getRoomDb().exchangeDAO().getAnyExchange());
    }

    @Test
    public void testRequestBadApiKey(){
        UnicastSubject<List<Exchange>> result = new IEXSupportedExchangesRequest(queue, "abcdef", context).makeRequest();
        result
                .test()
                .awaitDone(5, TimeUnit.SECONDS)
                .assertError(Objects::nonNull);
    }
}
