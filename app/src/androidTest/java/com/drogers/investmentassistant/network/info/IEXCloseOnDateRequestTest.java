package com.drogers.investmentassistant.network.info;

import android.content.Context;

import com.android.volley.toolbox.Volley;
import com.drogers.investmentassistant.DataHelper;
import com.drogers.investmentassistant.InvestmentAssistantApplication;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.LocalDate;

import androidx.test.core.app.ApplicationProvider;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

@RunWith(MockitoJUnitRunner.class)
public class IEXCloseOnDateRequestTest extends IEXTestBase {
    private LocalDate requestDate;

    @Override
    @Before
    public void doTestSetup(){
        super.doTestSetup();
        requestDate = LocalDate.of(2019, 1, 22);
    }


    @Test
    public void testWeekendCall(){
        //below date is a Sunday
        requestDate = LocalDate.of(2019, 4, 14);
        BigDecimal result = new IEXCloseOnDateRequest(queue, apiKey, validTicker, dummyExchange, requestDate).makeRequest();
        assertNotNull(result);
    }

    @Test
    public void testPublicHolidayCall(){
        //below date is Easter Sunday, should track back to Thursday
        requestDate = LocalDate.of(2019, 4, 21);
        BigDecimal result = new IEXCloseOnDateRequest(queue, apiKey, validTicker, dummyExchange, requestDate).makeRequest();
        assertNotNull(result);
    }

    @Test
    public void testMakeRequestAssetFound(){
        BigDecimal result = new IEXCloseOnDateRequest(queue, apiKey, validTicker, dummyExchange, requestDate).makeRequest();
        assertNotNull(result);
    }

    @Test
    public void testMakeRequestAssetNotFound(){
        BigDecimal result = new IEXCloseOnDateRequest(queue, apiKey, "akfhawieflhwufhaslefno", dummyExchange, requestDate).makeRequest();
        assertNull(result);
    }

    //not an entirely useful test right now
    @Test
    public void testMakeRequestJSONParsingFailure(){
        //need to query for a real asset, so that response parsing is invoked
        IEXCloseOnDateRequest handlerMock = spy(new IEXCloseOnDateRequest(queue, apiKey, validTicker, dummyExchange, requestDate));
        doReturn(null).when(handlerMock).parseResponse(anyString());
        BigDecimal result = handlerMock.makeRequest();
        assertNull(result);
    }
}
