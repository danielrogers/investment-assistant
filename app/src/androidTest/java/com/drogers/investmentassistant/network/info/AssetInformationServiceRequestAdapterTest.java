package com.drogers.investmentassistant.network.info;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

public class AssetInformationServiceRequestAdapterTest {

    @Test
    public void isRequestStringValid_PositiveTest(){
        assertTrue(AssetInformationServiceRequestAdapter.isRequestStringValid("123Abc"));
    }

    @Test
    public void isRequestStringValid_NegativeTest_Symbol(){
        assertFalse(AssetInformationServiceRequestAdapter.isRequestStringValid("!"));
    }

    @Test
    public void isRequestStringValid_NegativeTest_EmptyWhitespace(){
        assertFalse(AssetInformationServiceRequestAdapter.isRequestStringValid(" "));
    }

    @Test
    public void isRequestStringValid_NegativeTest_Empty(){
        assertFalse(AssetInformationServiceRequestAdapter.isRequestStringValid(""));
    }

    @Test
    public void isRequestStringValid_NegativeTest_Whitespace(){
        assertFalse(AssetInformationServiceRequestAdapter.isRequestStringValid("abc 123"));
    }}
