package com.drogers.investmentassistant.network.info;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.drogers.investmentassistant.DataHelper;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;

import org.junit.Before;

import androidx.annotation.CallSuper;
import androidx.test.core.app.ApplicationProvider;

public class IEXTestBase {
    String validTicker;
    protected Context context;
    protected String apiKey;
    protected RequestQueue queue;
    protected Exchange dummyExchange;

    @CallSuper
    @Before
    public void doTestSetup() {
        context = ApplicationProvider.getApplicationContext();
        DataHelper dataHelper = new DataHelper();
        dataHelper.clearDb();
        dummyExchange = dataHelper.writeDummyNYSEExchange();
        RequestHandlerProvider.requestHandlerToUse = RequestHandlerType.IEX_SANDBOX;
        validTicker = "gwre";
        apiKey = IEXRequestHandler.readAPIKey(context);
        queue = Volley.newRequestQueue(context);
    }
}
