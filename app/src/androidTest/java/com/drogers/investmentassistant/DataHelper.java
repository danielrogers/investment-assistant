package com.drogers.investmentassistant;

import android.app.Application;

import com.drogers.investmentassistant.db.RoomDb;
import com.drogers.investmentassistant.db.entity.Asset;
import com.drogers.investmentassistant.db.entity.AssetOwnershipRecord;
import com.drogers.investmentassistant.db.entity.AssetOwnershipRecordRepository;
import com.drogers.investmentassistant.db.entity.AssetRepository;
import com.drogers.investmentassistant.db.entity.AssetType;
import com.drogers.investmentassistant.db.entity.transaction.Purchase;
import com.drogers.investmentassistant.db.entity.transaction.Transaction;
import com.drogers.investmentassistant.db.entity.transaction.PurchaseRepository;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;
import com.drogers.investmentassistant.network.info.DummyRequestHandler;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import androidx.test.core.app.ApplicationProvider;

public class DataHelper {

    public static final BigDecimal PURCHASE_PRICE = new BigDecimal("20.0");
    public static final BigDecimal AMOUNT_OWNED = new BigDecimal("5.0");
    private RoomDb roomDb;
    private AssetOwnershipRecordRepository assetOwnershipRecordRepository;
    private AssetRepository assetRepository;
    private PurchaseRepository purchaseRepository;

    public DataHelper(){
        Application application = ApplicationProvider.getApplicationContext();
        roomDb = RoomDb.getDatabase(application);
        assetOwnershipRecordRepository = new AssetOwnershipRecordRepository(roomDb.assetOwnershipRecordDAO());
        assetRepository = new AssetRepository(ApplicationProvider.getApplicationContext(), roomDb.assetDAO());
        purchaseRepository = new PurchaseRepository(roomDb.purchaseDAO());
    }

    public RoomDb getRoomDb(){
        return roomDb;
    }

    public void clearDb(){
        roomDb.clearAllTables();
    }

    /**
     * Will have suffix set to null
     * @return Committed exchange
     */
    public Exchange writeDummyNYSEExchange(){
        //this is in the handler as a dirty workaround to test package visibility.  Ideally DummyRequestHandler would be in the test package
        Exchange exchange = DummyRequestHandler.buildDummyNYSEExchange();
        roomDb.exchangeDAO().insert(exchange).subscribe();
        return exchange;
    }


    public Exchange writeDummyDublinExchange(){
        Exchange exchange = DummyRequestHandler.buildDummyDublinExchange();
        roomDb.exchangeDAO().insert(exchange).subscribe();
        return exchange;
    }

    /**
     * Writes two sample assets (GWRE & APPL) with UID's 1 and 2
     * @return Assets
     */
    public List<Asset> writeTwoDummyAssets(Exchange exchange){
        Asset a1 = getDummyLocalGuidewireAsset(exchange, 1);
        Asset a2 = getDummyLocalAppleAsset(exchange, 2);
        assetRepository
                .insert(a1, a2)
                .subscribe();
        ArrayList<Asset> returnList = new ArrayList<>();
        returnList.add(a1);
        returnList.add(a2);
        return returnList;
    }

    /**
     * Writes an asset which the DummyRequestHandler uses to throw exceptions. Will have uid of 3
     * @return
     */
    public Asset writeErrorAsset(Exchange exchange){
        Asset a1 = new Asset("Error Asset", AssetType.STOCK,"A company", DummyRequestHandler.ERROR_TICKER, exchange.marketIdentifierCode);
        a1.uid = 3;
        assetRepository
                .insert(a1)
                .subscribe();
        return a1;
    }

    /**
     * Writes an asset which has it's isUserDefinedAsset flag set to true. Will have uid of 4
     * @return
     */
    public Asset writeUserDefinedAsset(Exchange exchange){
        Asset a1 = new Asset("Custom Asset", AssetType.STOCK,"A custom company", "CSTM", exchange.marketIdentifierCode);
        a1.uid = 4;
        a1.isUserDefinedAsset = true;
        assetRepository
                .insert(a1)
                .subscribe();
        return a1;
    }

    private Asset getDummyLocalGuidewireAsset(Exchange exchange, int uid){
        Asset a = new Asset("Guidewire", AssetType.STOCK,"LOCAL Guidewire", "GWRE", exchange.marketIdentifierCode);
        a.uid = uid;
        return a;
    }

    private Asset getDummyLocalAppleAsset(Exchange exchange, int uid){
        Asset a = new Asset("Apple", AssetType.STOCK,"LOCAL fruit company", "AAPL", exchange.marketIdentifierCode);
        a.uid = uid;
        return a;
    }

    /**
     * Writes one sample asset (GWRE). It's description will contain the text LOCAL
     * @return Asset
     */
    public Asset writeOneDummyAsset(Exchange exchange, int uid){
        Asset a1 = getDummyLocalGuidewireAsset(exchange, uid);
        assetRepository
                .insert(a1)
                .subscribe();
        return a1;
    }

    /**
     * Generates a record with quantity AMOUNT_OWNED at price PURCHASE_PRICE
     * @param date
     * @param asset
     * @return
     */
    public Purchase generatePurchaseRecord(LocalDate date, Asset asset){
        Purchase purchase = new Purchase();
        purchase.assetUid = asset.uid;
        purchase.date = date;
        purchase.totalQuantity = AMOUNT_OWNED;
        purchase.assetPrice = PURCHASE_PRICE;
        purchase.remainingActiveUnits = AMOUNT_OWNED;
        return purchase;
    }

    /**
     * See {@link DataHelper#writeAORecordsForSingleAssetDailyFrom(LocalDate, Asset, BigDecimal, BigDecimal)}
     */
    public void writeAORecordsForSingleAssetDailyFrom(LocalDate fromDate, Asset asset, BigDecimal purchasePrice, BigDecimal amountOwned){

        ArrayList<AssetOwnershipRecord> recordsToCommit = new ArrayList<>();

        AssetOwnershipRecord record = new AssetOwnershipRecord(asset.uid, purchasePrice, amountOwned, fromDate, false);
        recordsToCommit.add(record);
        fromDate = fromDate.plusDays(1);
        while(fromDate.isBefore(LocalDate.now())){
            recordsToCommit.add(new AssetOwnershipRecord(asset.uid, purchasePrice, amountOwned, fromDate, true));
            fromDate = fromDate.plusDays(1);
        }
        asset.liveQuantity = asset.liveQuantity.add(AMOUNT_OWNED);
        assetRepository.update_synchronous(asset);
        assetOwnershipRecordRepository.upsert(recordsToCommit);
    }

    /**
     * Writes a series of AORecords every day from fromDate until today.  The first record will be
     * an intra-day record.  The price will be fixed at PURCHASE_PRICE, and the quantity will be AMOUNT_OWNED.
     * Will also increment the liveQuantity field of the Asset being purchased
     * @param fromDate
     * @param asset
     */
    public void writeAORecordsForSingleAssetDailyFrom(LocalDate fromDate, Asset asset){
        writeAORecordsForSingleAssetDailyFrom(fromDate, asset, PURCHASE_PRICE, AMOUNT_OWNED);
    }

    /**
     * See {@link DataHelper#writeAORecordsForSingleAssetWeeklyFrom(LocalDate, Asset, BigDecimal, BigDecimal)}
     */
    public void writeAORecordsForSingleAssetWeeklyFrom(LocalDate fromDate, Asset asset){
        writeAORecordsForSingleAssetWeeklyFrom(fromDate, asset, PURCHASE_PRICE, AMOUNT_OWNED);
    }

    /**
     * Writes a series of AORecords every week from fromDate until today (not inclusive of today).  The first record will be
     * an intra-day record, and will always be written (regardless of any edge cases around fromDate).
     * The price will be fixed at PURCHASE_PRICE, and the quantity will be AMOUNT_OWNED
     * @param fromDate
     * @param asset
     */
    public void writeAORecordsForSingleAssetWeeklyFrom(LocalDate fromDate, Asset asset, BigDecimal purchasePrice, BigDecimal amountOwned){
        ArrayList<AssetOwnershipRecord> recordsToCommit = new ArrayList<>();
        AssetOwnershipRecord record = new AssetOwnershipRecord(asset.uid, purchasePrice, amountOwned, fromDate, false);
        recordsToCommit.add(record);
        fromDate = fromDate.plusWeeks(1);
        while(fromDate.isBefore(LocalDate.now())){
            recordsToCommit.add(new AssetOwnershipRecord(asset.uid, purchasePrice, amountOwned, fromDate, true));
            fromDate = fromDate.plusWeeks(1);
        }
        assetOwnershipRecordRepository.upsert(recordsToCommit);
    }

    /**
     * Writes a series of AORecords every month from fromDate until today.  The first record will be
     * an intra-day record.  The price will be fixed at PURCHASE_PRICE, and the quantity will be AMOUNT_OWNED
     * @param fromDate
     * @param asset
     */
    public void writeAORecordsForSingleAssetMonthlyFrom(LocalDate fromDate, Asset asset){
        ArrayList<AssetOwnershipRecord> recordsToCommit = new ArrayList<>();
        AssetOwnershipRecord record = new AssetOwnershipRecord(asset.uid, PURCHASE_PRICE, AMOUNT_OWNED, fromDate, false);
        recordsToCommit.add(record);
        fromDate = fromDate.plusMonths(1);
        while(fromDate.isBefore(LocalDate.now())){
            recordsToCommit.add(new AssetOwnershipRecord(asset.uid, PURCHASE_PRICE, AMOUNT_OWNED, fromDate, true));
            fromDate = fromDate.plusMonths(1);
        }
        assetOwnershipRecordRepository.upsert(recordsToCommit);
    }

}
