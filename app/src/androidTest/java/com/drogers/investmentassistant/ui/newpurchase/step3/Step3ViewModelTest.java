package com.drogers.investmentassistant.ui.newpurchase.step3;

import android.util.Pair;

import com.drogers.investmentassistant.DataHelper;
import com.drogers.investmentassistant.InvestmentAssistantApplication;
import com.drogers.investmentassistant.MainActivity;
import com.drogers.investmentassistant.db.RoomDb;
import com.drogers.investmentassistant.db.entity.Asset;
import com.drogers.investmentassistant.db.entity.AssetDAO;
import com.drogers.investmentassistant.db.entity.AssetOwnershipRecord;
import com.drogers.investmentassistant.db.entity.AssetOwnershipRecordDAO;
import com.drogers.investmentassistant.db.entity.transaction.Purchase;
import com.drogers.investmentassistant.network.info.RequestHandlerProvider;
import com.drogers.investmentassistant.network.info.RequestHandlerType;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.lifecycle.ViewModelProviders;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import io.reactivex.subjects.UnicastSubject;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@LargeTest
public class Step3ViewModelTest {
    private Step3ViewModel viewModel;
    private AssetDAO assetDAO;
    private AssetOwnershipRecordDAO assetOwnershipRecordDAO;
    private Purchase purchaseRecord;
    private DataHelper dataHelper;
    private Asset dummyGWREAsset;
    private Asset dummyAAPLAsset;

    @Rule
    public ActivityTestRule<MainActivity> activityRule =
            new ActivityTestRule<>(MainActivity.class, false, true);

    @Before
    public void doTestSetup(){
        InvestmentAssistantApplication application = ApplicationProvider.getApplicationContext();
        RequestHandlerProvider.requestHandlerToUse = RequestHandlerType.IEX_SANDBOX;
        RoomDb db = RoomDb.getDatabase(application);
        assetDAO = db.assetDAO();
        assetOwnershipRecordDAO = db.assetOwnershipRecordDAO();
        generateTestData();
    }

    private void generateTestData(){
        dataHelper = new DataHelper();
        dataHelper.clearDb();
        List<Asset> assets = dataHelper.writeTwoDummyAssets(dataHelper.writeDummyDublinExchange());
        dummyGWREAsset = assets.get(0);
        dummyAAPLAsset = assets.get(1);
        initSingleAssetPurchase();
    }

    private void initSingleAssetPurchase(){
        //gwre
        Asset a = assetDAO.getAssetExchange(1).asset;
        assertNotNull(a);
        purchaseRecord = dataHelper.generatePurchaseRecord(null, a);
        viewModel =  ViewModelProviders.of(activityRule.getActivity()).get(Step3ViewModel.class);
        viewModel.initPurchase(purchaseRecord, a);
    }

    /**
     * Tests that getAllRecordsToUpsertForDates generates only one AORecord
     * when passed a list of dates containing only today
     */
    @Test
    public void testRecordsToUpsert_NoPreviousRecords_Today(){
        ArrayList<LocalDate> dates = new ArrayList<>();
        LocalDate today = LocalDate.now();
        dates.add(today);
        viewModel.progressSubject = UnicastSubject.create();
        List<AssetOwnershipRecord> assetOwnershipRecordList = viewModel.getAllRecordsToUpsertForDates(dates);
        assertEquals(1, assetOwnershipRecordList.size());
        AssetOwnershipRecord generatedRecord = assetOwnershipRecordList.get(0);
        assertFalse(generatedRecord.priceRepresentsClosingValue);
        assertEquals(generatedRecord.amountOwned, purchaseRecord.remainingActiveUnits);
        assertTrue(generatedRecord.asOfDate.isEqual(today));
    }

    /**
     * Tests that getAllRecordsToUpsertForDates only generates one AORecord
     * when passed a list of dates containing only today, and there are other
     * AORecords for today
     */
    @Test
    public void testRecordsToUpsert_WithOtherRecords_Today(){
        ArrayList<LocalDate> dates = new ArrayList<>();
        LocalDate today = LocalDate.now();
        dates.add(today);
        viewModel.progressSubject = UnicastSubject.create();

        AssetOwnershipRecord preexistingAssetOwnershipRecord = new AssetOwnershipRecord(1, DataHelper.PURCHASE_PRICE, DataHelper.AMOUNT_OWNED, today, false);
        assetOwnershipRecordDAO.upsert(preexistingAssetOwnershipRecord);

        List<AssetOwnershipRecord> assetOwnershipRecordList = viewModel.getAllRecordsToUpsertForDates(dates);
        assertEquals(1, assetOwnershipRecordList.size());
        AssetOwnershipRecord generatedRecord = assetOwnershipRecordList.get(0);
        assertFalse(generatedRecord.priceRepresentsClosingValue);
        assertEquals(generatedRecord.amountOwned, purchaseRecord.remainingActiveUnits);
        assertTrue(generatedRecord.asOfDate.isEqual(today));
    }

    /**
     * Tests that getAssetDetailMappings correctly maps a given list of AORecords
     */
    @Test
    public void testGetAssetDetailMappings(){
        dataHelper.writeAORecordsForSingleAssetDailyFrom(LocalDate.now().plusWeeks(-1), dummyAAPLAsset);
        dataHelper.writeAORecordsForSingleAssetDailyFrom(LocalDate.now().plusWeeks(-1), dummyGWREAsset);//todo investigate why graph flatlines when this test is ran
        List<AssetOwnershipRecord> records = dataHelper.getRoomDb().assetOwnershipRecordDAO().getAllAORecords();
        HashMap<Long, Pair<BigDecimal, BigDecimal>> results =  viewModel.getAssetDetailMappings(records);
        assertEquals(2, results.size());

        Pair<BigDecimal, BigDecimal> firstResultSet = results.get(1L);
        //verify the amount
        assertEquals(DataHelper.AMOUNT_OWNED.multiply(new BigDecimal(7)), firstResultSet.first);
        //verify the price
        assertEquals(DataHelper.PURCHASE_PRICE, firstResultSet.second);

    }

}

