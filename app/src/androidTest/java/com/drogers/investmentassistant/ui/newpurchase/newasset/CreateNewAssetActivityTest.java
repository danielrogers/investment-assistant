package com.drogers.investmentassistant.ui.newpurchase.newasset;

import android.app.Activity;
import android.content.Intent;

import com.drogers.investmentassistant.DataHelper;
import com.drogers.investmentassistant.R;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;
import com.drogers.investmentassistant.ui.newpurchase.NewPurchaseKeys;
import com.drogers.investmentassistant.ui.newpurchase.TestingUtils;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.clearText;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static junit.framework.TestCase.assertTrue;

@LargeTest
public class CreateNewAssetActivityTest {

    private static String assetSymbol = "assetSymbol";
    private static String assetName = "assetName";
    private static String assetDescription = "assetDescription";
    private static Exchange dummyExchange;
    @Rule
    public ActivityTestRule<CreateNewAssetActivity> activityRule =
            new ActivityTestRule<>(CreateNewAssetActivity.class, false, false);

    @Before
    public void doSetup() {
        DataHelper dataHelper = new DataHelper();
        dataHelper.clearDb();
        dummyExchange = dataHelper.writeDummyDublinExchange();
    }

    private Intent buildIntent(){
        Intent i = new Intent();
        i.putExtra(NewPurchaseKeys.CREATE_NEW_ASSET_TICKER_KEY, assetSymbol);
        i.putExtra(NewPurchaseKeys.CREATE_NEW_ASSET_EXCHANGE_KEY, dummyExchange);
        return i;
    }

    @Test
    public void testSymbolFieldPrefills() {
        activityRule.launchActivity(buildIntent());
        onView(withText(assetSymbol)).check(matches(isDisplayed()));
        onView(withText(dummyExchange.name)).check(matches(isDisplayed()));
    }

    @Test
    public void testExchangeInfoShown(){
        activityRule.launchActivity(buildIntent());
        onView(withText(dummyExchange.name)).check(matches(isDisplayed()));
    }

    @Test
    public void testWarningToastShows_blankName() {
        TestingUtils.waitForToastToClear();
        activityRule.launchActivity(buildIntent());
        Activity activity = activityRule.getActivity();
        onView(withId(R.id.new_asset_save)).perform(click());
        TestingUtils.verifyToastDisplayed(activity, activity.getString(R.string.create_new_asset_blank_name));
    }

    @Test
    public void testWarningToastShows_blankDescription() {
        TestingUtils.waitForToastToClear();
        activityRule.launchActivity(buildIntent());
        Activity activity = activityRule.getActivity();
        onView(withId(R.id.new_asset_name_input)).perform(typeText("f"), closeSoftKeyboard());
        onView(withId(R.id.new_asset_save)).perform(click());
        TestingUtils.verifyToastDisplayed(activity, activity.getString(R.string.create_new_asset_blank_description));
    }

    @Test
    public void testWarningToastShows_blankSymbol() {
        TestingUtils.waitForToastToClear();
        activityRule.launchActivity(buildIntent());
        Activity activity = activityRule.getActivity();
        onView(withId(R.id.new_asset_name_input)).perform(typeText("f"), closeSoftKeyboard());
        onView(withId(R.id.new_asset_symbol_input)).perform(clearText(), closeSoftKeyboard());
        onView(withId(R.id.new_asset_save)).perform(click());
        TestingUtils.verifyToastDisplayed(activity, activity.getString(R.string.create_new_asset_blank_symbol));
    }

    @Test
    public void testAssetSavesCorrectly() {
        activityRule.launchActivity(buildIntent());
        onView(withId(R.id.new_asset_name_input)).perform(typeText(assetName), closeSoftKeyboard());
        onView(withId(R.id.new_asset_description_input)).perform(typeText(assetDescription), closeSoftKeyboard());
        onView(withId(R.id.new_asset_save)).perform(click());
        onView(withText("Recent Assets:")).check(matches(isDisplayed()));
        onView(withText(assetName)).check(matches(isDisplayed()));
        onView(withText(assetDescription)).check(matches(isDisplayed()));
        assertTrue(new DataHelper()
                .getRoomDb()
                .assetDAO()
                .getAssetOnExchange_synchronous(assetSymbol, dummyExchange.marketIdentifierCode)
                .isUserDefinedAsset);
    }
}
