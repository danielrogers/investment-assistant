package com.drogers.investmentassistant.ui.newpurchase.step1;

import android.content.Intent;

import com.drogers.investmentassistant.DataHelper;
import com.drogers.investmentassistant.R;
import com.drogers.investmentassistant.db.entity.Asset;
import com.drogers.investmentassistant.db.entity.AssetDAO;
import com.drogers.investmentassistant.db.entity.AssetType;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.time.LocalDate;

import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import static androidx.test.espresso.Espresso.*;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.doesNotExist;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.*;

@LargeTest
public class NewPurchaseActivityStep1Test {

    private AssetDAO assetDAO;
    Exchange dummyExchange;
    @Rule
    public ActivityTestRule<NewPurchaseActivityStep1> activityRule =
            new ActivityTestRule<>(NewPurchaseActivityStep1.class, false, false);

    @Before
    public void prepareDatabase() {
        DataHelper dataHelper= new DataHelper();
        dataHelper.clearDb();
        assetDAO = dataHelper.getRoomDb().assetDAO();
        dummyExchange = dataHelper.writeDummyNYSEExchange();
    }

    @Test
    public void testRecentAssetsEmpty() {
        activityRule.launchActivity(new Intent());
        onView(withText("Recent Assets:")).check(matches(isDisplayed()));
        onView(withText(R.string.new_purchase_step_1_no_recent_assets)).check(matches(isDisplayed()));
    }

    @Test
    public void testRecentAssetsDisplaysValues() {
        String name1 = "Dummy1";
        String name2 = "Dummy2";
        String description1 = "description1";
        String description2 = "description2";
        Asset dummyAsset1 = new Asset(name1, AssetType.STOCK, description1, "dmy", dummyExchange.marketIdentifierCode, LocalDate.now());
        Asset dummyAsset2 = new Asset(name2, AssetType.STOCK, description2, "dmy2", dummyExchange.marketIdentifierCode, LocalDate.now());
        assetDAO.insert(dummyAsset1, dummyAsset2).subscribe();

        activityRule.launchActivity(new Intent());
        onView(withText(name1)).check(matches(isDisplayed()));
        onView(withText(name2)).check(matches(isDisplayed()));
        onView(withText(description1)).check(matches(isDisplayed()));
        onView(withText(description2)).check(matches(isDisplayed()));
        onView(withText(R.string.new_purchase_step_1_no_recent_assets)).check(doesNotExist());
    }

    @Test
    public void testActivityTransitionsCorrectly() {
        String name = "Dummy1";
        String description1 = "description2";

        Asset dummyAsset1 = new Asset(name, AssetType.STOCK, description1, "dmy", dummyExchange.marketIdentifierCode, LocalDate.now());
        assetDAO.insert(dummyAsset1).subscribe();

        activityRule.launchActivity(new Intent());
        onView(withText(name)).check(matches(isDisplayed()));
        onView(withText(name)).perform(click());
        onView(withText("Purchasing: " + name));

    }
}