package com.drogers.investmentassistant.ui.newpurchase.step3;

import android.content.Intent;

import com.drogers.investmentassistant.DataHelper;
import com.drogers.investmentassistant.MainActivity;
import com.drogers.investmentassistant.R;
import com.drogers.investmentassistant.db.entity.Asset;
import com.drogers.investmentassistant.db.entity.transaction.Purchase;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;
import com.drogers.investmentassistant.network.info.RequestHandlerProvider;
import com.drogers.investmentassistant.network.info.RequestHandlerType;
import com.drogers.investmentassistant.ui.newpurchase.NewPurchaseKeys;
import com.drogers.investmentassistant.ui.newpurchase.TestingUtils;

import org.apache.commons.lang3.NotImplementedException;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.espresso.IdlingRegistry;
import androidx.test.espresso.idling.CountingIdlingResource;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.pressBack;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isEnabled;
import static androidx.test.espresso.matcher.ViewMatchers.isRoot;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.core.IsNot.not;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

@LargeTest
@RunWith(MockitoJUnitRunner.class)
public class NewPurchaseActivityStep3Test {

    private Asset dummyGWREAsset;
    private Asset dummyAAPLAsset;
    DataHelper dataHelper;
    Exchange dummyDublinExchange;

    @Rule
    public ActivityTestRule<NewPurchaseActivityStep3> activityRule =
            new ActivityTestRule<>(NewPurchaseActivityStep3.class, false, false);

    @Before
    public void doTestSetup() {
        RequestHandlerProvider.requestHandlerToUse = RequestHandlerType.IEX_SANDBOX;
        dataHelper = new DataHelper();
        dataHelper.clearDb();
        dummyDublinExchange = dataHelper.writeDummyDublinExchange();
        List<Asset> dummyAssets = dataHelper.writeTwoDummyAssets(dummyDublinExchange);
        dummyGWREAsset = dummyAssets.get(0);
        dummyAAPLAsset = dummyAssets.get(1);
    }

    @After
    public void doAfter(){
        NewPurchaseActivityStep3 a = activityRule.getActivity();
        if(a != null) {
            CountingIdlingResource componentIdlingResource = a.getIdlingResource();
            IdlingRegistry.getInstance().unregister(componentIdlingResource);
        }
    }

    /**
     * Tests how a purchase backdated to a year ago for a pre-existing asset performs
     */
    @Test
    public void testOneYearUpdatePreExistingAsset(){
        dataHelper.writeAORecordsForSingleAssetDailyFrom(LocalDate.now().plusYears(-1), dummyGWREAsset);

        Purchase purchaseRecord = dataHelper.generatePurchaseRecord(LocalDate.now().plusYears(-1), dummyGWREAsset);
        Intent intent = new Intent(ApplicationProvider.getApplicationContext(), NewPurchaseActivityStep3.class);
        intent.putExtra(NewPurchaseKeys.STEP_3_PURCHASE_EXTRA_KEY, purchaseRecord);
        intent.putExtra(NewPurchaseKeys.STEP_3_ASSET_EXTRA_KEY, dummyGWREAsset);

        activityRule.launchActivity(intent);
        onView(withText("Finish")).check(matches(not(isEnabled())));
        CountingIdlingResource componentIdlingResource = activityRule.getActivity().getIdlingResource();
        IdlingRegistry.getInstance().register(componentIdlingResource);

        //this view shouldn't be seen until the update has finished, which is linked to the
        //idling resource
        onView(withText("Completed!")).check(matches(isDisplayed()));
        onView(withText("Finish")).check(matches(isEnabled()));
        //uid of GWRE is 1, when created from DataHelper. Would expect quantity of 10
        BigDecimal liveQuantity = dataHelper.getRoomDb().assetDAO().getAssetExchange(1).asset.liveQuantity;
        assertEquals(new BigDecimal("10.0"), liveQuantity);
    }

    /**
     * Tests how a purchase backdated to a year ago for a new asset performs
     * In order to test this, a one year backdated series of weekly APPL AORecords is written.  A Transaction for GWRE
     * is then written, also for one year ago.
     * This test should be watched on the emulator to make sure it's "ok" - not providing any target KPIs right now.
     */
    @Test
    public void testOneYearUpdateNewAsset(){
        //write backdated AAPL info for the last year
        dataHelper.writeAORecordsForSingleAssetWeeklyFrom(LocalDate.now().plusYears(-1), dummyAAPLAsset);

        Purchase purchaseRecord = dataHelper.generatePurchaseRecord(LocalDate.now().plusYears(-1), dummyGWREAsset);
        Intent intent = new Intent(ApplicationProvider.getApplicationContext(), NewPurchaseActivityStep3.class);
        intent.putExtra(NewPurchaseKeys.STEP_3_PURCHASE_EXTRA_KEY, purchaseRecord);
        intent.putExtra(NewPurchaseKeys.STEP_3_ASSET_EXTRA_KEY, dummyGWREAsset);

        activityRule.launchActivity(intent);
        CountingIdlingResource componentIdlingResource = activityRule.getActivity().getIdlingResource();
        IdlingRegistry.getInstance().register(componentIdlingResource);

        onView(withText("Completed!")).check(matches(isDisplayed()));
        onView(withText("Finish")).check(matches(isEnabled()));
        //uid of GWRE is 1, when created from DataHelper. Would expect quantity of 5
        BigDecimal liveQuantity = dataHelper.getRoomDb().assetDAO().getAssetExchange(1).asset.liveQuantity;
        assertEquals(new BigDecimal("5.0"), liveQuantity);
    }

    /**
     * Tests that the back button is blocked during the update process
     */
    @Test
    public void testBackButtonBlockedDuringUpdate(){
        //using 10 years ago to give the test time to execute, and the dummy service to avoid spamming the sandbox api and using up requests
        RequestHandlerProvider.requestHandlerToUse = RequestHandlerType.DUMMY_SERVICE;
        dataHelper.writeAORecordsForSingleAssetDailyFrom(LocalDate.now().plusYears(-10), dummyGWREAsset);

        Purchase purchaseRecord = dataHelper.generatePurchaseRecord(LocalDate.now().plusYears(-10), dummyGWREAsset);
        Intent intent = new Intent(ApplicationProvider.getApplicationContext(), NewPurchaseActivityStep3.class);
        intent.putExtra(NewPurchaseKeys.STEP_3_PURCHASE_EXTRA_KEY, purchaseRecord);
        intent.putExtra(NewPurchaseKeys.STEP_3_ASSET_EXTRA_KEY, dummyGWREAsset);

        activityRule.launchActivity(intent);
        onView(withText("Finish")).check(matches(not(isEnabled())));
        onView(isRoot()).perform(pressBack());

        TestingUtils.verifyToastDisplayed(
                activityRule.getActivity(),
                activityRule.getActivity().getResources().getString(R.string.new_purchase_step_3_cant_go_back));
        onView(withText("Finish")).check(matches(not(isEnabled())));
    }

    /**
     * Tests what happens when an error is thrown during the process of entity generation. When the OK button is clicked,
     * the user should be brought home
     */
    @Test
    public void testErrorDialogOKSelected(){
        RequestHandlerProvider.requestHandlerToUse = RequestHandlerType.DUMMY_SERVICE;
        //write some backdated aorecords, so that the error asset latest price will be queried, thus throwing the exception
        dataHelper.writeAORecordsForSingleAssetDailyFrom(LocalDate.now().plusWeeks(1), dummyGWREAsset);

        Asset errorAsset = dataHelper.writeErrorAsset(dummyDublinExchange);
        Purchase purchaseRecord = dataHelper.generatePurchaseRecord(LocalDate.now(), errorAsset);
        Intent intent = new Intent(ApplicationProvider.getApplicationContext(), NewPurchaseActivityStep3.class);
        intent.putExtra(NewPurchaseKeys.STEP_3_PURCHASE_EXTRA_KEY, purchaseRecord);
        intent.putExtra(NewPurchaseKeys.STEP_3_ASSET_EXTRA_KEY, errorAsset);

        activityRule.launchActivity(intent);
        CountingIdlingResource componentIdlingResource = activityRule.getActivity().getIdlingResource();
        IdlingRegistry.getInstance().register(componentIdlingResource);

        //validate that the warning dialog is showing here, and that clicking back doesn't dismiss it
        onView(withText(containsString("Severe error occurred during portfolio update"))).check(matches(isDisplayed()));
        onView(isRoot()).perform(pressBack());
        onView(withText(containsString("Severe error occurred during portfolio update"))).check(matches(isDisplayed()));

        //press OK and go home
        onView(withText("OK")).perform(click());
        onView(withText(containsString("Portfolio Value"))).check(matches(isDisplayed()));

        //verify that the purchase didn't go through - 3 is the uid of the errorAsset
        assertEquals(0, dataHelper.getRoomDb().assetDAO().getAssetExchange(3).asset.liveQuantity.compareTo(BigDecimal.ZERO));
    }

    /**
     * Tests the following:
     *      "are you sure" dialog is shown when a backdated purchase can't fetch all of the price data,
     *      pressing OK causes the data to commit correctly
     *      After OK press the dialog transitions correctly
     *      the dialog isn't cancellable
     * In order to test this, a one year backdated series of APPL AORecords are written.  A Transaction for GWRE
     * is then written, also for one year ago.  This then causes a price data to be fetched for GWRE each date
     * over the last year.
     */
    @Test
    public void testAreYouSureDialogOkSelected(){

        doAreYouSureDialogBasicSetup();

        onView(withText("Accept changes")).perform(click());
        //commit phase should now happen, ensure that the purchase for GWRE was processed and the user can leave the Activity
        assertEquals(DataHelper.AMOUNT_OWNED, dataHelper.getRoomDb().assetDAO().getAssetExchange(1).asset.liveQuantity);
        onView(withText("Completed!")).check(matches(isDisplayed()));
        onView(withText("Finish")).check(matches(isEnabled()));

        //click finish, ensure we go home
        onView(withText("Finish")).perform(click());
        onView(withText(containsString("Portfolio Value"))).check(matches(isDisplayed()));

    }

    /**
     * Performs basic setup for the Are You Sure Dialog, namely:
     * <ul>
     *      <li>Using the dummy service, which always returns null price data</li>
     *      <li>Writing weekly backdated AORecords for the dummy AAPL asset over the last month</li>
     *      <li>Writing a Transaction record for the dummy GWRE record dated to a month ago</li>
     *      <li>Launching the activity and registering the idling resource</li>
     *      </ul>
     *  Also tests that the back button doesn't dismiss the dialog
     */
    private void doAreYouSureDialogBasicSetup(){
        //dummy service asset price requests will always return null
        RequestHandlerProvider.requestHandlerToUse = RequestHandlerType.DUMMY_SERVICE;

        //write backdated AAPL info for the last year
        dataHelper.writeAORecordsForSingleAssetWeeklyFrom(LocalDate.now().plusMonths(-1), dummyAAPLAsset);

        Purchase purchaseRecord = dataHelper.generatePurchaseRecord(LocalDate.now().plusMonths(-1), dummyGWREAsset);
        Intent intent = new Intent(ApplicationProvider.getApplicationContext(), NewPurchaseActivityStep3.class);
        intent.putExtra(NewPurchaseKeys.STEP_3_PURCHASE_EXTRA_KEY, purchaseRecord);
        intent.putExtra(NewPurchaseKeys.STEP_3_ASSET_EXTRA_KEY, dummyGWREAsset);

        activityRule.launchActivity(intent);
        IdlingRegistry.getInstance().register(activityRule.getActivity().getIdlingResource());
        //at this stage expecting the popup to show
        onView(withText(containsString("You are recording a backdated purchase, but Investment Assistant was unable to get all of the historical price data"))).check(matches(isDisplayed()));
        //test back button doesn't cancel dialog
        onView(isRoot()).perform(pressBack());
        //dialog should still be present
        onView(withText(containsString("You are recording a backdated purchase, but Investment Assistant was unable to get all of the historical price data"))).check(matches(isDisplayed()));

    }

    /**
     * Tests the following:
     *      "are you sure" dialog is shown when a backdated purchase can't fetch all of the price data
     *      pressing cancel brings the user back to the overview screen
     * In order to test this, a one year backdated series of APPL AORecords are written.  A Transaction for GWRE
     * is then written, also for one year ago.  This then causes a price data to be fetched for GWRE each date
     * over the last year.
     */
    @Test
    public void testAreYouSureDialogCancelSelected(){
        doAreYouSureDialogBasicSetup();
        //press cancel and ensure the user is brought to the home screen
        onView(withText("Cancel changes")).perform(click());
        onView(withText(containsString("Portfolio Value"))).check(matches(isDisplayed()));

        //ensure the purchase was abandoned - uid for the GWRE asset is 1
        assertEquals(0, dataHelper.getRoomDb().assetDAO().getAssetExchange(1).asset.liveQuantity.compareTo(BigDecimal.ZERO));
    }

    /**
     * Tests the retry functionality of the dialog. Presses retry, followed
     */
   @Test
    public void testAreYouSureDialogRetry(){
        doAreYouSureDialogBasicSetup();
        onView(withText("Retry update")).perform(click());
        //note: this should really be monitored on the test emulator as there's no way of knowing if the button did nothing

       onView(withText("Accept changes")).perform(click());
       //commit phase should now happen, ensure that the purchase for GWRE was processed and the user can leave the Activity
       assertEquals(DataHelper.AMOUNT_OWNED, dataHelper.getRoomDb().assetDAO().getAssetExchange(1).asset.liveQuantity);
       onView(withText("Completed!")).check(matches(isDisplayed()));
       onView(withText("Finish")).check(matches(isEnabled()));

       //click finish, ensure we go home
       onView(withText("Finish")).perform(click());
       onView(withText(containsString("Portfolio Value"))).check(matches(isDisplayed()));
    }


    /**
     * Tests the following scenario:
     *  Update attempted, fails due to network lag
     *  Connection re-established
     *  Update re-tried
     *  Update runs successfully, without showing warning dialog
     *
     *  Test needed to cover regression where ViewModel for NewPurchase step 3 doesn't correctly clear it's error state
     */
    @Test
    public void testAreYouSureDialogManualRetry_manual(){
       throw new NotImplementedException("Test me manually!");
    }

    /**
     * Tests that the "are you sure" dialog is shown when a custom asset purchase is made.
     */
    @Test
    public void testUpdatingCustomAsset(){
        RequestHandlerProvider.requestHandlerToUse = RequestHandlerType.DUMMY_SERVICE;
        Asset dummyCustomAsset = dataHelper.writeUserDefinedAsset(dummyDublinExchange);
        dataHelper.writeAORecordsForSingleAssetDailyFrom(LocalDate.now().plusWeeks(-1), dummyCustomAsset);

        Purchase purchaseRecord = dataHelper.generatePurchaseRecord(LocalDate.now().plusWeeks(-1), dummyCustomAsset);
        Intent intent = new Intent(ApplicationProvider.getApplicationContext(), NewPurchaseActivityStep3.class);
        intent.putExtra(NewPurchaseKeys.STEP_3_PURCHASE_EXTRA_KEY, purchaseRecord);
        intent.putExtra(NewPurchaseKeys.STEP_3_ASSET_EXTRA_KEY, dummyCustomAsset);

        activityRule.launchActivity(intent);
        CountingIdlingResource componentIdlingResource = activityRule.getActivity().getIdlingResource();
        IdlingRegistry.getInstance().register(componentIdlingResource);

        onView(withText(containsString("You are recording a purchase for a custom asset - this means that"))).check(matches(isDisplayed()));
        //test back button doesn't cancel dialog
        onView(isRoot()).perform(pressBack());
        onView(withText(containsString("You are recording a purchase for a custom asset - this means that"))).check(matches(isDisplayed()));

        //click accept an ensure dialog is dismissed
        onView(withText("Accept changes")).perform(click());
        BigDecimal expectedQuantity = DataHelper.AMOUNT_OWNED.multiply(new BigDecimal("2"));
        assertEquals(0, expectedQuantity.compareTo(dataHelper.getRoomDb().assetDAO().getAssetExchange(4).asset.liveQuantity));
        onView(withText("Completed!")).check(matches(isDisplayed()));
        onView(withText("Finish")).check(matches(isEnabled()));

        //click finish, ensure we go home
        onView(withText("Finish")).perform(click());
        onView(withText(containsString("Portfolio Value"))).check(matches(isDisplayed()));
    }

    /**
     * Tests that can be used to ensure the following issue does not occur:
     *  Wizard Completes, goes to overview screen
     *  Back button pressed, app exits
     *  App resumed from recents menu
     *  App picks up at wrong activity (NewPurchaseStep 3 instead of Overview)
     *
     * This can only be tested by launching the app from the overview screen
     */
    @Test
    public void endToEndTest_manual(){
        ActivityTestRule<MainActivity> mainActivityRule =
                new ActivityTestRule<>(MainActivity.class, false, false);
        mainActivityRule.launchActivity(null);

        dataHelper.writeAORecordsForSingleAssetDailyFrom(LocalDate.now().plusWeeks(-1), dummyGWREAsset);

        Purchase purchaseRecord = dataHelper.generatePurchaseRecord(LocalDate.now().plusWeeks(-1), dummyGWREAsset);
        Intent intent = new Intent(ApplicationProvider.getApplicationContext(), NewPurchaseActivityStep3.class);
        intent.putExtra(NewPurchaseKeys.STEP_3_PURCHASE_EXTRA_KEY, purchaseRecord);
        intent.putExtra(NewPurchaseKeys.STEP_3_ASSET_EXTRA_KEY, dummyGWREAsset);
        activityRule.launchActivity(intent);

        CountingIdlingResource componentIdlingResource = activityRule.getActivity().getIdlingResource();
        IdlingRegistry.getInstance().register(componentIdlingResource);

        //this view shouldn't be seen until the update has finished, which is linked to the
        //idling resource
        onView(withText("Completed!")).check(matches(isDisplayed()));
        onView(withText("Finish")).check(matches(isEnabled()));
        //not performing any data checks here, that's covered by other tests
        onView(withText("Finish")).perform(click());
        //ensure now on the overview screen
        onView(withText(containsString("Portfolio Value"))).check(matches(isDisplayed()));
        //fails on purpose as ths needs to be manually monitored
        assertEquals("Foo", "Test this manually by exiting the app and resuming it from recents!");
    }

}
