package com.drogers.investmentassistant.ui.newpurchase.assetsearch;

import android.content.Intent;

import androidx.test.espresso.IdlingRegistry;
import androidx.test.espresso.idling.CountingIdlingResource;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import com.drogers.investmentassistant.DataHelper;
import com.drogers.investmentassistant.R;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;
import com.drogers.investmentassistant.network.info.DummyRequestHandler;
import com.drogers.investmentassistant.network.info.RequestHandlerProvider;
import com.drogers.investmentassistant.network.info.RequestHandlerType;
import com.drogers.investmentassistant.ui.newpurchase.NewPurchaseKeys;
import com.drogers.investmentassistant.ui.newpurchase.TestingUtils;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.doesNotExist;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static com.drogers.investmentassistant.network.info.DummyRequestHandler.ERROR_TICKER;
import static junit.framework.TestCase.assertEquals;
import static org.hamcrest.CoreMatchers.containsString;

@LargeTest
public class AssetSearchStep2ActivityTest {

    DataHelper dataHelper;
    Exchange dummyDublinExchange;
    @Rule
    public ActivityTestRule<AssetSearchStep2Activity> activityRule =
            new ActivityTestRule<>(AssetSearchStep2Activity.class, false, false);

    @Before
    public void prepareDatabase() {
        dataHelper = new DataHelper();
        dataHelper.clearDb();
        DummyRequestHandler.throwErrorOnExchangeRequest = false;
        RequestHandlerProvider.requestHandlerToUse = RequestHandlerType.DUMMY_SERVICE;
        dummyDublinExchange = dataHelper.writeDummyDublinExchange();

    }

    private Intent buildSearchIntent(Exchange exchange, String queryString){
        Intent i = new Intent();
        i.putExtra(NewPurchaseKeys.REMOTE_ASSET_SEARCH_EXCHANGE_KEY, dummyDublinExchange);
        i.putExtra(NewPurchaseKeys.REMOTE_ASSET_SEARCH_TERM_KEY, queryString);
        return i;
    }

    /**
     * Tests that results are shown when the asset already exists on the specified exchange
     */
    @Test
    public void resultsFoundLocallyTest(){
        dataHelper.writeOneDummyAsset(dummyDublinExchange, 1);
        activityRule.launchActivity(buildSearchIntent(dummyDublinExchange,"gwre"));
        CountingIdlingResource componentIdlingResource = activityRule.getActivity().getIdlingResource();

        onView(withText("Guidewire")).check(matches(isDisplayed()));
        onView(withText(containsString("LOCAL"))).check(matches(isDisplayed()));
        onView(withText("Euronext Dublin Testing Exchange")).check(matches(isDisplayed()));
        onView(withText("No results found!")).check(doesNotExist());
        IdlingRegistry.getInstance().unregister(componentIdlingResource);
    }


    /**
     * Tests that only one result is shown when the asset already exists on other exchanges
     */
    @Test
    public void multipleLocalResultsTest(){
        dataHelper.writeOneDummyAsset(dummyDublinExchange, 1);
        dataHelper.writeOneDummyAsset(dataHelper.writeDummyNYSEExchange(), 2);
        activityRule.launchActivity(buildSearchIntent(dummyDublinExchange,"gwre"));
        CountingIdlingResource componentIdlingResource = activityRule.getActivity().getIdlingResource();

        onView(withText("Guidewire")).check(matches(isDisplayed()));
        onView(withText(containsString("LOCAL"))).check(matches(isDisplayed()));
        onView(withText("Euronext Dublin Testing Exchange")).check(matches(isDisplayed()));
        onView(withText("No results found!")).check(doesNotExist());
        onView(withText("New York Stock Testing Exchange")).check(doesNotExist());
        IdlingRegistry.getInstance().unregister(componentIdlingResource);
    }

    /**
        Tests no results at local or remote. Asset is written locally on a different exchange for
        test completeness
     */
    @Test
    public void noResultsTest(){
        dataHelper.writeOneDummyAsset(dataHelper.writeDummyNYSEExchange(), 2);
        activityRule.launchActivity(buildSearchIntent(dummyDublinExchange, ERROR_TICKER));
        CountingIdlingResource componentIdlingResource = activityRule.getActivity().getIdlingResource();

        onView(withText("Could not find ERROR_TICKER at Euronext Dublin Testing Exchange")).check(matches(isDisplayed()));
        IdlingRegistry.getInstance().unregister(componentIdlingResource);
    }

}
