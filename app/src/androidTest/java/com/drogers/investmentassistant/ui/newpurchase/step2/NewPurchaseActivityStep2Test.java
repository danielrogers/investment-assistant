package com.drogers.investmentassistant.ui.newpurchase.step2;

import android.app.Activity;
import android.content.Intent;

import com.drogers.investmentassistant.DataHelper;
import com.drogers.investmentassistant.R;
import com.drogers.investmentassistant.db.entity.Asset;
import com.drogers.investmentassistant.db.entity.AssetDAO;
import com.drogers.investmentassistant.ui.newpurchase.NewPurchaseKeys;
import com.drogers.investmentassistant.ui.newpurchase.TestingUtils;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.time.LocalDate;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static com.drogers.investmentassistant.ui.newpurchase.step2.Step2ViewModel.dateTimeFormatter;
import static junit.framework.Assert.assertNotNull;
import static org.hamcrest.Matchers.not;

@LargeTest
public class NewPurchaseActivityStep2Test {

    private AssetDAO assetDAO;
    Intent intent;
    private static String assetSymbol = "GWRE";
    @Rule
    public ActivityTestRule<NewPurchaseActivityStep2> activityRule =
            new ActivityTestRule<>(NewPurchaseActivityStep2.class, false, false);

    @Before
    public void doTestSetup() {
        DataHelper dataHelper = new DataHelper();
        dataHelper.clearDb();
        assetDAO = dataHelper.getRoomDb().assetDAO();
        Asset dummyAsset = dataHelper.writeOneDummyAsset(dataHelper.writeDummyDublinExchange(), 1);

        intent = new Intent(ApplicationProvider.getApplicationContext(), NewPurchaseActivityStep2.class);
        intent.putExtra(NewPurchaseKeys.ASSET_EXTRA_KEY, dummyAsset);
    }

    @Test
    public void testPriceFieldNotSet(){
        TestingUtils.waitForToastToClear();
        activityRule.launchActivity(intent);
        onView(withId(R.id.numberPurchased_input)).perform(typeText("4"), closeSoftKeyboard());
        onView(withText("Record Purchase")).perform(click());
        Activity activity = activityRule.getActivity();
        TestingUtils.verifyToastDisplayed(
                activity,
                activity.getResources().getString(R.string.error_new_purchase_invalid_price));
    }

    @Test
    public void testAmountFieldNotSet(){
        TestingUtils.waitForToastToClear();
        activityRule.launchActivity(intent);
        onView(withId(R.id.purchasingPrice_editText)).perform(typeText("4"), closeSoftKeyboard());
        onView(withText("Record Purchase")).perform(click());
        Activity activity = activityRule.getActivity();
        TestingUtils.verifyToastDisplayed(
                activity,
                activity.getResources().getString(R.string.error_new_purchase_invalid_quantity));
    }

    @Test
    public void testConfirmDialogShows(){
        Double amount = 4d;
        String price = "5.50";

        activityRule.launchActivity(intent);
        onView(withId(R.id.purchasingPrice_editText)).perform(typeText(price), closeSoftKeyboard());
        onView(withId(R.id.numberPurchased_input)).perform(typeText(String.valueOf(amount)), closeSoftKeyboard());
        onView(withText("Record Purchase")).perform(click());
        String expectedText = String.format(activityRule.getActivity().getResources().getString(R.string.new_purchase_step_2_are_you_sure),
                amount,
                assetSymbol,
                price,
                LocalDate.now().format(dateTimeFormatter));
        onView(withText(expectedText)).check(matches(isDisplayed()));
        onView(withText("No")).perform(click());
        onView(withText("Record Purchase")).check(matches(isDisplayed()));
    }

    @Test
    public void testTransitionsToStep3(){
        Double amount = 4d;
        String price = "5.50";

        activityRule.launchActivity(intent);
        onView(withId(R.id.purchasingPrice_editText)).perform(typeText(price), closeSoftKeyboard());
        onView(withId(R.id.numberPurchased_input)).perform(typeText(String.valueOf(amount)), closeSoftKeyboard());
        onView(withText("Record Purchase")).perform(click());
        onView(withText("Yes")).perform(click());

        /*purchase has been performed, we should see the following:
            * asset entity updated
        */
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withText("Updating your portfolio")).check(matches(isDisplayed()));
    }
}
