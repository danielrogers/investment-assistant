package com.drogers.investmentassistant.ui.newpurchase.assetsearch;

import android.app.SearchManager;
import android.content.Intent;

import com.drogers.investmentassistant.DataHelper;
import com.drogers.investmentassistant.R;
import com.drogers.investmentassistant.network.info.DummyRequestHandler;
import com.drogers.investmentassistant.network.info.RequestHandlerProvider;
import com.drogers.investmentassistant.network.info.RequestHandlerType;
import com.drogers.investmentassistant.ui.newpurchase.TestingUtils;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import androidx.test.espresso.IdlingRegistry;
import androidx.test.espresso.idling.CountingIdlingResource;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.doesNotExist;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static junit.framework.TestCase.assertEquals;

@LargeTest
public class AssetSearchStep1ActivityTest {

    DataHelper dataHelper;
    @Rule
    public ActivityTestRule<AssetSearchStep1Activity> activityRule =
            new ActivityTestRule<>(AssetSearchStep1Activity.class, false, false);

    @Before
    public void prepareDatabase() {
        dataHelper = new DataHelper();
        dataHelper.clearDb();
        DummyRequestHandler.throwErrorOnExchangeRequest = false;
        RequestHandlerProvider.requestHandlerToUse = RequestHandlerType.DUMMY_SERVICE;

    }

    private Intent buildSearchIntent(String queryString){
        Intent i = new Intent();
        i.setAction(Intent.ACTION_SEARCH);
        i.putExtra(SearchManager.QUERY, queryString);
        return i;
    }

    @Test
    public void resultsFoundLocallyTest(){
        dataHelper.writeOneDummyAsset(dataHelper.writeDummyNYSEExchange(), 1);
        activityRule.launchActivity(buildSearchIntent("gwre"));
        CountingIdlingResource componentIdlingResource = activityRule.getActivity().getIdlingResource();

        onView(withText("Guidewire")).check(matches(isDisplayed()));
        onView(withText("New York Stock Testing Exchange")).check(matches(isDisplayed()));
        onView(withText("No results found!")).check(doesNotExist());
        IdlingRegistry.getInstance().unregister(componentIdlingResource);
    }

    //tests case where stock is present on multiple exchanges
    @Test
    public void multipleResultsFoundLocallyTest(){
        dataHelper.writeOneDummyAsset(dataHelper.writeDummyNYSEExchange(), 1);
        dataHelper.writeOneDummyAsset(dataHelper.writeDummyDublinExchange(), 2);
        activityRule.launchActivity(buildSearchIntent("gwre"));
        CountingIdlingResource componentIdlingResource = activityRule.getActivity().getIdlingResource();
        IdlingRegistry.getInstance().register(componentIdlingResource);

        onView(withText("New York Stock Testing Exchange")).check(matches(isDisplayed()));
        onView(withText("No results found!")).check(doesNotExist());

        onView(withText("Euronext Dublin Testing Exchange")).check(matches(isDisplayed()));
        IdlingRegistry.getInstance().unregister(componentIdlingResource);
    }

    //no results found locally, list of exchanges should be loaded
    @Test
    public void noLocalResultsTest(){
        activityRule.launchActivity(buildSearchIntent("gwre"));
        CountingIdlingResource componentIdlingResource = activityRule.getActivity().getIdlingResource();
        IdlingRegistry.getInstance().register(componentIdlingResource);

        onView(withText(R.string.asset_search_remote_select_exchange)).check(matches(isDisplayed()));
        onView(withText("New York Stock Testing Exchange")).check(matches(isDisplayed()));
        onView(withText("Euronext Dublin Testing Exchange")).check(matches(isDisplayed()));
        IdlingRegistry.getInstance().unregister(componentIdlingResource);
    }


    @Test
    public void exchangeFetchErrorTest() {
        TestingUtils.waitForToastToClear();
        DummyRequestHandler.throwErrorOnExchangeRequest = true;

        activityRule.launchActivity(buildSearchIntent("gwre"));
        CountingIdlingResource componentIdlingResource = activityRule.getActivity().getIdlingResource();
        IdlingRegistry.getInstance().register(componentIdlingResource);

        TestingUtils.verifyToastDisplayed(activityRule.getActivity(), "Could not search:");
        IdlingRegistry.getInstance().unregister(componentIdlingResource);
    }

    @Test
    public void exchangeFetchNetworkTimeoutTest_manual() {
        assertEquals("Manually test when WiFi/data is turned on but unavailable", "");
        activityRule.launchActivity(buildSearchIntent("gwre"));
        CountingIdlingResource componentIdlingResource = activityRule.getActivity().getIdlingResource();
        IdlingRegistry.getInstance().register(componentIdlingResource);

        TestingUtils.verifyToastDisplayed(activityRule.getActivity(), "Could not search:");
        IdlingRegistry.getInstance().unregister(componentIdlingResource);
        assertEquals("Manually test when WiFi/data is turned on but unavailable", "");
    }
}
