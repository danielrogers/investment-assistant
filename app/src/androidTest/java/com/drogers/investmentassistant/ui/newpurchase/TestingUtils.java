package com.drogers.investmentassistant.ui.newpurchase;

import android.app.Activity;
import android.app.Application;

import com.drogers.investmentassistant.db.RoomDb;

import androidx.test.core.app.ApplicationProvider;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.RootMatchers.withDecorView;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;

public class TestingUtils {
    public static void verifyToastDisplayed(Activity activity, String containingText){
        onView(withText(containsString(containingText)))
                .inRoot(withDecorView(not(activity
                        .getWindow()
                        .getDecorView())))
                .check(matches(isDisplayed()));
    }

    //small wait period to allow any pre-existing toast to clear
    public static void waitForToastToClear(){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
