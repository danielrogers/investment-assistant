package com.drogers.investmentassistant.ui.indepth;

import androidx.lifecycle.ViewModelProviders;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import com.drogers.investmentassistant.DataHelper;
import com.drogers.investmentassistant.InvestmentAssistantApplication;
import com.drogers.investmentassistant.MainActivity;
import com.drogers.investmentassistant.db.RoomDb;
import com.drogers.investmentassistant.db.entity.Asset;
import com.drogers.investmentassistant.db.entity.AssetOwnershipRecord;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;
import com.drogers.investmentassistant.db.entity.transaction.Purchase;
import com.drogers.investmentassistant.db.entity.transaction.PurchaseDAO;
import com.drogers.investmentassistant.network.info.RequestHandlerProvider;
import com.drogers.investmentassistant.network.info.RequestHandlerType;
import com.drogers.investmentassistant.ui.newpurchase.step3.Step3ViewModel;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.schedulers.Schedulers;

import static junit.framework.TestCase.assertEquals;

@LargeTest
public class TransactionsOverviewViewModelTest {

    InDepthViewModel viewModel;
    DataHelper dataHelper = new DataHelper();
    PurchaseDAO purchaseDAO;

    @Rule
    public ActivityTestRule<MainActivity> activityRule =
            new ActivityTestRule<>(MainActivity.class, false, true);

    @Before
    public void doTestSetup(){
        InvestmentAssistantApplication application = ApplicationProvider.getApplicationContext();
        RequestHandlerProvider.requestHandlerToUse = RequestHandlerType.IEX_SANDBOX;
        RoomDb db = RoomDb.getDatabase(application);
        purchaseDAO = db.purchaseDAO();
        viewModel = ViewModelProviders.of(activityRule.getActivity()).get(InDepthViewModel.class);
        dataHelper.clearDb();
        writeTestData();
    }

    /**
     * Records a purchase using the step 3 view model - not ideal for unit testing, but it's far simpler
     * to use this functionality than rewrite it all in DataHelper
     */
    private void createPurchaseUsingStep3ViewModel(Asset asset, Exchange exchange, LocalDate date){
        Purchase purchase = dataHelper.generatePurchaseRecord(date, asset);
        Step3ViewModel viewModel = new Step3ViewModel(InvestmentAssistantApplication.application);
        viewModel
                .generatePurchaseData(purchase, asset, exchange, false)
                .subscribeOn(Schedulers.newThread())
                .test()
                .awaitDone(3, TimeUnit.SECONDS)
                .assertComplete();
        viewModel
                .commitData()
                .test()
                .awaitDone(3, TimeUnit.SECONDS)
                .assertComplete();
    }

    /**
     * Write purchase data for three Purchases:
     *  <ul>
     *  <li>GWRE & AAPL occurring 8 weeks ago from now.</li>
     *  <li>GWRE occurring 4 weeks ago from now.</li>
     *  <li>AAPL purchased today</li>
     *  </ul>
     * AORecords written weekly for each.
     */
    private void writeTestData(){
        LocalDate today = LocalDate.now();
        LocalDate dummyPurchaseDate = today.plusWeeks(-8);
        Exchange ex = dataHelper.writeDummyDublinExchange();
        List<Asset> assets = dataHelper.writeTwoDummyAssets(ex);
        Asset gwreAsset = assets.get(0);
        Asset applAsset = assets.get(1);
        createPurchaseUsingStep3ViewModel(gwreAsset, ex, dummyPurchaseDate);
        createPurchaseUsingStep3ViewModel(applAsset, ex, dummyPurchaseDate);
        dummyPurchaseDate = today.plusWeeks(-4);
        createPurchaseUsingStep3ViewModel(gwreAsset, ex, dummyPurchaseDate);
        createPurchaseUsingStep3ViewModel(applAsset, ex, today);
    }

    @Test
    public void testGetAllGroupedRelatedRecords(){
        Purchase purchase = purchaseDAO.getAll_synchronous().get(0); //first purchase will be for GWRE, 8 weeks ago
        LinkedHashMap<LocalDate, List<AssetOwnershipRecord>> results = viewModel.getGroupedRecordsForPurchase(purchase);
        assertEquals(3, results.keySet().size());
        assertEquals(1, results.get(LocalDate.now()).size());
    }

    @Test
    public void testCalculateAmountDeletionFromRecords(){
        Purchase purchase = purchaseDAO.getAll_synchronous().get(0); //first purchase will be for GWRE, 8 weeks ago
        LinkedHashMap<LocalDate, List<AssetOwnershipRecord>> groupedRecordsForPurchase = viewModel.getGroupedRecordsForPurchase(purchase);

        BigDecimal amountToDelete = DataHelper.AMOUNT_OWNED;

        ArrayList<List<AssetOwnershipRecord>> recordsForDeleteAndUpdate = viewModel.calculateAmountDeletionFromRecords(amountToDelete, groupedRecordsForPurchase);
        List<AssetOwnershipRecord> recordsToDelete = recordsForDeleteAndUpdate.get(0);
        List<AssetOwnershipRecord> recordsToUpdate = recordsForDeleteAndUpdate.get(1);
        assertEquals(2, recordsToDelete.size());
        assertEquals(1, recordsToUpdate.size());
    }
}
