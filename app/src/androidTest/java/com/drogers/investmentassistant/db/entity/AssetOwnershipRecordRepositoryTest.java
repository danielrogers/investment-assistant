package com.drogers.investmentassistant.db.entity;


import com.drogers.investmentassistant.DataHelper;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class AssetOwnershipRecordRepositoryTest {
    DataHelper dataHelper;
    AssetOwnershipRecordRepository assetOwnershipRecordRepository;

    @Before
    public void doTestSetup() {
        dataHelper = new DataHelper();
        dataHelper.clearDb();
        assetOwnershipRecordRepository = new AssetOwnershipRecordRepository(dataHelper.getRoomDb().assetOwnershipRecordDAO());

    }

    @Test
    public void testGetAllAORecordsFromLastYearPlus1(){
        //write records each month for last 14 months
        Exchange dummyExchange = dataHelper.writeDummyNYSEExchange();
        List<Asset> assets = dataHelper.writeTwoDummyAssets(dummyExchange);
        Asset dummyGWREAsset = assets.get(0);
        Asset dummyAAPLAsset = assets.get(1);
        dataHelper.writeAORecordsForSingleAssetMonthlyFrom(LocalDate.now().plusMonths(-14), dummyGWREAsset);
        dataHelper.writeAORecordsForSingleAssetMonthlyFrom(LocalDate.now().plusMonths(-14), dummyAAPLAsset);

        dataHelper
                .getRoomDb()
                .assetOwnershipRecordDAO()
                .getAORecordsFromLastYearPlus1()
                .test()
                .assertValue(assetOwnershipRecords -> {
                    //28 records have been written, two each month for the last 14 months
                    //would expect 26 to come back
                    return assetOwnershipRecords.size() == 26;
                });
    }

    @Test
    public void testGetAllEntryDatesFrom_emptyDB(){
        assetOwnershipRecordRepository
                .getAllEntryDatesFrom(LocalDate.now().plusWeeks(-1))
                .test()
                .assertValue(localDates -> {return localDates.size() == 2;});
    }

    @Test
    public void testGetAllEntryDatesFrom_populatedDB(){
        dataHelper.writeAORecordsForSingleAssetDailyFrom(LocalDate.now().plusDays(-4), dataHelper.writeOneDummyAsset(dataHelper.writeDummyNYSEExchange(), 1));
        //request entry dates for today -5, expect 5 to come back
        assetOwnershipRecordRepository
                .getAllEntryDatesFrom(LocalDate.now().plusDays(-4))
                .test()
                .assertValue(localDates -> {
                    return localDates.size() == 5;
                });
    }

    @Test
    public void testGetAllEntryDatesFrom_today(){
        //request entry dates for today -5, expect 5 to come back
        assetOwnershipRecordRepository
                .getAllEntryDatesFrom(LocalDate.now())
                .test()
                .assertValue(localDates -> {
                    return localDates.size() == 1;
                });
    }
}
