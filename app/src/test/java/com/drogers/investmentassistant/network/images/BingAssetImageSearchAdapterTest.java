package com.drogers.investmentassistant.network.images;

import com.drogers.investmentassistant.network.images.BingAssetImageSearchAdapter;
import com.drogers.investmentassistant.network.images.NoImageResultsException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.concurrent.TimeUnit;

import io.reactivex.observers.TestObserver;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class BingAssetImageSearchAdapterTest {

    /**
     * Tests that if the remote service returns null, getCompanyImageUrl correctly emits an error
     * @throws InterruptedException
     */
    @Test
    public void getImageResults_nullResultReturnedFromService() throws InterruptedException {
        BingAssetImageSearchAdapter fetcher = new BingAssetImageSearchAdapter("foo");
        fetcher = spy(fetcher);
        when(fetcher.getImageResults()).thenReturn(null);
        TestObserver<String> testObserver = new TestObserver<>();
        fetcher.getCompanyImageUrl().subscribe(testObserver);
        testObserver.awaitDone(1, TimeUnit.SECONDS);
        testObserver.assertError(NoImageResultsException.class);
    }

    /**
     * Tests that if the remote service returns an error, getImageResults correctly emits an error
     * This is a close test to what would happen if the API was down, or somehow unavailable.
     * @throws InterruptedException
     */
    @Test
    public void getImageResults_errorThrownFromService() {
        BingAssetImageSearchAdapter fetcher = new BingAssetImageSearchAdapter("foo");
        fetcher = spy(fetcher);
        when(fetcher.getImageResults()).thenThrow(new RuntimeException());
        TestObserver<String> testObserver = new TestObserver<>();
        fetcher.getCompanyImageUrl().subscribe(testObserver);
        testObserver.awaitDone(1, TimeUnit.SECONDS);
        testObserver.assertError(RuntimeException.class);
    }
}
