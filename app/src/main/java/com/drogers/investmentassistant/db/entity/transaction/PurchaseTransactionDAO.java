package com.drogers.investmentassistant.db.entity.transaction;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.reactivex.Maybe;

@Dao
public abstract class PurchaseTransactionDAO {
/*
    @androidx.room.Transaction
    public void insert(PurchaseTransaction... purchaseTransactions){
        ArrayList<Purchase> purchases = new ArrayList<>();
        ArrayList<Transaction> transactions = new ArrayList<>();
        for(PurchaseTransaction pt : purchaseTransactions){
            purchases.add(pt.purchase);
            transactions.add(pt.transaction);
        }
        insertPurchases(purchases.toArray(new Purchase[purchases.size()]));
        insertTransactions(transactions.toArray(new Transaction[0]));
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract Maybe<List<Long>> insertPurchases(Purchase... unit);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract Maybe<List<Long>> insertTransactions(Transaction... unit);
    */
}
