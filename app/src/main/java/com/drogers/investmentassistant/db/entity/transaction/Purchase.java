package com.drogers.investmentassistant.db.entity.transaction;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.drogers.investmentassistant.db.TypeConverters;

import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@androidx.room.TypeConverters(TypeConverters.class)
public class Purchase extends Transaction implements Parcelable{

    @PrimaryKey(autoGenerate=true) public long uid;
    //the active quantity - an asset is considered active until it is sold
    @NonNull public BigDecimal remainingActiveUnits;

    public Purchase(){
    }
    /*
    -------------------------------
    Parcelable implementation
    -------------------------------
     */
    public static final Parcelable.Creator<Purchase> CREATOR = new Parcelable.Creator<Purchase>() {
        @Override
        public Purchase createFromParcel(Parcel in) {
            return new Purchase(in);
        }

        @Override
        public Purchase[] newArray(int size) {
            return new Purchase[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(uid);
        dest.writeLong(assetUid);
        dest.writeValue(totalQuantity);
        dest.writeValue(date);
        dest.writeValue(assetPrice);
        dest.writeValue(remainingActiveUnits);
    }

    private Purchase(Parcel in){
        uid = in.readLong();
        assetUid = in.readLong();
        totalQuantity = (BigDecimal) in.readValue(null);
        date = (LocalDate) in.readValue(null);
        assetPrice = (BigDecimal) in.readValue(null);
        remainingActiveUnits = (BigDecimal) in.readValue(this.getClass().getClassLoader());
    }
}
