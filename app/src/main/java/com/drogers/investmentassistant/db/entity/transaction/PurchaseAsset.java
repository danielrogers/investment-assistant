package com.drogers.investmentassistant.db.entity.transaction;

import androidx.room.Embedded;
import androidx.room.Relation;

import com.drogers.investmentassistant.db.AssetExchange;
import com.drogers.investmentassistant.db.entity.Asset;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;

import java.util.List;

public class PurchaseAsset {
    @Embedded
    public Purchase purchase;

    @Relation(parentColumn = "assetUid", entityColumn = "uid", entity = Asset.class )
    List<AssetExchange> assetExchanges;


    //assetExchanges will only ever have one AE - slight workaround to take advantage of the convenience of @Relation
    public Asset getAsset(){
        return assetExchanges.get(0).asset;
    }

    //assetExchanges will only ever have one AE - slight workaround to take advantage of the convenience of @Relation
    public Exchange getExchange(){
        return assetExchanges.get(0).getExchange();
    }
}
