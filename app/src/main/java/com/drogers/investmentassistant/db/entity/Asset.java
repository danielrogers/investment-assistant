package com.drogers.investmentassistant.db.entity;

import android.os.Parcel;
import android.os.Parcelable;
import com.drogers.investmentassistant.db.TypeConverters;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(indices = {@Index(value = {"symbol", "exchangeMarketIdentifierCode"}, unique = true)},
        foreignKeys = @ForeignKey(entity = Exchange.class,
                parentColumns = "marketIdentifierCode",
                childColumns = "exchangeMarketIdentifierCode"))
@androidx.room.TypeConverters(TypeConverters.class)
public class Asset implements Parcelable {
    @NonNull @PrimaryKey(autoGenerate=true) public long uid;
    @NonNull @ColumnInfo(collate = ColumnInfo.NOCASE) public String symbol;
    @NonNull public String name;
    @NonNull public AssetType assetType;
    @NonNull public String description;
    public LocalDate lastPurchaseDate;
    //the amount of this asset currently in the portfolio
    @NonNull public BigDecimal liveQuantity;
    @NonNull public boolean isUserDefinedAsset;
    //MIC of the exchange this asset is traded on
    @NonNull public String exchangeMarketIdentifierCode;
    //if adding a field here, make sure to parcel it!


    public Asset(String name, AssetType assetType, String description, String symbol, String exchangeMarketIdentifierCode){
        this();
        this.name = name;
        this.assetType = assetType;
        this.description = description;
        this.symbol = symbol;
        this.exchangeMarketIdentifierCode = exchangeMarketIdentifierCode;
    }



    public Asset(String name, AssetType assetType, String description, String symbol, String exchangeMarketIdentifierCode, LocalDate lastPurchaseDate){
        this(name, assetType, description, symbol, exchangeMarketIdentifierCode);
        this.lastPurchaseDate = lastPurchaseDate;
    }

    public Asset(){
        this.liveQuantity = new BigDecimal("0.0");
    }

    /*
    Below methods used for Parcelable interface
     */

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(uid);
        dest.writeString(name);
        dest.writeString(assetType.name());
        dest.writeString(description);
        dest.writeString(symbol);
        if(lastPurchaseDate == null){
            dest.writeValue(null);
        }else {
            dest.writeValue(lastPurchaseDate);
        }
        dest.writeValue(liveQuantity);
        dest.writeByte((byte) (isUserDefinedAsset ? 1 : 0));
        dest.writeString(exchangeMarketIdentifierCode);
    }

    public static final Parcelable.Creator<Asset> CREATOR = new Parcelable.Creator<Asset>() {
        public Asset createFromParcel(Parcel in) {
            return new Asset(in);
        }

        public Asset[] newArray(int size) {
            return new Asset[size];
        }
    };

    public Asset(Parcel in){
        this.uid = in.readLong();
        this.name = in.readString();
        this.assetType = AssetType.valueOf(in.readString());
        this.description = in.readString();
        this.symbol = in.readString();
        Object dateValue = in.readValue(null);
        if(dateValue != null) {
            this.lastPurchaseDate = (LocalDate) dateValue;
        }
        this.liveQuantity = (BigDecimal) in.readValue(null);
        this.isUserDefinedAsset = in.readByte() != 0;
        this.exchangeMarketIdentifierCode = in.readString();
    }

    @Override
    public String toString(){
        return this.name + "; " + this.symbol + "; " + this.uid;
    }
}
