package com.drogers.investmentassistant.db.entity;

import android.content.Context;
import android.util.Log;

import com.drogers.investmentassistant.db.AssetExchange;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;
import com.drogers.investmentassistant.network.info.AssetInformationServiceRequestAdapter;
import com.drogers.investmentassistant.network.info.RequestHandlerProvider;

import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.UnicastSubject;
import timber.log.Timber;

public class AssetRepository implements IADisposable {

    private AssetDAO assetDAO;
    private Context context;
    private CompositeDisposable compositeDisposable;

    public AssetRepository(Context context, AssetDAO assetDAO) {
        this.assetDAO = assetDAO;
        this.context = context;
        compositeDisposable = new CompositeDisposable();
    }

    /**
     * Gets the 20 most recently used assets.
     *
     * @return
     */
    public Maybe<List<AssetExchange>> getRecentAssets() {
        return assetDAO.getRecentAssets();
    }


    public List<String> getSymbolFromUid(long uid){
        return assetDAO.getSymbolFromUid(uid);
    }

    /**
     * Searches for an asset based on the specified query String (exact matches only). The local DB
     * is searched
     * @param symbol
     * @return
     */
    public UnicastSubject<List<AssetExchange>> getAllAssetsWithSymbolLocally(String symbol) {
        final String trimmedQueryString = symbol.trim();
        Timber.d("Querying locally for " + trimmedQueryString);
        UnicastSubject<List<AssetExchange>> subject = UnicastSubject.create();
        Single<List<AssetExchange>> localData = assetDAO.getAllAssetsWithSymbol(trimmedQueryString);
        compositeDisposable.add(localData
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(asset -> {
                            if(asset.isEmpty()){
                                Timber.d("Asset not found locally");
                            }else{
                                Timber.d("Asset found locally");
                            }
                            subject.onNext(asset);
                        },
                        throwable -> {
                            Timber.d("Asset not found locally!");
                            subject.onError(throwable);
                        }
                ));
        return subject;
    }


    private void doRemoteSearch(UnicastSubject<AssetExchange> subject, String queryString, Exchange exchange){
        AssetInformationServiceRequestAdapter iexRequestHandler = RequestHandlerProvider.getAssetInfoRequestAdapterImplementation(context);
        compositeDisposable.add(iexRequestHandler
                .makeStockInfoRequest(queryString, exchange)
                .subscribe(asset -> {
                            AssetExchange assetExchange = new AssetExchange();
                            assetExchange.asset = asset;
                            assetExchange.setExchange(exchange);
                            subject.onNext(assetExchange);
                        },
                        subject::onError)
        );
    }

    public UnicastSubject<AssetExchange> getAssetFromLocalOrRemote(String queryString, Exchange exchange) {
        final String trimmedQueryString = queryString.trim();
        Timber.d("Querying locally for " + trimmedQueryString + " on exchange " + exchange.marketIdentifierCode);
        UnicastSubject<AssetExchange> subject = UnicastSubject.create();
        Single<AssetExchange> localData = assetDAO.getAssetOnExchange(trimmedQueryString, exchange.marketIdentifierCode);
        compositeDisposable.add(localData
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(asset -> {
                            subject.onNext(asset);
                            Timber.d("Asset found locally");
                        },
                        throwable -> {
                            Timber.d("Asset not found locally, searching remote!");
                            doRemoteSearch(subject, queryString, exchange);
                        }
                ));
        return subject;
    }

    public Maybe<List<Long>> insert(Asset... assets){
        return assetDAO.insert(assets);
    }

    public Maybe<Integer> update(Asset... assets) {
        return assetDAO.updateAssets(assets);
    }

    public Integer update_synchronous(Asset... assets) {
        return assetDAO.updateAssets_synchronous(assets);
    }

    public AssetExchange getAssetExchange(long uid){
        return assetDAO.getAssetExchange(uid);
    };

    @Override
    public CompositeDisposable getDisposable() {
        return compositeDisposable;
    }
}
