package com.drogers.investmentassistant.db.entity.exchange;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.drogers.investmentassistant.db.TypeConverters;


@Entity(indices = {@Index(value = "marketIdentifierCode", unique = true)})
@androidx.room.TypeConverters(TypeConverters.class)
public class Exchange implements Parcelable {
    @NonNull @PrimaryKey public String marketIdentifierCode;
    @NonNull public String name;
    public String suffix;
    @NonNull public String iexSearchCode; //code used to identify the exchange on iex. not the same as the MIC

    public Exchange(){
    }

    public Exchange(String marketIdentifierCode, String name, String suffix, String iexSearchCode){
        this.marketIdentifierCode = marketIdentifierCode;
        this.name = name;
        this.suffix = suffix;
        this.iexSearchCode = iexSearchCode;
    }

    protected Exchange(Parcel in) {
        name = in.readString();
        suffix = in.readString();
        marketIdentifierCode = in.readString();
        iexSearchCode = in.readString();
    }

    public static final Creator<Exchange> CREATOR = new Creator<Exchange>() {
        @Override
        public Exchange createFromParcel(Parcel in) {
            return new Exchange(in);
        }

        @Override
        public Exchange[] newArray(int size) {
            return new Exchange[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(suffix);
        dest.writeString(marketIdentifierCode);
        dest.writeString(iexSearchCode);
    }
}
