package com.drogers.investmentassistant.db.entity.transaction;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import io.reactivex.Maybe;

@Dao
public interface PurchaseDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Maybe<List<Long>> insert(Purchase... unit);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    List<Long> insert_synchronous(Purchase... unit);

    @Query("SELECT * from purchase order by date(date) desc")
    Maybe<List<PurchaseAsset>> getAllPurchasesDesc();

    @Query("SELECT * from purchase")
    List<Purchase> getAll_synchronous();

    @Delete
    Integer deletePurchases(Purchase... purchases);
}
