package com.drogers.investmentassistant.db.entity;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface AssetTypeDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(AssetType assetType);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAssets(AssetType... assetType);

    // Update multiple entries with one call.
    @Update
    void updateAssets(AssetType... assetTypes);

    // Simple query without parameters that returns values.
    @Query("SELECT * from assetType")
    LiveData<List<AssetType>> getAllAssetTypes();

}
