package com.drogers.investmentassistant.db.entity;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;
import io.reactivex.Maybe;
import io.reactivex.subjects.UnicastSubject;

@Dao
@androidx.room.TypeConverters(com.drogers.investmentassistant.db.TypeConverters.class)
public abstract class AssetOwnershipRecordDAO {

    @Insert(onConflict=OnConflictStrategy.IGNORE)
    abstract long insert(AssetOwnershipRecord assetOwnershipRecord);

    @Insert(onConflict=OnConflictStrategy.IGNORE)
    abstract List<Long> insert(AssetOwnershipRecord... assetOwnershipRecord);

    @Update
    abstract void update(AssetOwnershipRecord assetOwnershipRecord);

    @Transaction
    public void upsert(AssetOwnershipRecord... assetOwnershipRecords){
        for(AssetOwnershipRecord aoRecord : assetOwnershipRecords){
            if(insert(aoRecord) == -1){
                update(aoRecord);
            }
        }
    }

    @Transaction
    public void upsert(List<AssetOwnershipRecord> assetOwnershipRecords){
        for(AssetOwnershipRecord aoRecord : assetOwnershipRecords){
            if(insert(aoRecord) == -1){
                update(aoRecord);
            }
        }
    }

    @Delete
    abstract Integer delete(AssetOwnershipRecord... assetOwnershipRecords);

    /**
     * Gets all AssetOwnershipRecord entries from the last year, plus any entries which occurred on the first date greater than one year ago from today
     * @return
     */
    @Query("select * from" +
            "(select * from (select * from assetownershiprecord where date(asofdate) in (select date(asofdate) from assetownershiprecord where date(asofdate) < date('now', '-1 year') order by date(asOfDate) DESC limit 1))" +
            " union " +
            " select * from assetownershiprecord where date(asOfdate) >= date('now', '-1 year'))" +
            " order by date(asOfDate) ")
    abstract Maybe<List<AssetOwnershipRecord>> getAORecordsFromLastYearPlus1();

    /**
    Queries for any AORecords which should be present on this date.  The first set of AORecords found
     with an asOfDate previous to formattedDate are examined.  Any of these with an amountOwned > 0
     and which are themselves not present on formattedDate are returned
     */
    @Query("select * from assetownershiprecord " +
            " where " +
            " date(asofdate) in (select date(asofdate) from assetownershiprecord where date(asofdate) < date(:formattedDate) order by date(asOfDate) DESC limit 1 ) " +
            " and " +
            " assetuid not in (select assetuid from assetownershiprecord where date(asofdate) = :formattedDate) " +
            " and amountOwned > 0 ")
    abstract List<AssetOwnershipRecord> getAORecordsWhichShouldExistOnDate(String formattedDate);

    /**
     * Gets any AO records referring to the specified asset on the given date
     * @param formattedDate Date in format yyyy-MM-dd
     * @param assetUid
     * @return
     */
    @Query("select * from assetownershiprecord where date(asOfDate) = date(:formattedDate) and assetUid = :assetUid")
    abstract List<AssetOwnershipRecord> getAORecordsOnDate_synchronous(String formattedDate, long assetUid);


    @Query("select * from assetownershiprecord")
    public abstract List<AssetOwnershipRecord> getAllAORecords();

    /**
     * Gets a List of all dates on which there are any AORecords, where the date is on or after the specified date
     * Will also return the specified fromDate and today's date, regardless of whether or not there are any entries on either date
     * @param formattedFromDate Date in format yyyy-MM-dd
     * @return
     */
    @Query("select date(:formattedFromDate) union select asOfDate from assetownershiprecord where date(asOfDate) >= date(:formattedFromDate) union select date('now')")
    abstract Maybe<List<LocalDate>> getAllEntryDatesFrom(String formattedFromDate);

    @Query("select asOfDate from assetownershiprecord where date(asOfDate) >= date(:formattedFromDate) union select date('now')")
    abstract List<LocalDate> getAllEntryDatesFrom_synchronous(String formattedFromDate);

    /**
    Returns a list of all AORecords relating to the specified Asset with an entry date greater than
    or equal to formattedFromDate
     * @param assetUid
     * @param formattedFromDate Date in format yyyy-MM-dd
     */
    @Query("select * from assetownershiprecord as aor where aor.assetUid = :assetUid and date(asOfDate) >= date(:formattedFromDate)")
    abstract List<AssetOwnershipRecord> getAllRecordsRelatingToAsset(long assetUid, String formattedFromDate);
}
