package com.drogers.investmentassistant.db.entity;

import com.drogers.investmentassistant.db.TypeConverters;

import java.math.BigDecimal;
import java.time.LocalDate;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

/**
 * Represents how much of an asset was owned as of a given day, and the price on that day
 */
@Entity(foreignKeys = @ForeignKey(entity = Asset.class,
        parentColumns = "uid",
        childColumns = "assetUid"))
//todo reindex, can't use the below composite index as we could have multiple entries for one day
        //indices = {@Index(value = {"assetUid", "asOfDate"}, unique = true)})
@androidx.room.TypeConverters(TypeConverters.class)
public class AssetOwnershipRecord implements GraphableValue {
    @PrimaryKey(autoGenerate=true) @NonNull long uid;
    @NonNull public long assetUid;
    @NonNull public BigDecimal price;
    @NonNull public BigDecimal amountOwned;
    @NonNull public LocalDate asOfDate;
    //does this record's price reflect the closing price as of it's date, or does it simply represent a price at purchase time (e.g. midday)?
    @NonNull public boolean priceRepresentsClosingValue;
    @NonNull public boolean closingPriceEstimated;

    public AssetOwnershipRecord(){

    }

    public AssetOwnershipRecord(long assetUid, BigDecimal price, BigDecimal amountOwned, LocalDate asOfDate, boolean priceRepresentsClosingValue){
        this.assetUid = assetUid;
        this.price = price;
        this.amountOwned = amountOwned;
        this.asOfDate = asOfDate;
        this.priceRepresentsClosingValue = priceRepresentsClosingValue;
    }

    @Override
    public LocalDate getX() {
        return asOfDate;
    }

    @Override
    public BigDecimal getY() {
        return price.multiply(amountOwned);
    }
}
