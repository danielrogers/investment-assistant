package com.drogers.investmentassistant.db.entity.exchange;

import com.drogers.investmentassistant.db.RoomDb;

import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.Single;

public class ExchangeRepository {

    private ExchangeDAO exchangeDAO;

    public ExchangeRepository(RoomDb db) {
        exchangeDAO = db.exchangeDAO();
    }

    public Maybe<List<Long>> insert(List<Exchange> exchanges){
        return exchangeDAO.insert(exchanges.toArray(new Exchange[0]));
    }


    public Maybe<List<Long>> insertIfNotExists(List<Exchange> exchanges){
        return exchangeDAO.insertIfNotExists(exchanges.toArray(new Exchange[0]));
    }

    public Exchange getExchange_synchronous(String marketIdentifierCode){
        return exchangeDAO.getExchange_synchronous(marketIdentifierCode);
    }

    public Single<Exchange> getExchange(String marketIdentifierCode){
        return exchangeDAO.getExchange(marketIdentifierCode);
    }
}