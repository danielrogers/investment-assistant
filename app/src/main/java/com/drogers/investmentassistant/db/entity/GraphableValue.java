package com.drogers.investmentassistant.db.entity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

public interface GraphableValue {
    LocalDate getX();
    BigDecimal getY();
}
