package com.drogers.investmentassistant.db.entity.transaction;

import java.math.BigDecimal;
import java.time.LocalDate;

import androidx.annotation.NonNull;

import com.drogers.investmentassistant.db.TypeConverters;


/*
Base class for representing the purchase or sale of a distinct unit.
 */

@androidx.room.TypeConverters(TypeConverters.class)
public abstract class Transaction {

    @NonNull
    //the asset that this represents
    public long assetUid;

    //the total quantity of units involved in this transaction
    @NonNull
    public BigDecimal totalQuantity;

    @NonNull
    public LocalDate date;

    @NonNull
    //stored as a string on the DB
    public BigDecimal assetPrice;

    public Transaction(){
    }
}
