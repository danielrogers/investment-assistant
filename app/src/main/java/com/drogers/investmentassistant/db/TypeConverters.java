package com.drogers.investmentassistant.db;


import com.drogers.investmentassistant.db.entity.AssetType;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import androidx.room.TypeConverter;

public class TypeConverters {

    @TypeConverter
    public static String assetTypeToString(AssetType at){
        return at.name();
    }

    @TypeConverter
    public static AssetType stringToAssetType(String s){
        return AssetType.valueOf(s);
    }

    @TypeConverter
    public static String localDateToString(LocalDate d){
        if(d == null){
            return null;
        }
        return d.toString();
    }

    @TypeConverter
    public static LocalDate stringToLocalDate(String s){
        if(s == null){
            return null;
        }
        return LocalDate.parse(s);
    }

    @TypeConverter
    public static BigDecimal stringToBigDecimal(String s){
        if(s == null || s.isEmpty()){
            return null;
        }
        return new BigDecimal(s);
    }

    @TypeConverter
    public static String bigDecimalToString(BigDecimal bd){
        if(bd == null){
            return null;
        }
        return bd.toString();
    }
}
