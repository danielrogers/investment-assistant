package com.drogers.investmentassistant.db;


import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import android.content.Context;

import com.drogers.investmentassistant.db.entity.Asset;
import com.drogers.investmentassistant.db.entity.AssetDAO;
import com.drogers.investmentassistant.db.entity.AssetOwnershipRecord;
import com.drogers.investmentassistant.db.entity.AssetOwnershipRecordDAO;
import com.drogers.investmentassistant.db.entity.transaction.Purchase;
import com.drogers.investmentassistant.db.entity.transaction.PurchaseDAO;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;
import com.drogers.investmentassistant.db.entity.exchange.ExchangeDAO;

@Database(entities = {
        Asset.class,
        AssetOwnershipRecord.class,
        Exchange.class, Purchase.class}, version = 35)
public abstract class RoomDb extends RoomDatabase {

    public abstract PurchaseDAO purchaseDAO();
    public abstract AssetOwnershipRecordDAO assetOwnershipRecordDAO();
    public abstract AssetDAO assetDAO();
    public abstract ExchangeDAO exchangeDAO();

    private static RoomDb INSTANCE;

    public static RoomDb getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (RoomDb.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            RoomDb.class, "investment_assistant_database")
                            // Wipes and rebuilds instead of migrating
                            // if no Migration object.
                            .fallbackToDestructiveMigration()//todo this can't go to prod
                            .build();
                }
            }
        }
        return INSTANCE;
    }


    //poc of transaction rollbacks
    /*
    public static void testMultiLoad(RoomDb db){
        AssetDAO assetDAO = db.assetDAO();
        AssetOwnershipRecordDAO assetOwnershipRecordDAO = db.assetOwnershipRecordDAO();

        Asset asset1 = new Asset("Guidewire", AssetType.STOCK, "Guidewire software is a company", "GWRE2");
        Calendar instance = Calendar.getInstance();
        instance.set(2011, 5, 6);
        Single
                .just(1)
                .subscribeOn(Schedulers.computation())
                .subscribe(integer -> {
                    db.runInTransaction(() -> {
                        Long uid = assetDAO.insert(asset1).get(0);
                        AssetOwnershipRecord record1 = new AssetOwnershipRecord(uid, new BigDecimal("50"), 3, instance.getTime());
                        assetOwnershipRecordDAO.insert(record1);
                    });
                });
    }*/

    /*
    public static void destructivelyLoadSampleData(Application application, RoomDb db){
        //hack for convenience
        Single
                .just(1)
                .subscribeOn(Schedulers.computation())
                .subscribe(integer -> {
                    db.clearAllTables();
                });
        AssetRepository assetRepository = new AssetRepository(application, db.assetDAO());
        List<Asset> assets = getSampleAssetsToLoad();
        assetRepository
                .insert(assets.toArray(new Asset[assets.size()]))
                .subscribeOn(Schedulers.computation())
                .subscribe(longs -> Timber.d("Inserted " + longs.size() + " Asset records"));


        AssetOwnershipRecordRepository assetOwnershipRecordRepository = new AssetOwnershipRecordRepository(db.assetOwnershipRecordDAO());
        List<AssetOwnershipRecord> aORecords = getSampleAORecordsToWrite();
        assetOwnershipRecordRepository
                .insert(aORecords.toArray(new AssetOwnershipRecord[aORecords.size()]))
                .subscribeOn(Schedulers.computation())
                .subscribe(longs -> Timber.d("Inserted " + longs.size() + " AO records"));
    }*/

    public static void clearDB(RoomDb db){
        //hack for convenience
        Single
                .just(1)
                .subscribeOn(Schedulers.computation())
                .subscribe(integer -> {
                    db.clearAllTables();
                });
    }
}