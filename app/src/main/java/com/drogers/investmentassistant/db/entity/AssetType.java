package com.drogers.investmentassistant.db.entity;

import androidx.room.Entity;

@Entity
public enum AssetType {
    STOCK,
    COMMODITY,
    DUMMY
}
