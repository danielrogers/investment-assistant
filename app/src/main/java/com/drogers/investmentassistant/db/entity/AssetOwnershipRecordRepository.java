package com.drogers.investmentassistant.db.entity;

import com.drogers.investmentassistant.db.TypeConverters;

import java.time.LocalDate;
import java.util.List;
import io.reactivex.Maybe;

public class AssetOwnershipRecordRepository {

    private AssetOwnershipRecordDAO assetOwnershipRecordDAO;

    public AssetOwnershipRecordRepository(AssetOwnershipRecordDAO assetOwnershipRecordDAO) {
        this.assetOwnershipRecordDAO = assetOwnershipRecordDAO;
    }

    public void insert(List<AssetOwnershipRecord> assetOwnershipRecords){
        assetOwnershipRecordDAO.insert(assetOwnershipRecords.toArray(new AssetOwnershipRecord[0]));
    }

    public void insert(AssetOwnershipRecord assetOwnershipRecord){
        assetOwnershipRecordDAO.insert(assetOwnershipRecord);
    }

    public void upsert(List<AssetOwnershipRecord> assetOwnershipRecords){
        assetOwnershipRecordDAO.upsert(assetOwnershipRecords);
    }

    public Maybe<List<AssetOwnershipRecord>> getAORecordsFromLastYearPlus1(){
        return assetOwnershipRecordDAO.getAORecordsFromLastYearPlus1();
    }

    public List<AssetOwnershipRecord> getAORecordsWhichShouldExistOnDate(LocalDate date){
        return assetOwnershipRecordDAO.getAORecordsWhichShouldExistOnDate(TypeConverters.localDateToString(date));
    }

    public Integer delete(List<AssetOwnershipRecord> assetOwnershipRecords){
        return assetOwnershipRecordDAO.delete(assetOwnershipRecords.toArray(new AssetOwnershipRecord[0]));
    };

    /**
     * Gets any AO records referring to the specified asset on the given date - does not care about H:M:S fields
     * @param date
     * @param asset
     * @return
     */
    public List<AssetOwnershipRecord> getAORecordsOnDate_synchronous(LocalDate date, Asset asset){
        return assetOwnershipRecordDAO.getAORecordsOnDate_synchronous(TypeConverters.localDateToString(date), asset.uid);
    }

    /**
     * Gets a List of all dates on which there are any AORecords, where the date is on or after the specified date
     * Will also return the specified fromDate and today's date, regardless of whether or not there are any entries on either date
     * @param fromDate
     * @return
     */
    public Maybe<List<LocalDate>> getAllEntryDatesFrom(LocalDate fromDate){
        return assetOwnershipRecordDAO.getAllEntryDatesFrom(TypeConverters.localDateToString(fromDate));
    }

    public List<LocalDate> getAllEntryDatesFrom_synchronous(LocalDate fromDate){
        return assetOwnershipRecordDAO.getAllEntryDatesFrom_synchronous(TypeConverters.localDateToString(fromDate));
    }


    /**
     * See {@link AssetOwnershipRecordDAO#getAllRecordsRelatingToAsset(long, String)}
     * @param assetUid
     * @param fromDate
     * @return
     */
    public List<AssetOwnershipRecord> getAllRecordsRelatingToAsset(long assetUid, LocalDate fromDate){
        return assetOwnershipRecordDAO.getAllRecordsRelatingToAsset(assetUid, TypeConverters.localDateToString(fromDate));
    }


}
