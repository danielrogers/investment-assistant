package com.drogers.investmentassistant.db.entity.transaction;

import androidx.room.Delete;

import io.reactivex.Maybe;

import java.util.List;

public class PurchaseRepository {

    private PurchaseDAO purchaseDAO;

    public PurchaseRepository(PurchaseDAO dao) {
        purchaseDAO = dao;
    }

    public Maybe<List<Long>> insert (Purchase... purchases) {
        return purchaseDAO.insert(purchases);
    }

    public Integer deletePurchase(Purchase... purchases){
        return purchaseDAO.deletePurchases(purchases);
    }

    public List<Long> insert_synchronous(Purchase... purchases) {
        return purchaseDAO.insert_synchronous(purchases);
    }

    public Maybe<List<PurchaseAsset>> getAllPurchasesDesc(){
        return purchaseDAO.getAllPurchasesDesc();
    }

    public List<Purchase> getAll_synchronous(){
        return purchaseDAO.getAll_synchronous();
    }
}