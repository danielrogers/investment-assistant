package com.drogers.investmentassistant.db.entity;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Used to expose functionality for Disposable cleanup
 */
public interface IADisposable {
    CompositeDisposable getDisposable();

    default void onDispose(){
        getDisposable().dispose();
    };
}
