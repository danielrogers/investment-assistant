package com.drogers.investmentassistant.db.entity.exchange;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.Single;

@Dao
public abstract class ExchangeDAO {

    @Insert
    abstract public Maybe<List<Long>> insert(Exchange... exchanges);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract public Maybe<List<Long>> insertIfNotExists(Exchange... exchanges);

    @Query("SELECT * from exchange where marketIdentifierCode = :marketIdentifierCode")
    public abstract Exchange getExchange_synchronous(String marketIdentifierCode);

    @Query("SELECT * from exchange where marketIdentifierCode = :marketIdentifierCode")
    public abstract Single<Exchange> getExchange(String marketIdentifierCode);

    @Query("SELECT * from exchange LIMIT 1")
    public abstract Exchange getAnyExchange();
}
