package com.drogers.investmentassistant.db.entity;


import android.database.sqlite.SQLiteConstraintException;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.drogers.investmentassistant.db.AssetExchange;

import io.reactivex.Maybe;
import io.reactivex.Single;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.UnicastSubject;

@Dao
public abstract class AssetDAO{

    @Insert
    abstract public Maybe<List<Long>> insert(Asset... assets);


    // Update multiple entries with one call.
    @Update
    abstract Maybe<Integer> updateAssets(Asset... assets);
    // Update multiple entries with one call.

    @Update
    abstract Integer updateAssets_synchronous(Asset... assets);

    @Delete
    abstract Maybe<Integer> deleteAssets(Asset... assets);

    @Query("SELECT * from asset LIMIT 1")
    public abstract Asset getAnyAsset();

    @Query("select symbol from asset where uid = :uid")
    public abstract List<String> getSymbolFromUid(long uid);

    //Get all assets matching the symbol - could be multiple in the case of the same asset traded on different exchanges
    @Query("SELECT * from asset where symbol = :symbol")
    public abstract Single<List<AssetExchange>> getAllAssetsWithSymbol(String symbol);

    //Get all assets matching
    @Query("SELECT * from asset where uid = :uid")
    public abstract AssetExchange getAssetExchange(long uid);

    @Query("SELECT * from asset where symbol = :symbol and exchangeMarketIdentifierCode = :exchangeMIC")
    public abstract Single<AssetExchange> getAssetOnExchange(String symbol, String exchangeMIC);

    @Query("SELECT * from asset where symbol = :symbol and exchangeMarketIdentifierCode = :exchangeMIC")
    public abstract Asset getAssetOnExchange_synchronous(String symbol, String exchangeMIC);

    @Query("SELECT * from asset where lastPurchaseDate NOT NULL ORDER BY lastPurchaseDate DESC LIMIT 20")
    abstract Maybe<List<AssetExchange>> getRecentAssets();

}
