package com.drogers.investmentassistant.db;

import androidx.room.Embedded;
import androidx.room.Relation;

import com.drogers.investmentassistant.db.entity.Asset;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;

import java.util.ArrayList;
import java.util.List;


public class AssetExchange {

    public AssetExchange(){
        exchanges = new ArrayList<>(1);
    }

    @Embedded
    public Asset asset;

    @Relation(parentColumn = "exchangeMarketIdentifierCode", entityColumn = "marketIdentifierCode")
    public List<Exchange> exchanges;

    //exchanges will only ever have one exchange - slight workaround to take advantage of the convenience of @Relation
    public Exchange getExchange(){
        return exchanges.get(0);
    }

    public void setExchange(Exchange exchange){
        if(exchanges.size() == 0){
            exchanges.add(exchange);
        }else {
            exchanges.set(0, exchange);
        }
    }
}
