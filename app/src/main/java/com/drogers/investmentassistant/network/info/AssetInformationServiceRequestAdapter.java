package com.drogers.investmentassistant.network.info;

import com.drogers.investmentassistant.db.entity.Asset;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.reactivex.subjects.UnicastSubject;
import timber.log.Timber;

public abstract class AssetInformationServiceRequestAdapter {

    public abstract UnicastSubject<Asset> makeStockInfoRequest(String ticker, Exchange exchange);

    public abstract BigDecimal getStockClosingPriceAsOfDate(String ticker, Exchange exchange, LocalDate date);

    public abstract BigDecimal getStockLatestClosePrice(String ticker, Exchange exchange);

    public abstract UnicastSubject<List<Exchange>> getSupportedExchanges();

    static boolean isRequestStringValid(String s){
        if(s.isEmpty()){
            Timber.e("Request String is empty!");
            return false;
        }
        Pattern p = Pattern.compile("[^a-zA-Z0-9-]");
        Matcher m = p.matcher(s);
        if(m.find()){
            Timber.e("Request string is invalid: " + s);
            return false;
        }
        return true;
    }
}
