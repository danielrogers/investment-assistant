package com.drogers.investmentassistant.network.info.exchanges;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.drogers.investmentassistant.db.RoomDb;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;
import com.drogers.investmentassistant.db.entity.exchange.ExchangeRepository;
import com.drogers.investmentassistant.network.AssetParseException;
import com.drogers.investmentassistant.network.info.BaseIEXRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.UnicastSubject;
import timber.log.Timber;

public class IEXSupportedExchangesRequest extends BaseIEXRequest<UnicastSubject<List<Exchange>>> {

    private ExchangeRepository repository;

    public IEXSupportedExchangesRequest(RequestQueue queue, String apiKey, Context context) {
        super(queue, apiKey, null, null);
        repository = new ExchangeRepository(RoomDb.getDatabase(context));
    }

    private String buildRequestURL() {
        //sample: https://cloud.iexapis.com/stable/ref-data/exchanges?token=TOKEN
        //  https://sandbox.iexapis.com/stable/ref-data/exchanges?token=TOKEN
        String requestURL = iexServiceUrl + "/ref-data/exchanges?token=" +
                apiKey;
        logRequest("Request URL: " + requestURL);
        return requestURL;
    }


    @Override
    public UnicastSubject<List<Exchange>> makeRequest() {
        uid = System.currentTimeMillis();
        UnicastSubject<List<Exchange>> subject = UnicastSubject.create();
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                buildRequestURL(),
                createOnResponseListener(subject),
                createOnErrorListener(subject));
        queue.add(stringRequest);
        return subject;
    }

    private Response.Listener<String> createOnResponseListener(UnicastSubject<List<Exchange>> subject){
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                handleValidResponseFromServer(subject, response);
            }
        };
    }

    /**
     * Handles a 200 response from the remote service.  An Exchange is parsed from the returned JSON
     * and published to the specified Subject
     * @param subject
     * @param response
     */
    private void handleValidResponseFromServer(UnicastSubject<List<Exchange>> subject, String response){
        logRequest("Valid response from server: " + response);
        List<Exchange> exchanges = parseResponse(response);
        if(exchanges == null){
            subject.onError(new AssetParseException("Response parsing failed"));
            return;
        }
        Maybe<List<Long>> insertionResults = repository.insertIfNotExists(exchanges);
        //not concerned about disposing this, as it's not likely to linger in the background
        //for a long period of time
        Disposable d = insertionResults
                .subscribeOn(Schedulers.computation())
                .subscribe(longs -> {
                    //potentially as even if all the candidates for insertion were skipped, we'll still end up here
                    Timber.d("Potentially wrote exchanges to the DB");
                    subject.onNext(exchanges);
                    subject.onComplete();
                }, throwable -> {
                    Timber.e("Error writing exchanges to the DB: " + throwable.getMessage());
                    throwable.printStackTrace();
                    subject.onError(throwable);
                });
    }

    private Response.ErrorListener createOnErrorListener(UnicastSubject<List<Exchange>> subject){
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                logRequest("Error response from server: " + error.getMessage());
                subject.onError(error);
            }
        };
    }


    /**
     * Parses an Asset from a response string. Will return null if parsing fails
     * @param responseString
     * @return
     */
    List<Exchange> parseResponse(String responseString) {
        List<Exchange> parsedExchanges = new ArrayList<>();
        try {
            JSONArray responseArray = new JSONArray(responseString);
            if(responseArray.length() == 0){
                Timber.e("No exchanges found!");
                return null;
            }
            for(int i = 0; i < responseArray.length(); i++){
                JSONObject exchangeJson = responseArray.getJSONObject(i);
                Exchange exchange = new Exchange();
                exchange.marketIdentifierCode = exchangeJson.getString("mic");
                exchange.name = exchangeJson.getString("description");
                exchange.suffix = exchangeJson.getString("exchangeSuffix");
                exchange.iexSearchCode = exchangeJson.getString("exchange");
                parsedExchanges.add(exchange);
            }
            return parsedExchanges;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }


}
