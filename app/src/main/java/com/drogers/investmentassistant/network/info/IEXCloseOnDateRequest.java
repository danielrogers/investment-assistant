package com.drogers.investmentassistant.network.info;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import timber.log.Timber;

class IEXCloseOnDateRequest extends BaseIEXRequest<BigDecimal> {

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyyMMdd");
    private LocalDate date;

    IEXCloseOnDateRequest(RequestQueue q, String apiKey, String ticker, Exchange exchange, LocalDate date) {
        super(q, apiKey, exchange, ticker);
        if(isOnWeekend(date)){
            Timber.d("Original date of: " + date + " is a weekend, adjusting request parameters");
            this.date = adjustDateForNonWorkingDay(date);
        }else{
            this.date = date;
        }
    }

    private boolean isOnWeekend(LocalDate date){
        DayOfWeek dayOfWeek = date.getDayOfWeek();
        return dayOfWeek == DayOfWeek.SUNDAY || dayOfWeek == DayOfWeek.SATURDAY;
    }

    /**
     * Subtracts dates from the given date until it's a weekday - will always
     * take at least one day away.
     * @param date
     * @return
     */
    private LocalDate adjustDateForNonWorkingDay(LocalDate date){
        int daysToSubtract;
        switch(date.getDayOfWeek()){
            case SUNDAY:
                daysToSubtract = 2;
                break;
            case MONDAY:
                daysToSubtract = 3;
                break;
            default:
                daysToSubtract = 1;
                break;
        }
        return date.plusDays(-daysToSubtract);
    }

    protected BigDecimal makeRequest() {
        uid = System.currentTimeMillis();
        RequestFuture<String> future = RequestFuture.newFuture();
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                buildRequestURL(),
                future,
                future);
        queue.add(stringRequest);

        try {
            String response = future.get(5, TimeUnit.SECONDS);
            return parseResponse(response);
        } catch (InterruptedException | TimeoutException | ExecutionException e) {
            logRequest("Could not complete request " + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    private String formatDate(LocalDate date){
        return date.format(DATE_FORMATTER);
    }

    private String buildRequestURL() {
        /*note: this should work with a filter to only return close price, but there seems to be a bug with the endpoint.
        if filtering is applied, it returns latest price data, and ignores the date param
        https://cloud.iexapis.com/beta/stock/gwre/chart/date/20190220?chartByDay=true&token=TOKEN
        */
        String requestURL = iexServiceUrl + "/stock/" +
                tickerWithExchangeSuffix +
                "/chart/date/" + formatDate(date) + "?chartByDay=true&token=" +
                apiKey;
        logRequest("URL: " + requestURL);
        return requestURL;
    }

    //todo test this on a weekday
    BigDecimal parseResponse(String responseString){
        logRequest("Response: " + responseString);
        BigDecimal parsedValue;
        try {
            JSONArray responseArray = new JSONArray(responseString);
            if(responseArray.length() == 0){
                logRequest("No results returned, assuming public holiday. Querying for most recent weekday");
                return new IEXCloseOnDateRequest(queue, apiKey, ticker, exchange, adjustDateForNonWorkingDay(this.date)).makeRequest();
            }
            JSONObject responseObject = responseArray.getJSONObject(0);
            String close = responseObject.getString("close");
            parsedValue = new BigDecimal(close);
            return parsedValue;
        } catch (JSONException | NumberFormatException e) {
            logRequest("Error parsing JSON");
            e.printStackTrace();
            return null;
        }
    }

}
