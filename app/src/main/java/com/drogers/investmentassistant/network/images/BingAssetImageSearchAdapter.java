package com.drogers.investmentassistant.network.images;

import com.microsoft.azure.cognitiveservices.search.imagesearch.BingImageSearchAPI;
import com.microsoft.azure.cognitiveservices.search.imagesearch.BingImageSearchManager;
import com.microsoft.azure.cognitiveservices.search.imagesearch.models.ImageObject;
import com.microsoft.azure.cognitiveservices.search.imagesearch.models.ImagesModel;

import io.reactivex.Observable;

public class BingAssetImageSearchAdapter implements AssetImageSearchAdapter {

    private static final String SUBSCRIPTION_KEY = "9ded57c4f02348528a8f32153ed23fa9";
    private String searchTerm;

    public BingAssetImageSearchAdapter(String searchTerm){
        this.searchTerm = searchTerm;
    }
    /**
     * Runs a search against the given search term plus " logo", and returns an Observable which will emit
     * the URL of the first hit, and then complete. Can emit errors.
     * @return
     */
    public Observable<String> getCompanyImageUrl(){
        return Observable.create(subscriber -> {
            subscriber.onNext(mapImageModel(getImageResults()));
            subscriber.onComplete();
        });
    }

    private String mapImageModel(ImagesModel imagesModel){
        if(imagesModel != null && imagesModel.value().size() > 0){
            // Image results
            ImageObject firstImageResult = imagesModel.value().get(0);
            return firstImageResult.contentUrl();
        }
        throw new NoImageResultsException("No image results returned from service!");
    }

    ImagesModel getImageResults(){
        BingImageSearchAPI client = BingImageSearchManager.authenticate(SUBSCRIPTION_KEY);
        return client.bingImages().search(searchTerm + " logo", null);
    }
}
