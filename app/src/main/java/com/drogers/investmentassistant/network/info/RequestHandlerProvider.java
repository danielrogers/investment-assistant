package com.drogers.investmentassistant.network.info;

import android.content.Context;

public class RequestHandlerProvider {

    public static RequestHandlerType requestHandlerToUse = RequestHandlerType.IEX_PROD_SERVICE;

    /**
     * Returns either the service or dummy implementation
     * @param context If null, dummy is always returned
     * @return
     */
    public static AssetInformationServiceRequestAdapter getAssetInfoRequestAdapterImplementation(Context context){
        if(context == null || requestHandlerToUse == RequestHandlerType.DUMMY_SERVICE){
            return new DummyRequestHandler();
        }
        return IEXRequestHandler.getInstance(context);
    }

}
