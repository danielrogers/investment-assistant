package com.drogers.investmentassistant.network.images;

import android.app.Application;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.widget.ImageView;

import com.drogers.investmentassistant.R;
import com.drogers.investmentassistant.db.entity.Asset;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class PicassoAssetImageLoader implements ImageLoader {
    private static final Bitmap.CompressFormat IMAGE_COMPRESSION_FORMAT = Bitmap.CompressFormat.PNG;
    private String cacheDir;
    private ImageView imageView;
    private Asset asset;

    /**
     * Implementation of ImageLoader which loads an image and manages caching. Custom caching is used
     * in replace of Picasso caching, due to limitations with how Picasso caches - Picasso
     * uses URLs as cache keys. In order to minimize calls to the search api, we'd have to store the
     * URLs after the initial request was made, in order to get future cache hits (even if we had an unlimited service,
     * there's no guarantee the same url will be returned every time a search is ran). Although we would have
     * these URLs stored, we couldn't then depend on them if the cache were to be cleared. <br>
     * The more straightforward implementation is to simply store the image on disk, using the asset symbol as a file name
     * @param imageView
     * @param asset
     * @param application
     */
    public PicassoAssetImageLoader(ImageView imageView, Asset asset, Application application){
        this.imageView = imageView;
        this.asset = asset;
        this.cacheDir = application.getCacheDir() + "/";
    }

    @Override
    public void loadImage() {
        Bitmap image = loadCachedImage(asset.symbol);
        if(image != null){
            Timber.d("Cached image found, loading local file");
            imageView.setImageBitmap(image);
        }else{
            Timber.d("No cached image found");
            loadFromRemote();
        }
    }

    private void loadFromRemote(){
        Timber.d("Fetching image from remote");
        Observable<String> url = new BingAssetImageSearchAdapter(asset.name).getCompanyImageUrl();
        //no need to dispose d as it is effectively a Single; it calls onComplete immediately
        //after it's first emission
        Disposable d = url.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::doPicassoLoad, throwable -> {
                    throwable.printStackTrace();
                    Timber.e(throwable.getMessage());
                });
    }

    private void doPicassoLoad(String s){
        Picasso p = Picasso.get();
        p.setLoggingEnabled(true);
        p.load(s)
                .centerInside()
                //.networkPolicy(NetworkPolicy.NO_CACHE)
                //.memoryPolicy(MemoryPolicy.NO_CACHE)
                .resize(imageView.getWidth(), imageView.getHeight())
                .placeholder(R.drawable.placeholder)
                .into(imageView, getPicassoLoadCallbacks());
    }

    /**
     * Generates onSuccess and onError callbacks for when Picasso completes loading into the specified imageView
     * @return
     */
    private Callback getPicassoLoadCallbacks(){
        return new Callback() {
            @Override
            public void onSuccess() {
                cacheImage(getBitmapFromImageView(imageView), asset);
            }
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                Timber.e(e.getMessage());
            }
        };
    }

    private Bitmap getBitmapFromImageView(ImageView imageView){
        BitmapDrawable drawable = (BitmapDrawable) imageView.getDrawable();
        return drawable.getBitmap();
    }

    private Bitmap loadCachedImage(String assetName) {
        return BitmapFactory.decodeFile(cacheDir + assetName + "." + IMAGE_COMPRESSION_FORMAT.toString());
    }

    private void cacheImage(Bitmap image, Asset asset) {
        String fileName = asset.symbol + "." + IMAGE_COMPRESSION_FORMAT.toString();
        File tempFile = new File( cacheDir + fileName);
        try (FileOutputStream out = new FileOutputStream(tempFile)) {
            image.compress(IMAGE_COMPRESSION_FORMAT, 100, out);
            Timber.d("Image cached to " + fileName);
        } catch (IOException e) {
            Timber.e("Caching image failed!");
        }
    }

}

