package com.drogers.investmentassistant.network.images;

interface ImageLoader {
    void loadImage();
}
