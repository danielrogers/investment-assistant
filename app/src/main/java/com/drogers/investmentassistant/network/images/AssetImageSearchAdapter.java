package com.drogers.investmentassistant.network.images;
import io.reactivex.Observable;
import io.reactivex.subjects.UnicastSubject;

/*
Searches for an image and provides a url to the first search result
 */
public interface AssetImageSearchAdapter {
    Observable<String> getCompanyImageUrl();
}
