package com.drogers.investmentassistant.network;

public class AssetParseException extends Exception {
    public AssetParseException(String s){
        super(s);
    }
}
