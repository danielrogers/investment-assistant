package com.drogers.investmentassistant.network.info;

import android.util.Log;

import com.android.volley.RequestQueue;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;

import io.reactivex.subjects.UnicastSubject;
import timber.log.Timber;

public abstract class BaseIEXRequest<T> {

    static final protected String IEX_URL = "https://cloud.iexapis.com/stable";
    static final protected String IEX_SANDBOX_URL = "https://sandbox.iexapis.com/stable";
    String ticker;
    protected String tickerWithExchangeSuffix;
    protected String apiKey;
    protected RequestQueue queue;
    protected String iexServiceUrl;
    protected long uid;
    protected Exchange exchange;

    protected BaseIEXRequest(RequestQueue queue, String apiKey, Exchange exchange, String ticker){
        this.ticker = ticker;
        this.apiKey = apiKey;
        this.queue = queue;
        this.exchange = exchange;
        if(exchange != null && exchange.suffix != null){
            this.tickerWithExchangeSuffix = ticker + exchange.suffix;
        }else{
            this.tickerWithExchangeSuffix = ticker;
        }
        iexServiceUrl = getIEXUrl();
    }

    private String getIEXUrl(){
        if(RequestHandlerProvider.requestHandlerToUse == RequestHandlerType.IEX_SANDBOX){
            return IEX_SANDBOX_URL;
        }
        return IEX_URL;
    }

    protected abstract T makeRequest();

    protected void logRequest(String message){
        Timber.d("Request #" + uid + " " + message);
    }
}
