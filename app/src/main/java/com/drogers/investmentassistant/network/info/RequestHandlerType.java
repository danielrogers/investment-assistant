package com.drogers.investmentassistant.network.info;

public enum RequestHandlerType {
    IEX_SANDBOX,
    DUMMY_SERVICE,
    IEX_PROD_SERVICE;
}
