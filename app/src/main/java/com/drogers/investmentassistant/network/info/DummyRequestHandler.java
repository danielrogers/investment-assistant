package com.drogers.investmentassistant.network.info;

import com.android.volley.ClientError;
import com.android.volley.Header;
import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.drogers.investmentassistant.db.entity.Asset;
import com.drogers.investmentassistant.db.entity.AssetType;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.NotImplementedException;

import io.reactivex.subjects.UnicastSubject;


public class DummyRequestHandler extends AssetInformationServiceRequestAdapter {

    //use this when you want the request to throw an error
    public static String ERROR_TICKER = "ERROR_TICKER";
    //should this handler throw an error when the list of supported exchanges is queried
    public static boolean throwErrorOnExchangeRequest;

    private Exchange exchange;

    public DummyRequestHandler(){

    }

    /**
     * Any assets returned form this will have their description starting with REMOTE.  Use ERROR_TICKER to force an exception
     * @param ticker
     * @param exchange
     * @return
     */
    @Override
    public UnicastSubject<Asset> makeStockInfoRequest(String ticker, Exchange exchange) {
        this.exchange = exchange;
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return parseRequest(ticker);
    }

    @Override
    public BigDecimal getStockClosingPriceAsOfDate(String ticker, Exchange exchange, LocalDate date)  {
        if(ticker.equals(ERROR_TICKER)){
            throw new RuntimeException("ERROR_TICKER requested");
        }
        return null;
    }

    private UnicastSubject<Asset> parseRequest(String request){
        UnicastSubject<Asset> subject = UnicastSubject.create();
        if(request.equals(ERROR_TICKER)){
            VolleyError error = new ClientError(new NetworkResponse(
                    404,
                    new byte[1],
                    false,
                    0,
                    Collections.<Header>emptyList()));
            subject.onError(error);
        }else{
            Asset a = new Asset(request, AssetType.DUMMY, "REMOTE: " + request, request, exchange.marketIdentifierCode);
            subject.onNext(a);
        }
        return subject;
    }

    @Override
    public BigDecimal getStockLatestClosePrice(String ticker, Exchange exchange) {
        return null;
    }

    @Override
    public UnicastSubject<List<Exchange>> getSupportedExchanges() {
        UnicastSubject<List<Exchange>> subject = UnicastSubject.create();
        if(throwErrorOnExchangeRequest){
            subject.onError(new Exception("Kaboom"));
            return subject;
        }
        ArrayList<Exchange> exchanges = new ArrayList<>();
        exchanges.add(buildDummyDublinExchange());
        exchanges.add(buildDummyNYSEExchange());
        subject.onNext(exchanges);
        subject.onComplete();
        return  subject;
    }

    public static Exchange buildDummyNYSEExchange(){
        Exchange exchange = new Exchange();
        exchange.suffix = null;
        exchange.name = "New York Stock Testing Exchange";
        exchange.marketIdentifierCode = "nyseTest";
        exchange.iexSearchCode = "";
        return exchange;
    }

    public static Exchange buildDummyDublinExchange(){
        Exchange exchange = new Exchange();
        exchange.suffix = null;
        exchange.name = "Euronext Dublin Testing Exchange";
        exchange.marketIdentifierCode = "dubTest";
        exchange.iexSearchCode = "";
        return exchange;
    }

}
