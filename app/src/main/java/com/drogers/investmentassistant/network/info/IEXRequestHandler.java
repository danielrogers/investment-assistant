package com.drogers.investmentassistant.network.info;

import android.content.Context;
import android.util.AndroidRuntimeException;

import com.drogers.investmentassistant.InvestmentAssistantApplication;
import com.drogers.investmentassistant.db.entity.Asset;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;
import com.drogers.investmentassistant.network.info.exchanges.IEXSupportedExchangesRequest;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;

import io.reactivex.subjects.UnicastSubject;

public class IEXRequestHandler extends AssetInformationServiceRequestAdapter {
    private static volatile IEXRequestHandler INSTANCE;
    private String apiKey;
    private RequestQueue requestQueue;

    private IEXRequestHandler(RequestQueue q, Context c) {
        requestQueue = q;
    }

    static IEXRequestHandler getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (IEXRequestHandler.class) {
                if (INSTANCE == null) {
                    INSTANCE = new IEXRequestHandler(Volley.newRequestQueue(context.getApplicationContext()), context);
                }
            }
        }
        return INSTANCE;
    }

    private String getApiKey(){
        if(apiKey == null){
            apiKey = readAPIKey(InvestmentAssistantApplication.getContext());
        }
        return apiKey;
    }

    static String readAPIKey(Context context){
        try{
            String fileName;
            if(RequestHandlerProvider.requestHandlerToUse == RequestHandlerType.IEX_SANDBOX)
                fileName = "test_api_key.txt";
            else
                fileName = "api_key.txt";
            InputStream is = context.getAssets().open(fileName);
            Scanner in = new Scanner(is);
            return in.nextLine();
        } catch (IOException e) {
            e.printStackTrace();
            throw new AndroidRuntimeException("API Key could not be read");
        }
    }


    /**
     * @param ticker Stock symbol.
     * @param exchange Exchange being searched
     * @return Subject which results will be published to if successfully retrieved.
     *          If an asset is found at the remote, it is committed. This saves
     *          from running future requests.  Errors can be emitted if the service can't be reached,
     *          or parsing fails
     */
    @Override
    public UnicastSubject<Asset> makeStockInfoRequest(String ticker, Exchange exchange) {
        return new IEXInfoRequest(requestQueue, getApiKey(), ticker, exchange, InvestmentAssistantApplication.getContext()).makeRequest();
    }

    @Override
    public BigDecimal getStockClosingPriceAsOfDate(String ticker, Exchange exchange, LocalDate date) {
        return new IEXCloseOnDateRequest(requestQueue, getApiKey(), ticker, exchange, date).makeRequest();
    }

    @Override
    public BigDecimal getStockLatestClosePrice(String ticker, Exchange exchange) {
        return new IEXLatestClosingPriceRequest(requestQueue, getApiKey(), exchange, ticker).makeRequest();
    }

    @Override
    public UnicastSubject<List<Exchange>> getSupportedExchanges() {
        return new IEXSupportedExchangesRequest(requestQueue, getApiKey(), InvestmentAssistantApplication.getContext()).makeRequest();
    }

}
