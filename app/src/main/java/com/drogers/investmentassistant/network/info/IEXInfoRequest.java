package com.drogers.investmentassistant.network.info;

import android.content.Context;
import android.util.Log;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.drogers.investmentassistant.R;
import com.drogers.investmentassistant.db.RoomDb;
import com.drogers.investmentassistant.db.entity.Asset;
import com.drogers.investmentassistant.db.entity.AssetRepository;
import com.drogers.investmentassistant.db.entity.AssetType;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;
import com.drogers.investmentassistant.network.AssetParseException;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.spec.ECField;
import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.UnicastSubject;

class IEXInfoRequest extends BaseIEXRequest<UnicastSubject<Asset>>  {

    Context context;
    AssetRepository repository;

    IEXInfoRequest(RequestQueue q, String apiKey, String ticker, Exchange exchangeBeingSearched, Context context) {
        super(q, apiKey, exchangeBeingSearched, ticker);
        this.context = context;
        repository = new AssetRepository(context, RoomDb.getDatabase(context).assetDAO());
    }

    private String buildRequestURL() {
        //sample: https://sandbox.iexapis.com/stable/stock/gwre/batch?types=company,quote&filter=symbol,companyName,description,issueType,close&token=TOKEN
        //sample: https://cloud.iexapis.com/stable/stock/gwre/batch?types=company,quote&filter=symbol,companyName,description,issueType,close&token=TOKEN
        String requestURL = iexServiceUrl + "/stock/" +
                tickerWithExchangeSuffix +
                "/batch?types=company,quote&filter=symbol,companyName,description,issueType,close&token=" +
                apiKey;
        logRequest("Request URL: " + requestURL);
        return requestURL;
    }

    protected UnicastSubject<Asset> makeRequest() {
        uid = System.currentTimeMillis();
        UnicastSubject<Asset> subject = UnicastSubject.create();
        if(!AssetInformationServiceRequestAdapter.isRequestStringValid(ticker)){
            subject.onError(new Exception("Request string is invalid!"));
            return subject;
        }
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                buildRequestURL(),
                createOnResponseListener(subject),
                createOnErrorListener(subject));
        queue.add(stringRequest);
        return subject;
    }

    private Response.Listener<String> createOnResponseListener(UnicastSubject<Asset> subject){
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                handleValidResponseFromServer(subject, response);
            }
        };
    }

    /**
     * Handles a 200 response from the remote service.  An Asset is parsed from the returned JSON
     * and published to the specified Subject
     * @param subject
     * @param response
     */
    private void handleValidResponseFromServer(UnicastSubject<Asset> subject, String response){
        logRequest("Valid response from server: " + response);
        Asset parsedAsset = parseAssetFromResponse(response);
        if(parsedAsset == null){
            subject.onError(new AssetParseException("Asset parsing failed"));
            return;
        }
        Maybe<List<Long>> insertionResults = repository.insert(parsedAsset);
        //not concerned about disposing this, as it's not likely to linger in the background
        //for a long period of time
        Disposable d = insertionResults
                .subscribeOn(Schedulers.computation())
                .subscribe(_uidList -> {
                    //make sure to update the uid, as it won't have been previously set
                    parsedAsset.uid = _uidList.get(0);
                    subject.onNext(parsedAsset);
                    subject.onComplete();
                });
    }


    private Response.ErrorListener createOnErrorListener(UnicastSubject<Asset> subject){
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                logRequest("Error response from server: " + error.getMessage());
                subject.onError(error);
            }
        };
    }

    /**
     * Parses an Asset from a response string. Will return null if parsing fails
     * @param responseString
     * @return
     */
    Asset parseAssetFromResponse(String responseString) {
        Asset parsedAsset = new Asset();
        try {
            JSONObject responseObject = new JSONObject(responseString);
            JSONObject companyInfo = responseObject.getJSONObject("company");
            parsedAsset.name = companyInfo.getString("companyName");
            parseAssetType(companyInfo.getString("issueType"), parsedAsset);
            String description = companyInfo.getString("description");
            if(description == null || description.isEmpty()){
                description = context.getResources().getString(R.string.misc_no_desc_available);
            }
            parsedAsset.description = description;
            //parsedAsset.symbol = companyInfo.getString("symbol"); don't use the returned symbol as it will contain the suffix
            parsedAsset.symbol = ticker;
            parsedAsset.exchangeMarketIdentifierCode = exchange.marketIdentifierCode;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return parsedAsset;
    }

    /**
     * Parses given string into an AssetType
     * @param s cs (Common Stock) for STOCK
     * @param a Asset whose assetType field will be written to
     */
    private void parseAssetType(String s, Asset a){
        if(s.equals("cs")){
            a.assetType = AssetType.STOCK;
        }else{
            a.assetType = AssetType.DUMMY;
        }
    }

}
