package com.drogers.investmentassistant.network.images;

public class NoImageResultsException extends RuntimeException {
    public NoImageResultsException(String s){
        super(s);
    }
}
