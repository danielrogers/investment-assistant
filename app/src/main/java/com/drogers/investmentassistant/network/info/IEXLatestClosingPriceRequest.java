package com.drogers.investmentassistant.network.info;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import timber.log.Timber;

public class IEXLatestClosingPriceRequest extends BaseIEXRequest<BigDecimal> {

    IEXLatestClosingPriceRequest(RequestQueue q, String apiKey, Exchange exchange, String ticker) {
        super(q, apiKey, exchange, ticker);
    }

    private String buildRequestURL() {
        //https://sandbox.iexapis.com/stable/stock/gwre/batch?types=quote&filter=close&token=TOKEN
        //https://cloud.iexapis.com/stable/stock/gwre/batch?types=quote&filter=close&token=TOKEN
        String requestURL = iexServiceUrl + "/stock/" +
                tickerWithExchangeSuffix +
                "/batch?types=quote&filter=close&token=" +
                apiKey;
        logRequest(requestURL);
        return requestURL;
    }

    protected BigDecimal makeRequest() {
        uid = System.currentTimeMillis();
        RequestFuture<String> future = RequestFuture.newFuture();
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                buildRequestURL(),
                future,future);
        queue.add(stringRequest);

        try {
            String response = future.get(5, TimeUnit.SECONDS);
            return parseResponse(response);
        } catch (InterruptedException | TimeoutException | ExecutionException e) {
            Timber.e("Could not complete request " + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    BigDecimal parseResponse(String responseString){
        logRequest("Response: " + responseString);
        BigDecimal parsedValue;
        try {
            JSONObject responseObject = new JSONObject(responseString);
            JSONObject quoteInfo = responseObject.getJSONObject("quote");
            //todo this is a big dirty kludge, do not let this into production!!
            String close = quoteInfo.getString("close");
            if(close.equals("null")){
                parsedValue = new BigDecimal(100);
            }else {
                parsedValue = new BigDecimal(close);
            }
            return parsedValue;
        } catch (JSONException e) {
            Timber.e("Error parsing JSON");
            e.printStackTrace();
            return null;
        }catch(NumberFormatException nfe){
            Timber.e("Error parsing JSON");
            nfe.printStackTrace();
            return null;
        }
    }
}
