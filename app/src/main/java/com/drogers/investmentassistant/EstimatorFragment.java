package com.drogers.investmentassistant;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.drogers.investmentassistant.databinding.FragmentEstimatorBinding;
import com.drogers.investmentassistant.db.RoomDb;


/**
 * A simple {@link Fragment} subclass.
 */
public class EstimatorFragment extends RefreshableFragment implements View.OnClickListener {

    private FragmentEstimatorBinding binding;
    public EstimatorFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_estimator, container, false);
        binding.clearDBButton.setOnClickListener(this);
        binding.sampleDataButton.setOnClickListener(this);
        // Inflate the layout for this fragment
        return binding.getRoot();
    }

    private void clearDbHandler(){
        RoomDb.clearDB(RoomDb.getDatabase(this.getContext()));
    }


    private void loadSampleDataHandler(){
        //load asset
        //load exchange

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.clearDBButton:
                clearDbHandler();
                break;
            case R.id.sampleDataButton:
                loadSampleDataHandler();
                break;
        }
    }

    @Override
    protected void handleRefresh() {

    }
}
