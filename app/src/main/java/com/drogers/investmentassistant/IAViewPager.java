package com.drogers.investmentassistant;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.jjoe64.graphview.GraphView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;

/**
 * Custom view pager which allows a child GraphView to scroll horizontally
 */
public class IAViewPager extends ViewPager {
    public IAViewPager(@NonNull Context context) {
        super(context);
    }

    public IAViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected boolean canScroll(View v, boolean checkV, int dx, int x, int y) {
        if(v instanceof GraphView)
            return true;
        return super.canScroll(v, checkV, dx, x, y);
    }
}
