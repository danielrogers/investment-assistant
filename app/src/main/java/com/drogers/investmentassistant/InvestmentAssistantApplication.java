package com.drogers.investmentassistant;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import com.drogers.investmentassistant.logging.FileLoggerTree;
import com.squareup.leakcanary.LeakCanary;

import timber.log.Timber;

public class InvestmentAssistantApplication extends Application {

    public static volatile Application application;

    @Override
    public void onCreate(){
        super.onCreate();
        registerActivityLifecycleCallbacks(createLifeCycleCallbacks());
        application = this;
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        //LeakCanary.install(this);
        Timber.plant(new FileLoggerTree());
    }

    public static Context getContext() {
        return application.getApplicationContext();
    }

    private ActivityLifecycleCallbacks createLifeCycleCallbacks(){
        return new ActivityLifecycleCallbacks() {

            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

            }

            @Override
            public void onActivityStarted(Activity activity) {

            }

            @Override
            public void onActivityResumed(Activity activity) {

            }

            @Override
            public void onActivityPaused(Activity activity) {

            }

            @Override
            public void onActivityStopped(Activity activity) {

            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {

            }
        };
    }

}
