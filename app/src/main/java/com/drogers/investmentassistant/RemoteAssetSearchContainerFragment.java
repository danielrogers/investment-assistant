package com.drogers.investmentassistant;


import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.drogers.investmentassistant.databinding.FragmentRemoteAssetSearchContainerBinding;

public class RemoteAssetSearchContainerFragment extends Fragment {

    public FragmentRemoteAssetSearchContainerBinding binding;

    public RemoteAssetSearchContainerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_remote_asset_search_container, container, false);
        return binding.getRoot();
    }

}
