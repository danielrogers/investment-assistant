package com.drogers.investmentassistant;

public abstract class RefreshableFragment extends IABoilerPlateFragment {

    private boolean needsRefresh;

    protected abstract void handleRefresh();

    void setNeedsRefresh(){
        this.needsRefresh = true;
    }

    void refresh(){
        if(needsRefresh) {
            handleRefresh();
            needsRefresh = false;
        }
    }
}
