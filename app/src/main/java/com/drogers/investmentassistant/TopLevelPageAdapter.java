package com.drogers.investmentassistant;

import com.drogers.investmentassistant.ui.indepth.InDepthFragment;
import com.drogers.investmentassistant.ui.overview.OverviewFragment;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class TopLevelPageAdapter extends FragmentStatePagerAdapter {

    private int numberOfTabs;
    private List<RefreshableFragment> fragments;

    TopLevelPageAdapter(FragmentManager fm, int numberOfTabs) {
        super(fm);
        this.numberOfTabs = numberOfTabs;
        fragments = new ArrayList<>();
        fragments.add(new OverviewFragment());
        fragments.add(new InDepthFragment());
        fragments.add(new EstimatorFragment());
    }

    void markFragmentForRefresh(int i){
        fragments.get(i).setNeedsRefresh();
    }

    List<RefreshableFragment> getFragments(){
        return fragments;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch(position) {
            case 0:
                return "Overview";
            case 1:
                return "In Depth";
            case 2:
                return "Estimator";
        }
        return null;
    }


    @Override
    public Fragment getItem(int i) {
        return fragments.get(i);
    }

    @Override
    public int getCount() {
        return numberOfTabs;
    }
}
