package com.drogers.investmentassistant.logging;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Logger;

import timber.log.BuildConfig;
import timber.log.Timber;

public class FileLoggerTree extends Timber.DebugTree {
    private static final String LOG_TAG = FileLoggerTree.class.getSimpleName();
    private static final String LOGGING_DIR = "com.drogers.investmentassistant.logging";
    private String fileNameTimeStamp = new SimpleDateFormat(
            "dd-MM-yyyy",
            Locale.getDefault()).format(new Date());

    @Override
    protected void log(int priority, String tag, String message, Throwable t) {
        logToConsole(priority, tag, message, t);
        try {
            String logTimeStamp = new SimpleDateFormat(
                    "HH:mm:ss:SSS",
                    Locale.getDefault()).format(new Date());
            File file = generateFile(LOGGING_DIR, fileNameTimeStamp + ".txt");

            // If file created or exists save logs
            if (file != null) {
                try (FileWriter writer = new FileWriter(file, true)) {
                    writer.append("\n")
                            .append(logTimeStamp)
                            .append("\t")
                            .append(tag)
                            .append("\t")
                            .append(message);
                    writer.flush();
                }
            }
        } catch (Exception e) {
            Log.wtf(LOG_TAG,"Error while logging: " + e);
        }
    }

    private void logToConsole(int priority, String tag, String message, Throwable t){
        if(t != null){
            switch (priority){
                case Log.ERROR:
                    Log.e(tag, message, t);
                    break;
                case Log.INFO:
                    Log.i(tag, message, t);
                    break;
                case Log.WARN:
                    Log.w(tag, message, t);
                    break;
                case Log.VERBOSE:
                    Log.v(tag, message, t);
                case Log.DEBUG:
                default:
                    Log.d(tag, message, t);
                    break;
            }
        }else{
            switch (priority){
                case Log.VERBOSE:
                    Log.v(tag, message);
                    break;
                case Log.ERROR:
                    Log.e(tag, message);
                    break;
                case Log.INFO:
                    Log.i(tag, message);
                    break;
                case Log.WARN:
                    Log.w(tag, message);
                    break;
                case Log.DEBUG:
                default:
                    Log.d(tag, message);
                    break;
            }
        }
    }

    @Override
    protected String createStackElementTag(StackTraceElement element) {
        // Add log statements line number to the log
        return super.createStackElementTag(element) + " - " + element.getLineNumber();
    }

    private File generateFile(String path, String fileName) {
        File file = null;
        if (isExternalStorageAvailable()) {
            File root = new File(Environment.getExternalStorageDirectory().getAbsolutePath(),
                    File.separator + path);

            boolean dirExists = true;

            if (!root.exists()) {
                dirExists = root.mkdirs();
            }

            if (dirExists) {
                file = new File(root, fileName);
            }
        }
        return file;
    }

    private boolean isExternalStorageAvailable() {
        return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
    }
}
