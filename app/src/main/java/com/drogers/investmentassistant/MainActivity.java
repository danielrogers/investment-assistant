package com.drogers.investmentassistant;

import android.os.Bundle;
import android.transition.Fade;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.drogers.investmentassistant.databinding.ActivityMainBinding;
import com.drogers.investmentassistant.ui.Utils;
import com.google.android.material.tabs.TabLayout;
import com.idescout.sql.SqlScoutServer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentTransaction;
import androidx.room.util.DBUtil;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setUpTransition();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        initialiseToolbar();
        setUpTabLayout();
        SqlScoutServer.create(this, getPackageName());//todo production?
    }

    private void setUpTransition(){
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        Fade fade = new Fade();
        fade.setDuration(1L);
        fade.excludeTarget(android.R.id.statusBarBackground, true);
        fade.excludeTarget(android.R.id.navigationBarBackground, true);
        getWindow().setEnterTransition(fade);
        getWindow().setExitTransition(fade);
        getWindow().setSharedElementsUseOverlay(false);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void closeFabMenu(View view) {
        Utils.closeFabMenu(view);
    }

    private void initialiseToolbar() {
        Toolbar toolbar = binding.toolbar;
        setSupportActionBar(toolbar);
    }

    private void setUpTabLayout() {
        TabLayout tabLayout = binding.tabLayout;
        tabLayout.addTab(tabLayout.newTab().setText("Overview"));
        tabLayout.addTab(tabLayout.newTab().setText("In Depth"));
        tabLayout.addTab(tabLayout.newTab().setText("Estimator"));
        // Set the tabs to fill the entire layout.
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        // Use PagerAdapter to manage page views in fragments.
        final ViewPager viewPager = binding.pager;
        final PagerAdapter adapter = new TopLevelPageAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        addOnTabSelectedListener(tabLayout, viewPager);
        addOnPageChangeListener(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }

    public void markOverviewFragmentForRefresh(){
        ((TopLevelPageAdapter) binding.pager.getAdapter()).markFragmentForRefresh(0);
    }


    private void addOnTabSelectedListener(final TabLayout tabLayout, final ViewPager viewPager) {
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    //if the popup fab menu needs to be minimized when scrolling, could be done here.
    private void addOnPageChangeListener(final ViewPager viewPager) {
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
                //Utils.closeFabMenu(viewPager.getRootView());
            }

            @Override
            public void onPageSelected(int i) {
                ((TopLevelPageAdapter) viewPager.getAdapter()).getFragments().get(i).refresh();
                viewPager.setCurrentItem(i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }
}
