package com.drogers.investmentassistant;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import io.reactivex.disposables.CompositeDisposable;
/**
 * Extension of AppCompatActivity with added methods for Disposable cleanup
 */
public class IABoilerPlateActivity extends AppCompatActivity {

    protected CompositeDisposable compositeDisposable;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        compositeDisposable = new CompositeDisposable();
    }


    @Override
    protected void onDestroy(){
        super.onDestroy();
        compositeDisposable.dispose();
    }
}
