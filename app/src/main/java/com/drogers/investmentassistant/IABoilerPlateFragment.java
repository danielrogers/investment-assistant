package com.drogers.investmentassistant;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.CallSuper;
import androidx.fragment.app.Fragment;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Extension of fragment with added methods for Disposable cleanup
 */
public class IABoilerPlateFragment extends Fragment {

    protected CompositeDisposable compositeDisposable;
    @Override
    @CallSuper
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        compositeDisposable = new CompositeDisposable();
        return null;
    }


    @Override
    public void onDestroyView(){
        super.onDestroyView();
        compositeDisposable.dispose();
    }

    public CompositeDisposable getCompositeDisposable(){
        return compositeDisposable;
    }

}
