package com.drogers.investmentassistant.ui.newpurchase.step2;

import android.app.Application;

import com.drogers.investmentassistant.BR;
import com.drogers.investmentassistant.R;
import com.drogers.investmentassistant.db.entity.Asset;
import com.drogers.investmentassistant.db.entity.transaction.Purchase;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;
import com.drogers.investmentassistant.ui.DefaultObservable;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import androidx.annotation.NonNull;
import androidx.databinding.Bindable;
import androidx.databinding.PropertyChangeRegistry;
import androidx.lifecycle.AndroidViewModel;

public class Step2ViewModel extends AndroidViewModel implements DefaultObservable {

    static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("d MMMM y");
    private Asset assetBeingPurchased;
    private Exchange exchangeBeingUsed;
    Purchase purchaseTransaction;
    private PropertyChangeRegistry callbacks = new PropertyChangeRegistry();

    public Step2ViewModel(@NonNull Application application) {
        super(application);
    }

    /**
     * Initialise a new purchase, based on the given asset
     * @param a
     */
    void initNewPurchase(Asset a, Exchange e){
        exchangeBeingUsed = e;
        assetBeingPurchased = a;
        purchaseTransaction = new Purchase();
        purchaseTransaction.assetUid = a.uid;
        purchaseTransaction.date = LocalDate.now();
        purchaseTransaction.totalQuantity = BigDecimal.ZERO;
        purchaseTransaction.remainingActiveUnits = BigDecimal.ZERO;
    }

    @Bindable
    public Asset getAssetBeingPurchased() {
        return assetBeingPurchased;
    }

    public Exchange getExchangeBeingUsed() {
        return exchangeBeingUsed;
    }

    /**
     *
     * @param date
     * @return true is date successfully set, else false
     */
    void setPurchaseDate(LocalDate date){
        purchaseTransaction.date = date;
        notifyPropertyChanged(BR.purchaseDate);
    }

    @Bindable
    public String getPurchaseDate(){
        if(purchaseTransaction.date != null)
            return purchaseTransaction.date.format(dateTimeFormatter);
        return "";
    }

    @Bindable
    public String getAmountBeingPurchasedAsString(){
        if(purchaseTransaction.totalQuantity.equals(BigDecimal.ZERO)){
            return "";
        }
        return String.valueOf(purchaseTransaction.totalQuantity);
    }

    public void setAmountBeingPurchasedAsString(String amount){
        if(amount == null || amount.isEmpty() || amount.equals(".")){
            purchaseTransaction.totalQuantity = BigDecimal.ZERO;
            purchaseTransaction.remainingActiveUnits = BigDecimal.ZERO;
        }else{
            purchaseTransaction.totalQuantity = new BigDecimal(amount);
            purchaseTransaction.remainingActiveUnits = new BigDecimal(amount);
        }
    }

    @Bindable
    public String getPurchasingUnitPriceAsString(){
        if(purchaseTransaction.assetPrice == null || purchaseTransaction.assetPrice.equals(BigDecimal.ZERO)){
            return "";
        }
        return String.valueOf(purchaseTransaction.assetPrice);
    }

    public void setPurchasingUnitPriceAsString(String amount){
        if(amount == null || amount.isEmpty() || amount.equals(".")){
            purchaseTransaction.assetPrice = null;
        }else{
            purchaseTransaction.assetPrice = new BigDecimal(amount);
        }
    }

    /**
     * Validates that all fields required for a purchase to be recorded are set correctly
     * @throws Exception
     */
    void validatePurchase() throws Exception{
        if(purchaseTransaction.date == null){
            throw new Exception(getString(R.string.error_new_purchase_invalid_date));
        }
        if(purchaseTransaction.totalQuantity.equals(BigDecimal.ZERO)){
            throw new Exception(getString(R.string.error_new_purchase_invalid_quantity));
        }
        if(purchaseTransaction.assetPrice == null || purchaseTransaction.assetPrice.equals(BigDecimal.ZERO)){
            throw new Exception(getString(R.string.error_new_purchase_invalid_price));
        }
    }

    @Override
    public PropertyChangeRegistry getCallbacks() {
        return callbacks;
    }

    private String getString(int resId){
        return getApplication().getResources().getString(resId);
    }
}
