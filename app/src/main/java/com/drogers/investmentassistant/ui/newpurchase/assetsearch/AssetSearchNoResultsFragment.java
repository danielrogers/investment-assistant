package com.drogers.investmentassistant.ui.newpurchase.assetsearch;


import android.content.Intent;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.drogers.investmentassistant.R;
import com.drogers.investmentassistant.databinding.FragmentAssetSearchNoResultsBinding;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;
import com.drogers.investmentassistant.ui.newpurchase.NewPurchaseKeys;
import com.drogers.investmentassistant.ui.newpurchase.newasset.CreateNewAssetActivity;


/**
 * A simple {@link Fragment} subclass.
 */
public class AssetSearchNoResultsFragment extends Fragment implements View.OnClickListener {

    //The string that was used in the search leading to this fragment being shown
    private String searchString;
    private FragmentAssetSearchNoResultsBinding dataBinding;
    private String exchangeName;
    private Exchange exchange;

    public AssetSearchNoResultsFragment() {
        // Required empty public constructor
    }

    void setSearchParams(String searchString, Exchange exchange){
        this.searchString = searchString;
        this.exchangeName = exchange.name;
        this.exchange = exchange;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_asset_search_no_results, container, false);
        dataBinding.createNewAssetButton.setOnClickListener(this);
        setText();
        return dataBinding.getRoot();
    }

    private void setText(){
        dataBinding.noResultsFoundTextView.setText(getString(R.string.asset_search_no_results_text, searchString, exchangeName));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.create_new_asset_button) {
            showCreateNewAssetActivity(v);
        }
    }

    private void showCreateNewAssetActivity(View v){
        Intent intent = new Intent(v.getContext(), CreateNewAssetActivity.class);
        intent.putExtra(NewPurchaseKeys.CREATE_NEW_ASSET_TICKER_KEY, searchString);
        intent.putExtra(NewPurchaseKeys.CREATE_NEW_ASSET_EXCHANGE_KEY, exchange);
        v.getContext().startActivity(intent);
    }
}
