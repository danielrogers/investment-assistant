package com.drogers.investmentassistant.ui.newpurchase.assetsearch.remote;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.drogers.investmentassistant.R;
import com.drogers.investmentassistant.databinding.AssetSearchRemoteExchangesTemplateBinding;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;
import com.drogers.investmentassistant.ui.newpurchase.NewPurchaseKeys;
import com.drogers.investmentassistant.ui.newpurchase.assetsearch.AssetSearchStep2Activity;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public class ExchangeListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LayoutInflater inflater;
    private List<Exchange> exchangesToDisplay;
    private String searchTerm;
    private ExchangeViewHolder lastChecked;

    public ExchangeListAdapter(Context context, String searchTerm){
        inflater = LayoutInflater.from(context);
        this.searchTerm = searchTerm;
        exchangesToDisplay = new ArrayList<>();
    }

    void setExchangesToDisplay(List<Exchange> newExchanges){
        newExchanges.sort((o1, o2) -> o1.name.compareToIgnoreCase(o2.name));
        exchangesToDisplay = newExchanges;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AssetSearchRemoteExchangesTemplateBinding binding =
                DataBindingUtil.inflate(inflater, R.layout.asset_search_remote_exchanges_template, parent, false);
        return new ExchangeViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ExchangeViewHolder exchangeViewHolder = (ExchangeViewHolder) holder;
        exchangeViewHolder.binding.exchangeRadioButton.setTag(position);
        exchangeViewHolder.binding.setExchange(exchangesToDisplay.get(position));
    }

    @Override
    public int getItemCount() {
        return exchangesToDisplay.size();
    }

    public class ExchangeViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        AssetSearchRemoteExchangesTemplateBinding binding;

        ExchangeViewHolder(AssetSearchRemoteExchangesTemplateBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.exchangeRadioButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            resetPreviousSelection();
            Exchange e = binding.getExchange();
            Timber.d("Selected exchange: " + e.marketIdentifierCode);
            Intent intent = new Intent(inflater.getContext(), AssetSearchStep2Activity.class);
            intent.putExtra(NewPurchaseKeys.REMOTE_ASSET_SEARCH_EXCHANGE_KEY, e);
            intent.putExtra(NewPurchaseKeys.REMOTE_ASSET_SEARCH_TERM_KEY, searchTerm);
            v.getContext().startActivity(intent);
        }

        private void resetPreviousSelection(){
            if(lastChecked != null){
                lastChecked.binding.exchangeRadioButton.setChecked(false);
            }
            lastChecked = this;
        }
    }
}
