package com.drogers.investmentassistant.ui;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.drogers.investmentassistant.R;
import com.drogers.investmentassistant.ui.newpurchase.assetsearch.AssetSearchStep1Activity;
import com.github.clans.fab.FloatingActionMenu;

import androidx.annotation.StringRes;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;

public class Utils {

    public static void closeFabMenu(View v){
        FloatingActionMenu fab = v.findViewById(R.id.new_transaction_fab);
        //if(fab != null)
        fab.close(false);
    }

    /**
     * Sets up the action bar for the current screen. Make sure to call this after any databinding calls!
     * @param context
     * @param toolbarTitle
     */
    public static void initialiseToolbar(AppCompatActivity context, @StringRes int toolbarTitle) {
        Toolbar toolbar = context.findViewById(R.id.toolbar);
        context.setSupportActionBar(toolbar);
        ActionBar actionbar = context.getSupportActionBar();
        if(actionbar != null){
            // Enable the Up button
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setTitle(toolbarTitle);
        }
    }

    /**
     * Sets up the action bar for the current screen. Make sure to call this after any databinding calls!
     * @param context
     * @param toolbarTitle
     */
    public static void initialiseToolbar(AppCompatActivity context, String toolbarTitle) {
        Toolbar toolbar = context.findViewById(R.id.toolbar);
        context.setSupportActionBar(toolbar);
        ActionBar actionbar = context.getSupportActionBar();
        if(actionbar != null){
            // Enable the Up button
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setTitle(toolbarTitle);
        }
    }

    /**
     * Sets up the action bar for the current screen, and then sets up the SearchView layout.
     * @param context
     * @param toolbarTitle
     */
    public static void initialiseSearchToolbar(AppCompatActivity context, @StringRes int toolbarTitle, boolean provideSuggestions){
        initialiseToolbar(context, toolbarTitle);
        setUpSearchViewLayoutParameters(context);
        setSearchableInfoForSearchView(context, provideSuggestions);

    }

    /**
     * Links the searchView to the appropriate activity, and sets up search suggestions if needed
     * @param activity
     * @param provideSuggestions
     */
    private static void setSearchableInfoForSearchView(AppCompatActivity activity, boolean provideSuggestions){
        SearchManager searchManager = (SearchManager) activity.getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = activity.findViewById(R.id.search_view);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(new ComponentName(activity, AssetSearchStep1Activity.class)));
        if(!provideSuggestions)
            searchView.setSuggestionsAdapter(null);
    }

    /**
     * Makes the search icon sit to the right of the actionbar when not expanded, and fully
     * extend to use as much of the toolbar as possible when expanded
     * @param activity
     */
    private static void setUpSearchViewLayoutParameters(AppCompatActivity activity){
        //orient the icon to the right of the toolbar when not searching, when searching use as much screen width as possible
        SearchView searchView = activity.findViewById(R.id.search_view);
        searchView.setLayoutParams(new Toolbar.LayoutParams(Gravity.END));
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setLayoutParams(new Toolbar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, Gravity.END));
                alertIfNoNetwork(v.getContext());
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                searchView.setLayoutParams(new Toolbar.LayoutParams(Gravity.END));
                return false;
            }
        });
    }

    /**
     * Displays a toast warning the user if there is no network connection
     */
    private static void alertIfNoNetwork(Context context){
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if(activeNetworkInfo == null || !activeNetworkInfo.isConnected()){
            Toast.makeText(context, "No network available, search results will be incomplete.", Toast.LENGTH_LONG).show();
        }
    }
}
