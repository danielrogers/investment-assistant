package com.drogers.investmentassistant.ui.newpurchase.assetlist;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.drogers.investmentassistant.R;
import com.drogers.investmentassistant.databinding.FragmentAssetsDetailListBinding;
import com.drogers.investmentassistant.db.AssetExchange;
import com.drogers.investmentassistant.db.entity.Asset;

import java.util.List;

/**
 * Fragment which shows a list of assets.  Currently used in the following places:
 *  New Purchase step 1, to show recent assets
 *  Asset Search Step 1, to show search results
 */
public class AssetsDetailsListFragment extends Fragment {
    private FragmentAssetsDetailListBinding dataBinding;
    private AssetListAdapter recentAssetListAdapter;
    private AssetListItemSelectedHandler assetListItemSelectedHandler;

    public static AssetsDetailsListFragment newInstance(AssetListItemSelectedHandler assetListItemSelectedHandler){
        AssetsDetailsListFragment fragment = new AssetsDetailsListFragment();
        fragment.assetListItemSelectedHandler = assetListItemSelectedHandler;
        return fragment;
    }


    public AssetsDetailsListFragment() {
    }

    public void setAssetExchangesToDisplay(List<AssetExchange> assetsToDisplay) {
        recentAssetListAdapter.setAssetExchangesToDisplay(assetsToDisplay);
        recentAssetListAdapter.notifyDataSetChanged();
    }

    private void initialiseRecyclerView(){
        RecyclerView recyclerView = dataBinding.newPurchaseRecyclerView;
        recentAssetListAdapter = new AssetListAdapter(this.getContext(), assetListItemSelectedHandler);
        recyclerView.setAdapter(recentAssetListAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_assets_detail_list, container, false);
        initialiseRecyclerView();
        return dataBinding.getRoot();
    }
}
