package com.drogers.investmentassistant.ui.newpurchase.step3;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.test.espresso.idling.CountingIdlingResource;

import io.reactivex.android.schedulers.AndroidSchedulers;
import timber.log.Timber;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.drogers.investmentassistant.IABoilerPlateActivity;
import com.drogers.investmentassistant.MainActivity;
import com.drogers.investmentassistant.R;
import com.drogers.investmentassistant.databinding.ActivityNewPurchaseStep3Binding;
import com.drogers.investmentassistant.db.entity.Asset;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;
import com.drogers.investmentassistant.db.entity.transaction.Purchase;
import com.drogers.investmentassistant.ui.Utils;
import com.drogers.investmentassistant.ui.newpurchase.NewPurchaseKeys;

/**
 * Step 3 of the New Purchase Wizard.  On this step data is fetched to update records
 * pertaining to the purchase and the affected entities are committed.
 */
public class NewPurchaseActivityStep3 extends IABoilerPlateActivity {

    private ActivityNewPurchaseStep3Binding dataBinding;
    Step3ViewModel viewModel;
    private Purchase purchaseRecord;
    private Asset assetBeingPurchased;
    private Exchange exchangeBeingUsed;
    private final CountingIdlingResource idlingResource = new CountingIdlingResource("NewPurchaseActivityStep3");

    /**
     * Idling resource is used while the entities to commit are being generated
     */
    public CountingIdlingResource getIdlingResource() {
        return idlingResource;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_purchase_step3);
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_new_purchase_step3);
        getIntentData();
        viewModel = ViewModelProviders.of(this).get(Step3ViewModel.class);
        startUpdate(false);
        Utils.initialiseToolbar(this, R.string.new_purchase_step_3_update_in_progress);
    }

    private void startUpdate(boolean isRetry){
        generateEntitiesToCommit(isRetry);
        setProgess(0);
    }

    private void setProgess(Integer i){
        if(i >= 100){
            dataBinding.progressTestView.setText(getString(R.string.new_purchase_step_3_completed));
        }else {
            dataBinding.progressTestView.setText(getString(R.string.new_purchase_step_3_percentage_progress, i.toString()));
        }
        dataBinding.progressBar2.setProgress((i));
    }

    private void getIntentData(){
        Intent i = getIntent();
        purchaseRecord = i.getParcelableExtra(NewPurchaseKeys.STEP_3_PURCHASE_EXTRA_KEY);
        assetBeingPurchased = i.getParcelableExtra(NewPurchaseKeys.STEP_3_ASSET_EXTRA_KEY);
        exchangeBeingUsed = i.getParcelableExtra(NewPurchaseKeys.EXCHANGE_EXTRA_KEY);
        if(purchaseRecord.assetUid != assetBeingPurchased.uid){
            throw new RuntimeException("Conflicting data passed to NewPurchaseActivityStep3!");
        }
    }


    /**
     * Generates all entities that need to be committed for this purchaseRecord. No data is committed here.
     */
    public void generateEntitiesToCommit(boolean isRetry){
        idlingResource.increment();
        compositeDisposable.add(viewModel
                .generatePurchaseData(purchaseRecord, assetBeingPurchased, exchangeBeingUsed, isRetry)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onEntityGenerationNext,
                        this::handleUpdateError,
                        this::onEntityGenerationComplete));
    }

    private void onEntityGenerationNext(Integer i){
        setProgess(i);
    }

    private void handleUpdateError(Throwable t){
        Timber.e(t.getMessage());
        t.printStackTrace();
        showUpdateErrorDialog(t);
        idlingResource.decrement();
    }

    private void onEntityGenerationComplete() {
        if (viewModel.getUpdateErrorStatus() == PortfolioUpdateErrorStatus.ERROR_INCOMPLETE_DATA_FETCH) {
            //show dialog
            showConfirmCancelDialog(getString(R.string.new_purchase_step_3_dialog_backdated_purchase), true);
            //should decrement this here so that tests can pick up the above dialog. If user goes for the commit option, re-increment
            idlingResource.decrement();
        }else if(viewModel.getUpdateErrorStatus() == PortfolioUpdateErrorStatus.ERROR_CUSTOM_ASSET_USED) {
            showConfirmCancelDialog(getString(R.string.new_purchase_step_3_dialog_custom_asset), false);
            idlingResource.decrement();
        }else if (viewModel.getUpdateErrorStatus() == PortfolioUpdateErrorStatus.NO_ERROR) {
            //commit the actual data
            commitData();
        }else{
            throw new RuntimeException("Viewmodel should not have entered this state!");
        }
    }

    private void showConfirmCancelDialog(String text, boolean offerRetry){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.new_purchase_step_3_dialog_warning));
        builder.setMessage(text);

        // add the buttons
        builder.setPositiveButton(getString(R.string.new_purchase_step_3_dialog_accept_changes), (dialog, which) -> {
            idlingResource.increment();
            commitData();
        });
        builder.setNegativeButton(getString(R.string.new_purchase_step_3_dialog_cancel_changes), (dialog, which) -> goToOverviewScreen());
        if(offerRetry) {
            builder.setNeutralButton(getString(R.string.new_purchase_step_3_dialog_retry_update), (dialog, which) -> startUpdate(true));
        }

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    /**
     * Starts the activity for the overview screen in a new task
     */
    private void goToOverviewScreen(){
        startActivity(new Intent(getApplicationContext(), MainActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
    }

    private void showUpdateErrorDialog(Throwable t){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.new_purchase_step_3_dialog_error));
        builder.setMessage(getString(R.string.new_purchase_step_3_dialog_error_info) + t.getLocalizedMessage());

        // add the buttons
        builder.setPositiveButton(getString(R.string.new_purchase_step_3_dialog_ok), (dialog, which) -> goToOverviewScreen());
        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }


    private void commitData(){
        compositeDisposable.add(viewModel
                .commitData()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        _void -> {},
                        this::handleUpdateError,
                        () -> {
                            setProgess(100);
                            dataBinding.finishButton.setEnabled(true);
                            idlingResource.decrement();
                        }));
    }

    public void onFinishButtonClicked(View v){
        goToOverviewScreen();
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(this, getResources().getString(R.string.new_purchase_step_3_cant_go_back), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        viewModel.getDisposable().dispose();
    }

}
