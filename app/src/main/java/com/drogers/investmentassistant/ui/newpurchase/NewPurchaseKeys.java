package com.drogers.investmentassistant.ui.newpurchase;

public class NewPurchaseKeys {
    public static final String ASSET_EXTRA_KEY =
            "com.drogers.investmentassistant.ui.newpurchase.assetsearch.AssetSearchResultsFoundFragment.ASSET_EXTRA";
    public static final String EXCHANGE_EXTRA_KEY =
            "com.drogers.investmentassistant.ui.newpurchase.assetsearch.AssetSearchResultsFoundFragment.EXCHANGE_EXTRA";
    public static final String CREATE_NEW_ASSET_TICKER_KEY =
            "com.drogers.investmentassistant.ui.newpurchase.assetsearch.AssetSearchNoResultsFragment.CREATE_NEW_ASSET_TICKER_KEY";
    public static final String CREATE_NEW_ASSET_EXCHANGE_KEY =
            "com.drogers.investmentassistant.ui.newpurchase.assetsearch.AssetSearchNoResultsFragment.CREATE_NEW_ASSET_EXCHANGE_KEY";
    public static final String STEP_3_PURCHASE_EXTRA_KEY =
            "com.drogers.investmentassistant.ui.newpurchase.step3.NewPurchaseActivityStep3.STEP_3_PURCHASE_EXTRA_KEY";
    public static final String STEP_3_ASSET_EXTRA_KEY =
            "com.drogers.investmentassistant.ui.newpurchase.step3.NewPurchaseActivityStep3.STEP_3_ASSET_EXTRA_KEY";
    public static final String REMOTE_ASSET_SEARCH_EXCHANGE_KEY =
            "com.drogers.investmentassistant.ui.newpurchase.assetsearch.REMOTE_ASSET_SEARCH_EXCHANGE_KEY";
    public static final String REMOTE_ASSET_SEARCH_TERM_KEY =
            "com.drogers.investmentassistant.ui.newpurchase.assetsearch.REMOTE_ASSET_SEARCH_TERM_KEY";
}
