package com.drogers.investmentassistant.ui.newpurchase.newasset;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.drogers.investmentassistant.R;
import com.drogers.investmentassistant.databinding.ActivityCreateNewAssetBinding;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;
import com.drogers.investmentassistant.ui.Utils;
import com.drogers.investmentassistant.ui.newpurchase.NewPurchaseKeys;
import com.drogers.investmentassistant.ui.newpurchase.step1.NewPurchaseActivityStep1;

public class CreateNewAssetActivity extends AppCompatActivity {

    ActivityCreateNewAssetBinding dataBinding;
    NewAssetViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_asset);
        viewModel = ViewModelProviders.of(this).get(NewAssetViewModel.class);
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_create_new_asset);
        dataBinding.setViewmodel(viewModel);
        Utils.initialiseToolbar(this, R.string.create_new_asset_toolbar_title);
        handleIntent();
    }

    private void handleIntent(){
        Intent i = getIntent();
        String assetSymbol = i.getStringExtra(NewPurchaseKeys.CREATE_NEW_ASSET_TICKER_KEY);
        Exchange exchange = i.getParcelableExtra(NewPurchaseKeys.CREATE_NEW_ASSET_EXCHANGE_KEY);
        viewModel.setAssetExchange(exchange);
        viewModel.setAssetSymbol(assetSymbol);
    }

    public void clickSaveButton(View v){
        try{
            viewModel.saveAsset(v.getContext());
            finish();
            startActivity(new Intent(getApplicationContext(), NewPurchaseActivityStep1.class)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        }catch (AssetVerificationException ave){
            Toast
                    .makeText(this, ave.getLocalizedMessage(), Toast.LENGTH_LONG)
                    .show();
        }
    }

}
