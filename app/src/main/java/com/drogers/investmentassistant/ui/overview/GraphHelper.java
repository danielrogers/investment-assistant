package com.drogers.investmentassistant.ui.overview;

import android.graphics.Color;

import com.drogers.investmentassistant.db.entity.GraphableValue;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

class GraphHelper {

    private List<? extends ArrayList<? extends GraphableValue>> graphableValues;
    //the first data point which should be graphed.  GraphableValues could be prepended with y=0,
    // values older than this, if it's relatively recent
    private GraphableValue firstPointOnGraph;
    private LocalDate oneYearAgo;
    private List<Entry> entriesToGraph;
    private LineChart lineChart;

    GraphHelper(List<? extends ArrayList<? extends GraphableValue>> graphableValues, LineChart chart){
        this.graphableValues = graphableValues;
        this.lineChart = chart;
        firstPointOnGraph = graphableValues.get(0).get(0);
        oneYearAgo = LocalDate.now().plusYears(-1);
    }

    private void calculateEntriesToGraph(){
        //if there aren't enough values to graph over the last 12 months, generate flat line to
        // fill the graphing window
        LocalDate firstDataPointTime = firstPointOnGraph.getX();
        int ageOfFirstDataPointInMonths = Math.abs((int) ChronoUnit.MONTHS.between(LocalDate.now(), firstDataPointTime));;
        if(ageOfFirstDataPointInMonths < 12){
            entriesToGraph = generateZeroedEntries();
        }else{

            entriesToGraph = new ArrayList<>();
        }

        entriesToGraph.add(calculateFirstDataPoint(graphableValues));
        //remaining data points
        for(int i = 1; i < graphableValues.size(); i++){
            List<? extends GraphableValue> valuesForThisDate = graphableValues.get(i);
            entriesToGraph.add(
                    new Entry(
                            valuesForThisDate.get(0).getX().toEpochDay(),
                            (float) sumYValues(valuesForThisDate).doubleValue())
            );
        }
    }

    private void formatGraph(LineDataSet dataSet){
        dataSet.setColor(Color.RED);
        dataSet.setValueTextColor(Color.BLACK);

        // no description text
        lineChart.getDescription().setEnabled(false);
        lineChart.setDrawGridBackground(false);
        lineChart.getLegend().setEnabled(false);

        //position x axis to the bottom
        XAxis x = lineChart.getXAxis();
        x.setPosition(XAxis.XAxisPosition.BOTTOM);

        //disable right y axis
        YAxis rightY = lineChart.getAxisRight();
        rightY.setEnabled(false);

        //set curve properties
        dataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        dataSet.setCubicIntensity(0.05f);
        dataSet.setDrawFilled(true);
        dataSet.setLineWidth(2f);
        dataSet.setDrawCircles(false);
        dataSet.setDrawValues(false);

        lineChart.animateXY(500, 500);
        lineChart.setTouchEnabled(false);
        setUpXAxisValueFormatter(lineChart);
    }

    private void setUpXAxisValueFormatter(LineChart chart){
        ValueFormatter formatter = new ValueFormatter() {
            @Override
            public String getAxisLabel(float value, AxisBase axis) {
                return LocalDate.ofEpochDay((long)value).format(DateTimeFormatter.ofPattern("MMM-yy"));
            }
        };
        XAxis xAxis = chart.getXAxis();
        xAxis.setValueFormatter(formatter);
    }

    void draw(){
        calculateEntriesToGraph();

        LineDataSet dataSet = new LineDataSet(entriesToGraph, "");
        formatGraph(dataSet);
        LineData lineData = new LineData(dataSet);
        lineChart.setData(lineData);
        lineChart.invalidate(); // refresh
    }

    /**
     * Generates a series of Entries with y=0 and x values in increments of one week.  These Entries go
     * for the date of firstPointOnGraph to today minus 1 year
     * Used when the first point on the graph is x > 0
     * @return
     */
    private List<Entry> generateZeroedEntries(){
        LocalDate realDataStartDate = firstPointOnGraph.getX();
        ArrayList<Entry> entries = new ArrayList<>();

        LocalDate zeroedDate = oneYearAgo;

        do{
            entries.add(new Entry(zeroedDate.toEpochDay(),0f));
            zeroedDate = zeroedDate.plusWeeks(1);
        }while(zeroedDate.compareTo(realDataStartDate) <= 0);

        return entries;
    }

    /**
     * First data point (x=0) is an interpolated point.  This is done
     * to allow the graph to render a little cleaner, due to some features
     * of GraphView
     */
    private Entry calculateFirstDataPoint(List<? extends ArrayList<? extends GraphableValue>> graphableValues){
        LocalDate firstDataPointTime = graphableValues.get(0).get(0).getX();
        BigDecimal firstDataPointTotalValue = sumYValues(graphableValues.get(0));
        LocalDate oneYearAgo = LocalDate.now().plusYears(-1);
        long oneYearAgoMillisecondsValue = Date.from(oneYearAgo.atStartOfDay(ZoneId.systemDefault()).toInstant()).getTime();
        boolean firstDataPointLessThanOneYearAgo = firstDataPointTime.isAfter(oneYearAgo);

        if(graphableValues.size() <= 1 || firstDataPointLessThanOneYearAgo)
            return new Entry(
                    firstDataPointTime.toEpochDay(),
                    (float)firstDataPointTotalValue.doubleValue());

        LocalDate secondDataPointTime = graphableValues.get(1).get(0).getX();
        BigDecimal secondDataPointTotalValue = sumYValues(graphableValues.get(1));
        //calc midpoint
        //in order to keep the graph neat (due to the way GraphView can format it's viewport),
        // a datapoint is generated to be placed at the beginning of the graph (graph x = 0)
        // This datapoint needs to be calculated so it has an accurate Y value
        long timeStampOfFirstSummary = Date.from(firstDataPointTime.atStartOfDay(ZoneId.systemDefault()).toInstant()).getTime();
        long timeStampOfSecondSummary = Date.from(secondDataPointTime.atStartOfDay(ZoneId.systemDefault()).toInstant()).getTime();

        long timeFromFirstToGraphStart = oneYearAgoMillisecondsValue - timeStampOfFirstSummary;
        long timeFromGraphStartToSecond = timeStampOfSecondSummary - oneYearAgoMillisecondsValue;

        long timeBetweenFirstAndSecond = timeFromFirstToGraphStart + timeFromGraphStartToSecond;
        double percentageOfLineAfterGraphStart = (double) timeFromGraphStartToSecond / (double) timeBetweenFirstAndSecond * 100d;

        double firstYValue = firstDataPointTotalValue.doubleValue();
        double secondYValue = secondDataPointTotalValue.doubleValue();
        double largestYValue, smallestYValue;
        if(firstYValue > secondYValue){
            largestYValue = firstYValue;
            smallestYValue = secondYValue;
        }else{
            largestYValue = secondYValue;
            smallestYValue = firstYValue;
        }
        double yValueDiff = largestYValue - smallestYValue;
        double actualYValue = smallestYValue + (yValueDiff * (percentageOfLineAfterGraphStart / 100));
        return new Entry(oneYearAgo.toEpochDay(), (float)actualYValue);
    }

    /**
     * Sums all of Y values in the given collection of GraphableValues
     * @param graphableValues
     * @return
     */
    private BigDecimal sumYValues(List<? extends GraphableValue> graphableValues){
        BigDecimal sumForThisDate = new BigDecimal(0);
        for(int e = 0; e < graphableValues.size(); e++){
            GraphableValue nextGraphableValue = graphableValues.get(e);
            sumForThisDate = sumForThisDate.add(nextGraphableValue.getY());
        }
        return sumForThisDate;
    }
}
