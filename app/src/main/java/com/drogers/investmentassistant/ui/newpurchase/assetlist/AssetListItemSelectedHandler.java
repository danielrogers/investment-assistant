package com.drogers.investmentassistant.ui.newpurchase.assetlist;

import com.drogers.investmentassistant.db.entity.Asset;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;

public interface AssetListItemSelectedHandler {
    void onItemSelected(Asset a, Exchange e);
}
