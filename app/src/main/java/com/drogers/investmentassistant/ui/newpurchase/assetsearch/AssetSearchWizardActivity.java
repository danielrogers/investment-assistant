package com.drogers.investmentassistant.ui.newpurchase.assetsearch;


import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.drogers.investmentassistant.IABoilerPlateActivity;
import com.drogers.investmentassistant.R;
import com.drogers.investmentassistant.RemoteAssetSearchContainerFragment;

/**
 * Contains convenience methods for loading commonly used Fragments relating to Asset searching
 */
public abstract class AssetSearchWizardActivity extends IABoilerPlateActivity {

    protected SearchResultsViewModel viewModel;

    protected RemoteAssetSearchContainerFragment loadContainerFragment(){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        RemoteAssetSearchContainerFragment containerFragment = new RemoteAssetSearchContainerFragment();
        transaction.replace(R.id.fragment_holder_layout, containerFragment);
        transaction.commitNowAllowingStateLoss();
        return containerFragment;
    }

    protected void loadSpinnerFragment(int targetView){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        SpinnerFragment fragment = new SpinnerFragment();
        fragmentTransaction.replace(targetView, fragment);
        fragmentTransaction.commitNow();
    }

}
