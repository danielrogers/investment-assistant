package com.drogers.investmentassistant.ui.indepth;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.drogers.investmentassistant.db.RoomDb;
import com.drogers.investmentassistant.db.entity.AssetOwnershipRecord;
import com.drogers.investmentassistant.db.entity.AssetOwnershipRecordRepository;
import com.drogers.investmentassistant.db.entity.transaction.Purchase;
import com.drogers.investmentassistant.db.entity.transaction.PurchaseAsset;
import com.drogers.investmentassistant.db.entity.transaction.PurchaseRepository;
import com.drogers.investmentassistant.db.entity.transaction.Transaction;

import org.apache.commons.lang3.NotImplementedException;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.subjects.UnicastSubject;
import timber.log.Timber;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

public class InDepthViewModel extends AndroidViewModel {

    private PurchaseRepository purchaseRepository;
    private AssetOwnershipRecordRepository aorRepository;
    private RoomDb database;
    public InDepthViewModel(@NonNull Application application) {
        super(application);
        database = RoomDb.getDatabase(application);
        purchaseRepository = new PurchaseRepository(database.purchaseDAO());
        aorRepository = new AssetOwnershipRecordRepository(database.assetOwnershipRecordDAO());
    }

    public Maybe<List<PurchaseAsset>> getAllPurchasesDesc(){
        return purchaseRepository.getAllPurchasesDesc();
    }

    LocalDate getAssetOwnershipRecordAsOfDate(AssetOwnershipRecord assetOwnershipRecord){
        return assetOwnershipRecord.asOfDate;
    }

    /**
     * Get all AORecords relating to the specified purchase record (i.e. referencing the same
     * asset), grouped by their date
     * @param purchase
     * @return
     */
    LinkedHashMap<LocalDate, List<AssetOwnershipRecord>> getGroupedRecordsForPurchase(Purchase purchase){
        List<AssetOwnershipRecord> aoRecords = aorRepository.getAllRecordsRelatingToAsset(purchase.assetUid, purchase.date);
        return aoRecords.stream().collect(groupingBy(this::getAssetOwnershipRecordAsOfDate, LinkedHashMap::new, toList()));
    }

    /**
     * Determines the effects of deleting amountToDelete from the collection of records
     * Returned ArrayList contains at 0: records to be deleted, and at 1: records to be updated
     */
    ArrayList<List<AssetOwnershipRecord>> calculateAmountDeletionFromRecords(BigDecimal amountToDelete, Map<LocalDate, List<AssetOwnershipRecord>> records){
        List<AssetOwnershipRecord> assetOwnershipRecordsToDelete = new ArrayList<>();
        List<AssetOwnershipRecord> assetOwnershipRecordsToUpdate = new ArrayList<>();
        for(LocalDate key : records.keySet()) {
            BigDecimal remainingAmountToDelete = amountToDelete;
            outerLoop:
            for (AssetOwnershipRecord record : records.get(key)) {
                remainingAmountToDelete = amountToDelete.subtract(record.amountOwned);
                int comparison = remainingAmountToDelete.compareTo(BigDecimal.ZERO);
                switch (comparison) {
                    case (0):
                        //remainingAmountToDelete is 0, purchase has been fully accounted for, record will be deleted
                        assetOwnershipRecordsToDelete.add(record);
                        break outerLoop;
                    case (-1):
                        //remainingAmountToDelete is a negative number, purchase has been fully accounted
                        //for, but this AOR won't be deleted
                        record.amountOwned = remainingAmountToDelete.abs();
                        assetOwnershipRecordsToUpdate.add(record);
                        break outerLoop;
                    case (1):
                        //remainingAmountToDelete is a positive number, this record will be deleted but purchase
                        //isn't fully accounted for; more will be updated
                        assetOwnershipRecordsToDelete.add(record);
                        break;
                }
            }
            if(remainingAmountToDelete.compareTo(BigDecimal.ZERO) > 0){
                String message = "Not enough AORs to delete purchase; this is a serious error!";
                Timber.wtf(message);
                throw new RuntimeException(message);
            }

        }
        ArrayList<List<AssetOwnershipRecord>> recordsToReturn = new ArrayList<>();
        recordsToReturn.add(assetOwnershipRecordsToDelete);
        recordsToReturn.add(assetOwnershipRecordsToUpdate);
        return recordsToReturn;
    }

    Completable deleteTransaction(Transaction transaction){
        return Completable.fromAction(() -> {
            database.runInTransaction(() -> {
                if(transaction instanceof Purchase){
                    Purchase purchase = (Purchase) transaction;
                    purchaseRepository.deletePurchase(purchase);
                    Map<LocalDate, List<AssetOwnershipRecord>> groupedRecords = getGroupedRecordsForPurchase(purchase);
                    List<List<AssetOwnershipRecord>> recordsForDeleteAndUpdate = calculateAmountDeletionFromRecords(purchase.totalQuantity, groupedRecords);
                    aorRepository.delete(recordsForDeleteAndUpdate.get(0));
                    aorRepository.upsert(recordsForDeleteAndUpdate.get(1));
                }else{
                    throw new NotImplementedException("Implement me, and fix this casting!");
                }
            });
        });
    }
}
