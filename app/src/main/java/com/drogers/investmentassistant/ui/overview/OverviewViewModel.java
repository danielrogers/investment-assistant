package com.drogers.investmentassistant.ui.overview;

import android.app.Application;

import com.drogers.investmentassistant.db.RoomDb;
import com.drogers.investmentassistant.db.entity.AssetOwnershipRecord;
import com.drogers.investmentassistant.db.entity.AssetOwnershipRecordRepository;;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import io.reactivex.Maybe;

public class OverviewViewModel extends AndroidViewModel {

    AssetOwnershipRecordRepository assetOwnershipRecordRepository;

    public OverviewViewModel(@NonNull Application application) {
        super(application);
        assetOwnershipRecordRepository = new AssetOwnershipRecordRepository(RoomDb.getDatabase(application).assetOwnershipRecordDAO());
    }

    Maybe<List<AssetOwnershipRecord>> getAORecordsFromLastYearPlus1(){
        return assetOwnershipRecordRepository.getAORecordsFromLastYearPlus1();
    }
}
