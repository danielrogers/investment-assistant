package com.drogers.investmentassistant.ui.indepth;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.drogers.investmentassistant.MainActivity;
import com.drogers.investmentassistant.R;
import com.drogers.investmentassistant.databinding.TransactionDetailsPurchaseRecyclerTemplateBinding;
import com.drogers.investmentassistant.databinding.TransactionDetailsSaleRecyclerTemplateBinding;
import com.drogers.investmentassistant.db.entity.Asset;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;
import com.drogers.investmentassistant.db.entity.transaction.Purchase;
import com.drogers.investmentassistant.db.entity.transaction.PurchaseAsset;
import com.squareup.haha.perflib.Main;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

//todo class needs to support asset sale transactions
public class TransactionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<PurchaseAsset> purchaseAssets;
    private LayoutInflater inflater;
    private static final int PURCHASE_TRANSACTION = 0;
    private static final int SALE_TRANSACTION = 1;
    private InDepthViewModel viewModel;
    private CompositeDisposable parentCompositeDisposable;
    private InDepthFragment parentFragment;

    TransactionAdapter(InDepthFragment parentFragment, InDepthViewModel viewModel) {
        inflater = LayoutInflater.from(parentFragment.getContext());
        purchaseAssets = new ArrayList<>();
        this.viewModel = viewModel;
        this.parentCompositeDisposable = parentFragment.getCompositeDisposable();
        this.parentFragment = parentFragment;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if(i == PURCHASE_TRANSACTION){
            TransactionDetailsPurchaseRecyclerTemplateBinding binding = DataBindingUtil.inflate(inflater, R.layout.transaction_details_purchase_recycler_template, viewGroup, false);
            return new PurchaseTransactionViewHolder(this, binding);
        }else{
            TransactionDetailsSaleRecyclerTemplateBinding binding = DataBindingUtil.inflate(inflater, R.layout.transaction_details_sale_recycler_template, viewGroup, false);
            return new SaleTransactionViewHolder(binding);
        }
    }

    @Override
    public int getItemViewType(int position) {
        //if(purchaseAssets.get(position).purchase instanceof Purchase){
            return PURCHASE_TRANSACTION;
        //}else{
        //    return SALE_TRANSACTION;
       // }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        PurchaseAsset current = purchaseAssets.get(i);
        TransactionViewHolder transactionViewHolder = (TransactionViewHolder) viewHolder;
        transactionViewHolder.resetLayout();
        transactionViewHolder.setTransactionAsset(current);
    }

    @Override
    public int getItemCount() {
        return purchaseAssets.size();
    }

    void setPurchaseAssets(List<PurchaseAsset> purchaseAssets){
        this.purchaseAssets = purchaseAssets;
        notifyDataSetChanged();
    }

    private interface TransactionViewHolder{
        void setTransactionAsset(PurchaseAsset purchaseAsset);
        void resetLayout();
    }

    private static class PurchaseTransactionViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener, TransactionViewHolder {

        private TransactionDetailsPurchaseRecyclerTemplateBinding binding;
        private InDepthViewModel viewModel;
        private CompositeDisposable parentCompositeDisposable;
        private TransactionAdapter parent;
        static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("d MMMM y");

        PurchaseTransactionViewHolder(TransactionAdapter parent, TransactionDetailsPurchaseRecyclerTemplateBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.viewModel = parent.viewModel;
            this.parentCompositeDisposable = parent.parentCompositeDisposable;
            this.parent = parent;
            binding.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

        }

        @Override
        public void setTransactionAsset(PurchaseAsset purchaseAsset) {
            TransactionDetailsPurchaseRecyclerTemplateBinding binding = this.binding;
            Asset asset = purchaseAsset.getAsset();
            Exchange exchange = purchaseAsset.getExchange();
            Context c = binding.assetName.getContext();
            Purchase purchase = purchaseAsset.purchase;
            binding.deleteImageView.setOnClickListener(view -> {
                showDeleteModal(c, purchaseAsset);
            });
            binding.setPurchaseAsset(purchaseAsset);
            binding.assetName.setText(asset.name);
            binding.transactionDetails.setText(buildPurchaseDetailsString(purchase));
            binding.exchangeName.setText(exchange.name);
            String symbol = asset.symbol.toUpperCase();
            binding.transactionHeader.setText(c.getString(R.string.in_depth_transaction_purchase_header, symbol));
        }

        private void showDeleteModal(Context c, PurchaseAsset purchaseToDelete){
            AlertDialog.Builder builder = new AlertDialog.Builder(c);
            builder.setTitle(c.getString(R.string.in_depth_transaction_adapter_delete_this_transaction));
            builder.setMessage(c.getString(R.string.in_depth_transaction_adapter_cannot_be_undone));

            // create and show the alert dialog
            builder.setPositiveButton(c.getString(R.string.delete), (dialog, which) -> {
                runDeletion(purchaseToDelete);
            });
            builder.setNegativeButton(c.getString(R.string.cancel), (dialogInterface, i) -> {dialogInterface.cancel();});
            AlertDialog dialog = builder.create();
            dialog.show();
        }

        private void markOverviewFragmentForRefresh(){
            MainActivity mainActivity = (MainActivity) this.parent.parentFragment.getActivity();
            mainActivity.markOverviewFragmentForRefresh();
        }

        private void runDeletion(PurchaseAsset purchaseToDelete){
            markOverviewFragmentForRefresh();
            binding.deletionProgressBar.setVisibility(View.VISIBLE);
            binding.deleteImageView.setEnabled(false);
            parentCompositeDisposable.add(viewModel
                    .deleteTransaction(purchaseToDelete.purchase)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(() -> {
                        parent.purchaseAssets.remove(purchaseToDelete);
                        parent.notifyDataSetChanged();
                    }));
        }

        private String buildPurchaseDetailsString(Purchase p){
            return p.date.format(dateTimeFormatter) + ", " + p.totalQuantity + " @ €" + p.assetPrice.toString() + " per unit, total €"+ p.totalQuantity.multiply(p.assetPrice).toString();
        }

        public void resetLayout(){
            binding.deletionProgressBar.setVisibility(View.INVISIBLE);
            binding.deleteImageView.setEnabled(true);
        }
    }

    private static class SaleTransactionViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener, TransactionViewHolder {

        TransactionDetailsSaleRecyclerTemplateBinding binding;
        SaleTransactionViewHolder(TransactionDetailsSaleRecyclerTemplateBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

        }

        @Override
        public void setTransactionAsset(PurchaseAsset purchaseAsset) {
            this.binding.setPurchaseAsset(purchaseAsset);
        }

        public void resetLayout(){

        }
    }
}
