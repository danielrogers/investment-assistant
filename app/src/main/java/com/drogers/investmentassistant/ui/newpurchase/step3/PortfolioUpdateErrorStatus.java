package com.drogers.investmentassistant.ui.newpurchase.step3;

public enum PortfolioUpdateErrorStatus {
    ERROR_INCOMPLETE_DATA_FETCH,
    ERROR_CUSTOM_ASSET_USED,
    NO_ERROR
}
