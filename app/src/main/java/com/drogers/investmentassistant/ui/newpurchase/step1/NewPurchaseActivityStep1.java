package com.drogers.investmentassistant.ui.newpurchase.step1;

import android.os.Bundle;
import android.util.Log;

import com.drogers.investmentassistant.IABoilerPlateActivity;
import com.drogers.investmentassistant.R;
import com.drogers.investmentassistant.ui.Utils;
import com.drogers.investmentassistant.ui.newpurchase.assetlist.AssetListFragmentLoader;


import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Step one of the new purchase wizard - this screen shows recently used assets
 */
public class NewPurchaseActivityStep1 extends IABoilerPlateActivity {

    private Step1ViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_purchase_step1);
        Utils.initialiseSearchToolbar(this, R.string.new_purchase_select_asset, true);
        //create the view model and bind it to this class
        viewModel = ViewModelProviders.of(this).get(Step1ViewModel.class);
        initialiseRecentAssetsView();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        compositeDisposable.dispose();
    }

    /**
     * Shows the correct Fragment for the recent assets view. If there are no recents,
     * a help text is shown, otherwise recents are shown
     */
    private void initialiseRecentAssetsView(){
        compositeDisposable.add(viewModel.getRecentAssets()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    assets -> {
                        if(assets.size() > 0){
                            AssetListFragmentLoader.loadAssetListFragment(this, assets, R.id.fragment_holder);
                        }else{
                            showNoRecentAssetsFragment();
                        }
                    },
                    throwable -> {
                        Timber.e(throwable.getMessage());
                        throwable.printStackTrace();
                        showNoRecentAssetsFragment();
                    })
        );
    }

    private void showNoRecentAssetsFragment(){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Timber.d("No recent assets found, showing No Results fragment");
        RecentAssetsNoResultsFragment recentAssetsNoResultsFragment = new RecentAssetsNoResultsFragment();
        transaction.replace(R.id.fragment_holder, recentAssetsNoResultsFragment);
        transaction.commitNowAllowingStateLoss();

    }
}
