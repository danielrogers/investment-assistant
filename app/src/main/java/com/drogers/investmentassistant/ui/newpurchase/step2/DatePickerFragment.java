package com.drogers.investmentassistant.ui.newpurchase.step2;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.DatePicker;

import java.util.Calendar;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    public interface DatePickerListener{
        void onDateSet(DatePicker view, int year, int month, int dayOfMonth);
    }

    DatePickerListener listener;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Context context = getContext();
        try{
            listener = (DatePickerListener) context;
        }catch(ClassCastException cce){
            throw new ClassCastException(context + " must implement DatePickListener");
        }


        // Use the current date as the default date in the picker.
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        //+1 needed here due to difference in date formats (LocalDate is 1-indexed)
        listener.onDateSet(view, year, month + 1, dayOfMonth);
    }
}
