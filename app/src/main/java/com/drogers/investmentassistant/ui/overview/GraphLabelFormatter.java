package com.drogers.investmentassistant.ui.overview;

import android.content.Context;

import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;

import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;

public class GraphLabelFormatter extends DateAsXAxisLabelFormatter {


    public GraphLabelFormatter(Context context, DateFormat dateFormat) {
        super(context, dateFormat);
    }

    @Override
    public String formatLabel(double value, boolean isValueX) {
        if (isValueX) {
            return super.formatLabel(value, isValueX);
        } else {
            if(value >= 1_000_000d){
                return "€" + (value / 1_000_000) + "M";
            }
            if(value >= 1000d){
                double dividedValue = value/1000;
                if(dividedValue % 1.0 == 0.0){
                    return "€" + (int) dividedValue + "K";
                }
                DecimalFormat df = new DecimalFormat("#.##");
                df.setRoundingMode(RoundingMode.HALF_DOWN);
                return "€" + df.format(dividedValue) + "K";
            }
            return "€" + super.formatLabel(value, isValueX);
        }
    }
}
