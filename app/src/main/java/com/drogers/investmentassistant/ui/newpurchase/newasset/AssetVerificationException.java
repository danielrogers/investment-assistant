package com.drogers.investmentassistant.ui.newpurchase.newasset;

public class AssetVerificationException extends Exception {
    public AssetVerificationException(String message){
        super(message);
    }
}
