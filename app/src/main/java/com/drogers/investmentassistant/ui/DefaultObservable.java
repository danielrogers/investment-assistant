package com.drogers.investmentassistant.ui;


import androidx.databinding.Observable;
import androidx.databinding.PropertyChangeRegistry;

/**
 * Boilerplate implementation of Observable.  Implementors of this interface need only
 * provide an implementation of getCallbacks, which exposes the PropertyChangeRegistry
 */
public interface DefaultObservable extends Observable {
    PropertyChangeRegistry getCallbacks();
    @Override
    default void addOnPropertyChangedCallback(
            Observable.OnPropertyChangedCallback callback) {
        getCallbacks().add(callback);
    }

    @Override
    default void removeOnPropertyChangedCallback(
            Observable.OnPropertyChangedCallback callback) {
        getCallbacks().remove(callback);
    }

    /**
     * Notifies observers that a specific property has changed. The getter for the
     * property that changes should be marked with the @Bindable annotation to
     * generate a field in the BR class to be used as the fieldId parameter.
     *
     * @param fieldId The generated BR id for the Bindable field.
     */
    default void notifyPropertyChanged(int fieldId) {
        getCallbacks().notifyCallbacks(this, fieldId, null);
    }
}
