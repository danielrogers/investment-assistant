package com.drogers.investmentassistant.ui.newpurchase.step3;

import android.annotation.SuppressLint;
import android.app.Application;
import android.util.Pair;

import com.drogers.investmentassistant.ThreeTuple;
import com.drogers.investmentassistant.db.AssetExchange;
import com.drogers.investmentassistant.db.RoomDb;
import com.drogers.investmentassistant.db.entity.Asset;
import com.drogers.investmentassistant.db.entity.AssetOwnershipRecord;
import com.drogers.investmentassistant.db.entity.AssetOwnershipRecordRepository;
import com.drogers.investmentassistant.db.entity.AssetRepository;
import com.drogers.investmentassistant.db.entity.IADisposable;
import com.drogers.investmentassistant.db.entity.transaction.Purchase;
import com.drogers.investmentassistant.db.entity.transaction.PurchaseRepository;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;
import com.drogers.investmentassistant.network.info.AssetInformationServiceRequestAdapter;
import com.drogers.investmentassistant.network.info.RequestHandlerProvider;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.UnicastSubject;
import timber.log.Timber;

public class Step3ViewModel extends AndroidViewModel implements IADisposable {

    private Asset assetBeingPurchased;
    private Purchase purchaseRecord;
    private AssetRepository assetRepository;
    private AssetOwnershipRecordRepository assetOwnershipRecordRepository;
    private PurchaseRepository purchaseRepository;
    UnicastSubject<Integer> progressSubject;
    private RoomDb database;
    private ThreeTuple<List<AssetOwnershipRecord>, Purchase, Asset> dataToCommit;
    private PortfolioUpdateErrorStatus updateErrorStatus;
    //this represents the last posted update progress
    private int previousProgress;
    private AssetInformationServiceRequestAdapter assetInfoAdapter;
    private BigDecimal previouslyFetchedPrice;
    private CompositeDisposable compositeDisposable;
    private Exchange exchangeBeingUsed;

    public Step3ViewModel(@NonNull Application application) {
        super(application);
        database = RoomDb.getDatabase(application);
        assetRepository = new AssetRepository(application, database.assetDAO());
        assetOwnershipRecordRepository = new AssetOwnershipRecordRepository(database.assetOwnershipRecordDAO());
        purchaseRepository = new PurchaseRepository(database.purchaseDAO());
        updateErrorStatus = PortfolioUpdateErrorStatus.NO_ERROR;
        assetInfoAdapter = RequestHandlerProvider.getAssetInfoRequestAdapterImplementation(application);
        compositeDisposable = new CompositeDisposable();
    }

    PortfolioUpdateErrorStatus getUpdateErrorStatus(){
        return updateErrorStatus;
    }

    /**
     * Checks if other AORecords should be present on the given date. Given:
     * <ul>
     *     <li>Asset X AORecord already exists on Monday</li>
     *     <li>New purchaseRecord is recorded for Asset Y, on Tuesday</li>
     * </ul>
     * Tuesday should have AORecords present representing Asset X
     * @param date
     * @return Containing Asset and amount of asset
     */
    private List<AssetOwnershipRecord> getAORecordsNeededForDate(LocalDate date){
        List<AssetOwnershipRecord> records = assetOwnershipRecordRepository.getAORecordsWhichShouldExistOnDate(date);
        if(records != null && !records.isEmpty()){
            return generateAORecordsFromMappings(getAssetDetailMappings(records), date);
        }
        return null;
    }

    /**
     * Sums up the given list of AORecords
     * @return Key will be the assetUID, Value will be a Pair containing the total amount of this asset owned and it's
     * price.  There is no guarantee that the price will be the closing price fot the given date; it's simply provided in
     * case an estimated price is needed
     */
    HashMap<Long, Pair<BigDecimal, BigDecimal>> getAssetDetailMappings(List<AssetOwnershipRecord> assetOwnershipRecords){
        @SuppressLint("UseSparseArrays") HashMap<Long, Pair<BigDecimal, BigDecimal>> summation = new HashMap<>();
        assetOwnershipRecords.forEach(record -> {
            BigDecimal amountForThisAsset = record.amountOwned;
            Pair<BigDecimal, BigDecimal> previousDataForThisAsset = summation.get(record.assetUid);
            if(previousDataForThisAsset == null){
                summation.put(record.assetUid, new Pair<>(amountForThisAsset, record.price));
            }else{
                BigDecimal previousAmountForThisAsset = previousDataForThisAsset.first;
                summation.put(record.assetUid, new Pair<>(amountForThisAsset.add(previousAmountForThisAsset), record.price));
            }
        });
        return summation;
    }

    List<AssetOwnershipRecord> generateAORecordsFromMappings(HashMap<Long, Pair<BigDecimal, BigDecimal>> mappings, LocalDate date){
        List<AssetOwnershipRecord> aoRecords = new ArrayList<>();
        mappings.forEach((assetUid, assetInfoPair) ->{
            BigDecimal amountOwned = assetInfoPair.first;
            BigDecimal oldPrice = assetInfoPair.second;

            AssetOwnershipRecord newRecord = new AssetOwnershipRecord(assetUid,
                    null,
                    amountOwned,
                    date, //todo what if this originated from a backdated transaction, and :date = today? Looks like request handler will get a blank request and track back to yesterday, verify on a weekday
                    true);
            AssetExchange assetExchange = assetRepository.getAssetExchange(assetUid);
            BigDecimal closingPriceOnDate = assetInfoAdapter.getStockClosingPriceAsOfDate(assetExchange.asset.symbol, assetExchange.getExchange(), date);
            if(closingPriceOnDate != null){
                newRecord.price = closingPriceOnDate;
            }else{
                updateErrorStatus = PortfolioUpdateErrorStatus.ERROR_INCOMPLETE_DATA_FETCH;
                newRecord.closingPriceEstimated = true;
                newRecord.price = oldPrice;
            }
            aoRecords.add(newRecord);
        });
        return aoRecords;
    }


    /**
     * Generates the first AORecord for this purchaseRecord.  The record will not be considered as a closing price record
     * The price is not queried for this AORecord, it is taken from the associated Transaction Record
     * @param dateOfPurchase Date purchaseRecord occurred
     * @return AORecord built as above
     */
    private AssetOwnershipRecord generateFirstAORecordForPurchase(LocalDate dateOfPurchase){
        return new AssetOwnershipRecord(assetBeingPurchased.uid,
                purchaseRecord.assetPrice,
                purchaseRecord.remainingActiveUnits,
                dateOfPurchase,
                false);
    }

    /**
     * Adds any additional AORecords on this date that may be needed. Additional records may be needed
     * if recording a purchaseRecord on a date which has no other AORecords present; in this case any previous active
     * AORecords will need to be represented on this date.
     * @param records Collection to add any generated AORecords to
     * @param date
     */
    private void addAdditionalRecordsOnDateIfNeeded(List<AssetOwnershipRecord> records, LocalDate date){
        List<AssetOwnershipRecord> additionalRecordsNeededForThisDate = getAORecordsNeededForDate(date);
        if(additionalRecordsNeededForThisDate != null && !additionalRecordsNeededForThisDate.isEmpty()) {
            records.addAll(additionalRecordsNeededForThisDate);
        }
    }

    /**
     * Gets all AORecords which need to be either created or updated in the DB for the given list of dates.
     * @param dates Dates on which records need to be created/updated
     * @return Record to be upserted
     */
    List<AssetOwnershipRecord> getAllRecordsToUpsertForDates(List<LocalDate> dates){
        ArrayList<AssetOwnershipRecord> records = new ArrayList<>();
        Timber.d("Generating initial AORecord for first date");
        records.add(generateFirstAORecordForPurchase(dates.get(0)));
        addAdditionalRecordsOnDateIfNeeded(records, dates.get(0));
        //give a rough estimate of progress
        progressSubject.onNext(Math.max(Math.min(1 / dates.size(), 95), 1));
        Timber.d("Generating AORecords for remaining dates");
        records.addAll(getSubsequentRecordsToUpsertForPurchase(dates));
        return records;
    }

    /**
     * Returns the first AORecord encountered in the given collection which represents the asset closing price.
     * This should only be used with a series of AORecords on the same date
     * @param records Records to choose from
     * @return First encountered end of day record (based on AssetOwnershipRecord::priceRepresentsClosingValue)
     */
    private AssetOwnershipRecord findEndOfDayRecordFromCollection(List<AssetOwnershipRecord> records){
        if(records.size() > 0) {
            LocalDate dateToUse = records.get(0).asOfDate;
            for (int i = 0; i < records.size(); i++) {
                AssetOwnershipRecord recordOnThisDate = records.get(i);
                if (!recordOnThisDate.asOfDate.isEqual(dateToUse)) {
                    throw new UnsupportedOperationException("findEndOfDayRecordFromCollection should only be called with a series of AORecords on the same date");
                }
                if (recordOnThisDate.priceRepresentsClosingValue) {
                    return recordOnThisDate;
                }
            }
        }
        return null;
    }

    /**
     * Fetches the closing price as of the specified date and sets it into the AORecord::price field.<br>
     * If the price cannot be retrieved, the following happens:
     * <ul>
     *  <li>The AORecord::closingPriceEstimated is set to true
     *  <li>If the previously fetched price is not null, it is used. These are processed sequentially so this will give it an accurate-ish representation
     *  <li>If the previously fetched prices is null, the purchaseRecord price is used
     *  </ul>
     * @param date Date on which to get the closing price
     * @param assetOwnershipRecord AORecord to update
     */
    private void fetchClosingPriceOnDateAndSet(LocalDate date, AssetOwnershipRecord assetOwnershipRecord){
        BigDecimal closingPriceOnDate = assetInfoAdapter.getStockClosingPriceAsOfDate(assetBeingPurchased.symbol, exchangeBeingUsed, date);
        if(closingPriceOnDate != null){
            previouslyFetchedPrice = closingPriceOnDate;
        }else{
            updateErrorStatus = PortfolioUpdateErrorStatus.ERROR_INCOMPLETE_DATA_FETCH;
            assetOwnershipRecord.closingPriceEstimated = true;
            if(previouslyFetchedPrice != null){
                closingPriceOnDate = previouslyFetchedPrice;
            }else{
                closingPriceOnDate = purchaseRecord.assetPrice;
            }
        }
        assetOwnershipRecord.price = closingPriceOnDate;
    }

    /**
     * Builds an end day AORecord for the given date
     * @param date Date on which to build for
     * @return Generated AORecord
     */
    private AssetOwnershipRecord buildAORecordForDate(LocalDate date){
        AssetOwnershipRecord assetOwnershipRecord = new AssetOwnershipRecord(assetBeingPurchased.uid,
                null,
                purchaseRecord.remainingActiveUnits,
                date, //todo what if this originated from a backdated transaction, and :date = today? Looks like request handler will get a blank request and track back to yesterday, verify on a weekday
                true);
        //if it's a user-defined asset, there's no need to waste an api call
        if(assetBeingPurchased.isUserDefinedAsset){
            assetOwnershipRecord.price = purchaseRecord.assetPrice;
        }else{
            fetchClosingPriceOnDateAndSet(date, assetOwnershipRecord);
        }
        return assetOwnershipRecord;
    }

    /**
     * Public progress results to the progressSubject
     * @param progressCount Current progress.  Results are only published to the
     *                      subject if this has changed since the previous method call
     */
    private void updateProgress(int progressCount){
        //don't spam the UI thread with too many updates
        if(progressCount != previousProgress) {
            if(progressCount % 10 == 0)
                Timber.d("Progress is: " + progressCount);
            progressSubject.onNext(progressCount);
            previousProgress = progressCount;
        }
    }

    /**
     * Generates a list of new or updated AORecords to reflect this purchaseRecord for the given dates. This
     * does not generate a record from the first element (hence subsequent)
     * @param dates For each Date an AORecord will be generated if there is none present on this date, or updated if
     *              present.
     * @return Records to upsert
     */
    private List<AssetOwnershipRecord> getSubsequentRecordsToUpsertForPurchase(List<LocalDate> dates){
        List<AssetOwnershipRecord> aORecordsToUpsert = new ArrayList<>();
        for(int i = 1; i < dates.size(); i++){
            LocalDate date = dates.get(i);
            List<AssetOwnershipRecord> recordsOnThisDate = assetOwnershipRecordRepository.getAORecordsOnDate_synchronous(date, assetBeingPurchased);
            AssetOwnershipRecord recordToUse = findEndOfDayRecordFromCollection(recordsOnThisDate);
            if(recordToUse == null){
                recordToUse = buildAORecordForDate(date);
            }else{
                recordToUse.amountOwned = recordToUse.amountOwned.add(purchaseRecord.remainingActiveUnits);
            }
            aORecordsToUpsert.add(recordToUse);
            updateProgress(getBoundedProgressValue(i, dates.size()));
        }
        return aORecordsToUpsert;
    }

    //results bounded between 1 and 95
    private int getBoundedProgressValue(int current, int target){
        float progress = (float) current / (float) target;
        progress = progress * 100;
        return (int) Math.max(1f, Math.min(progress, 95f));
    }

    /**
     * Initialises fields relating to the purchaseRecord
     */
    void initPurchase(Purchase purchaseRecord, Asset assetBeingPurchased){
        dataToCommit = new ThreeTuple<>();
        previousProgress = -1;
        this.purchaseRecord = purchaseRecord;
        this.assetBeingPurchased = assetBeingPurchased;
    }

    private void updateAssetBeingPurchased(Purchase purchaseRecord){
        assetBeingPurchased.lastPurchaseDate = LocalDate.now();
        assetBeingPurchased.liveQuantity = assetBeingPurchased.liveQuantity.add(purchaseRecord.remainingActiveUnits);
    }

    /**
     * Generates the data to be committed for the purchaseRecord, which will be available in the dataToCommit field
     * after this has completed. Purchase data is generated across multiple tables as follows:
     *  <ul>
     *  <li>Asset - the asset being purchased has it's lastPurchaseDate & liveQuantity field updated</li>
     *  <li>AssetOwnershipRecord - for every date in the AORecord table on or after this purchaseRecord
     *  date, an AORecord is either created, or updated if it already exists.  This allows accurate
     *  tracking of portfolio value across time, especially in the case of backdated purchases
     *  <li>Transaction - a Transaction record is created on this date</li>
     *  </ul>
     * @return Subject which tracks the progress of data generation
     */
    public UnicastSubject<Integer> generatePurchaseData(Purchase purchaseRecord, Asset assetBeingPurchased, Exchange exchangeBeingUsed, boolean isRetry){
        Timber.d("Recording purchaseRecord for " + assetBeingPurchased);
        initPurchase(purchaseRecord, assetBeingPurchased);
        if(!isRetry) {
            updateAssetBeingPurchased(purchaseRecord);
        }else{
            updateErrorStatus = PortfolioUpdateErrorStatus.NO_ERROR;
        }
        if(assetBeingPurchased.isUserDefinedAsset){
            updateErrorStatus = PortfolioUpdateErrorStatus.ERROR_CUSTOM_ASSET_USED;
        }
        dataToCommit = new ThreeTuple<>();
        progressSubject = UnicastSubject.create();
        this.exchangeBeingUsed = exchangeBeingUsed;
        compositeDisposable.add(assetOwnershipRecordRepository
                .getAllEntryDatesFrom(purchaseRecord.date)
                .subscribeOn(Schedulers.io())
                .subscribe(this::onEntryDatesReceived,
                        throwable -> {
                            Timber.e("Error generating AORecords: " + throwable.getMessage());
                            throwable.printStackTrace();
                            progressSubject.onError(throwable);
                        }));
        return progressSubject;
    }

    private void onEntryDatesReceived(List<LocalDate> dates){
        try{
            Timber.d("Updating AORecords for " + dates.size() + " dates");
            List<AssetOwnershipRecord> aoRecordsToUpsert = getAllRecordsToUpsertForDates(dates);
            Timber.d("All AORecords generated!");
            dataToCommit.putValues(aoRecordsToUpsert, purchaseRecord, assetBeingPurchased);
            progressSubject.onComplete();
        }catch (Throwable t){
            progressSubject.onError(t);
        }
    }

    /**
     * Commits any entity data which has been generated
     * @return UnicastSubject, commit success or error will be posted here.  Nothing will ver be pushed to OnNext
     */
    public UnicastSubject<Void> commitData(){
        UnicastSubject<Void> subject = UnicastSubject.create();
        compositeDisposable.add(Single.fromCallable(() -> {
            return 1;
        }).subscribeOn(Schedulers.io())
                .subscribe(integer -> {
                    try{
                        database.runInTransaction(() -> {
                            Timber.d("DB update in progress");
                            assetOwnershipRecordRepository.upsert(dataToCommit.value1);
                            assetRepository.update_synchronous(dataToCommit.value3);
                            purchaseRepository.insert_synchronous(dataToCommit.value2);
                            Timber.d("DB update complete");
                            subject.onComplete();
                        });
                    }catch (Exception e){
                        subject.onError(e);
                    }
                }));
        return subject;
    }

    @Override
    public CompositeDisposable getDisposable() {
        return compositeDisposable;
    }
}
