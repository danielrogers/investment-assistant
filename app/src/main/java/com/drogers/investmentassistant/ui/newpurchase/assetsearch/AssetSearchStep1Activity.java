package com.drogers.investmentassistant.ui.newpurchase.assetsearch;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.drogers.investmentassistant.MainActivity;
import com.drogers.investmentassistant.R;
import com.drogers.investmentassistant.RemoteAssetSearchContainerFragment;
import com.drogers.investmentassistant.databinding.ActivityAssetSearchStep1Binding;
import com.drogers.investmentassistant.db.AssetExchange;
import com.drogers.investmentassistant.network.info.AssetInformationServiceRequestAdapter;
import com.drogers.investmentassistant.network.info.RequestHandlerProvider;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;
import com.drogers.investmentassistant.ui.Utils;
import com.drogers.investmentassistant.ui.newpurchase.assetlist.AssetListFragmentLoader;
import com.drogers.investmentassistant.ui.newpurchase.assetsearch.remote.AssetSearchExchangeListFragment;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import androidx.test.espresso.idling.CountingIdlingResource;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.UnicastSubject;
import timber.log.Timber;

/**
 * Initial (and possibly final) step of asset searching.  Shows any locally cached assets matching search criteria,
 * or, failing that, a list of remote exchanges to search on
 */
public class AssetSearchStep1Activity extends AssetSearchWizardActivity {

    private final CountingIdlingResource idlingResource = new CountingIdlingResource("AssetSearchStep1Activity");
    private String queryString;
    private ActivityAssetSearchStep1Binding binding;

    public CountingIdlingResource getIdlingResource() {
        return idlingResource;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_asset_search_step1);
        viewModel = ViewModelProviders.of(this).get(SearchResultsViewModel.class);
        handleIntent(getIntent());
        binding.searchDiffExchangeButton.setVisibility(View.GONE);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent){
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            queryString = intent.getStringExtra(SearchManager.QUERY);
            Utils.initialiseToolbar(this, getString(R.string.asset_search_step_1_title, queryString));
            loadSpinnerFragment(R.id.fragment_holder_layout);
            doLocalSearch();
        }
    }

    private void cacheSearchSuggestion(){
        SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this,
                SearchSuggestionsProvider.AUTHORITY, SearchSuggestionsProvider.MODE);
        suggestions.saveRecentQuery(queryString, null);
    }

    /**
     * Save the search text to the recent search suggestions provider, then run the search
     */
    private void doLocalSearch() {
        idlingResource.increment();
        cacheSearchSuggestion();
        UnicastSubject<List<AssetExchange>> searchResults = viewModel.getAllAssetsWithSymbolLocally(queryString);
        compositeDisposable.add(searchResults
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        assets -> {
                            if(assets.isEmpty()){
                                Timber.d("No search results");
                                showSearchResultsFragment(null);
                            }else{
                                showSearchResultsFragment(assets);
                            }
                        },
                        throwable -> {
                            showSearchResultsFragment(null);
                            Timber.d("No search results: " + throwable.getMessage());
                            showErrorInfoIfNot404(throwable);
                        }));
    }

    /**
     * Shows the search results screen - this will either be a fragment containing search results
     * or a list of exchanges to search on
     * @param searchResults The Asset returned by the search. null for "search remote" screen
     */
    private void showSearchResultsFragment(List<AssetExchange> searchResults){
        if(searchResults != null){
            AssetListFragmentLoader.loadAssetListFragment(this, searchResults, R.id.fragment_holder_layout);
            //also show the button here
            binding.searchDiffExchangeButton.setVisibility(View.VISIBLE);
            idlingResource.decrement();
        }else{
            showRemoteExchangesFragment();
        }
    }

    /**
     * Displays a toast showing some error info if we get any sort of error
     * other than a 404 response from the server. Note that this error could originate from local
     * code (e.g. if search query parsing fails).
     */
    private void showErrorInfoIfNot404(Throwable t){
        if(t instanceof VolleyError){
            //no point showing an error toast for a 404
            NetworkResponse response = ((VolleyError) t).networkResponse;
            if(response != null && response.statusCode == 404){
                return;
            }
        }
        Toast.makeText(this, t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
    }

    private void showErrorAndReturnToOverviewScreen(Throwable t){
        if(t instanceof NetworkError || t instanceof TimeoutError){
            Toast.makeText(this, "Could not search - network unavailable", Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(this, "Could not search: " + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
        }
        startActivity(new Intent(getApplicationContext(), MainActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
    }

    /**
     * First shows the loading spinner, then fetches the exchanges &
     * once fetched replaces the spinner with the list of exchanges
     */
    private void showRemoteExchangesFragment(){
        RemoteAssetSearchContainerFragment containerFragment = loadContainerFragment();
        //set container header to title
        containerFragment.binding.headerTextView.setText(R.string.asset_search_remote_select_exchange);
        //set container body to spinner until results are fetched
        loadSpinnerFragment(containerFragment.binding.contentFragmentHolder.getId());
        //when results are fetched, set body to result list
        fetchExchangesAsync(containerFragment.binding.contentFragmentHolder.getId());
    }


    /**
     * Fetches a list of supported exchanges from the remote service.
     * If the service cannot be queried, an error toast is shown and the user is returned to the overview screen
     * @param containerId ID of the container view which results will be loaded into
     */
    private void fetchExchangesAsync(int containerId){
        AssetInformationServiceRequestAdapter requestHandler = RequestHandlerProvider.getAssetInfoRequestAdapterImplementation(this);
        compositeDisposable.add(requestHandler
                .getSupportedExchanges()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(exchanges -> {
                            loadExchangeListFragment(exchanges, containerId);
                            idlingResource.decrement();
                        },
                        throwable -> {
                            Timber.e(throwable.toString() + throwable.getMessage());
                            throwable.printStackTrace();
                            idlingResource.decrement();
                            showErrorAndReturnToOverviewScreen(throwable);
                        }));
    }

    private void loadExchangeListFragment(List<Exchange> exchanges, int containerId){
        AssetSearchExchangeListFragment exchangeListFragment = AssetSearchExchangeListFragment.newInstance(queryString);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(containerId, exchangeListFragment);
        fragmentTransaction.commitNowAllowingStateLoss();
        exchangeListFragment.setExchanges(exchanges);
    }

    public void handleDiffExchangeButtonClicked(View v){
        idlingResource.increment();
        showRemoteExchangesFragment();
        binding.searchDiffExchangeButton.setVisibility(View.GONE);
    }

}
