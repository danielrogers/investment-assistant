package com.drogers.investmentassistant.ui.overview;


import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.drogers.investmentassistant.IABoilerPlateFragment;
import com.drogers.investmentassistant.NewSaleActivity;
import com.drogers.investmentassistant.R;
import com.drogers.investmentassistant.RefreshableFragment;
import com.drogers.investmentassistant.databinding.FragmentOverviewBinding;
import com.drogers.investmentassistant.db.RoomDb;
import com.drogers.investmentassistant.db.entity.AssetOwnershipRecord;
import com.drogers.investmentassistant.ui.Utils;
import com.drogers.investmentassistant.ui.newpurchase.step1.NewPurchaseActivityStep1;
import com.github.clans.fab.FloatingActionButton;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;


/**
 * A simple {@link Fragment} subclass.
 */
public class OverviewFragment extends RefreshableFragment implements View.OnClickListener {

    private OverviewViewModel viewModel;
    private FragmentOverviewBinding dataBinding;
    public OverviewFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        // Inflate the layout for this fragment
        initBinding(inflater, container);
        registerListeners();
        initialiseGraph();
        dataBinding.portfolioValLabel
                .setText(getString(
                        R.string.overview_frag_value, "0.0"));
        return dataBinding.getRoot();
    }

    private void initBinding(LayoutInflater inflater, ViewGroup container){
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_overview, container, false);
        viewModel = ViewModelProviders.of(this).get(OverviewViewModel.class);
        dataBinding.setViewmodel(viewModel);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.closeFabMenu(dataBinding.getRoot());
    }

    private void registerListeners() {
        FloatingActionButton newPurchaseButton = dataBinding.newPurchaseFab;
        newPurchaseButton.setOnClickListener(this);
        FloatingActionButton newSaleButton = dataBinding.newSaleFab;
        newSaleButton.setOnClickListener(this);
        dataBinding.buyButton.setOnClickListener(this);
        dataBinding.sellButton.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buy_button:
                onNewPurchaseClicked();
                break;
            case R.id.sell_button:
                onNewSaleClicked(v);
                break;
            default:
                Timber.d("OnClick for v with id " + v.getId() + " is not handled");
        }
    }

    private void onNewPurchaseClicked() {
        Intent intent = new Intent(getActivity(),
                NewPurchaseActivityStep1.class);
        startActivity(intent);
    }

    private void onNewSaleClicked(View v) {
        Intent intent = new Intent(getActivity(),
                NewSaleActivity.class);
        View toolbar = getActivity().findViewById(R.id.toolbar);
        startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this.getActivity(), toolbar, "toolbar").toBundle());
    }

    private void onSampleDataClicked() {
        //RoomDb.destructivelyLoadSampleData(getActivity().getApplication(), RoomDb.getDatabase(this.getContext()));
        //RoomDb.testMultiLoad(RoomDb.getDatabase(this.getContext()));
        /*Intent intent = new Intent(this.getContext(), NewPurchaseActivityStep3.class);
        intent.putExtra(NewPurchaseKeys.STEP_3_PURCHASE_EXTRA_KEY, new Transaction());
        intent.putParcelableArrayListExtra("f", new ArrayList< >());
        startActivity(intent);*/
    }

    private void onClearDataClicked() {
        RoomDb.clearDB(RoomDb.getDatabase(this.getContext()));
    }

    private void initialiseGraph() {
        compositeDisposable.add(viewModel
                .getAORecordsFromLastYearPlus1()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSummariesRetrieved,
                        throwable -> {
                            Timber.e("ERROR: " + throwable.getMessage());
                            throwable.printStackTrace();
                        }));
        //add to graph
        //if there's no data??
    }

    private void onSummariesRetrieved(List<AssetOwnershipRecord> mostRecentAORecords){
        if(!mostRecentAORecords.isEmpty()) {
            List<ArrayList<AssetOwnershipRecord>> mappedAORecords = OverviewUtils.mapAORecords(mostRecentAORecords);
            setPortfolioValueText(mappedAORecords.get(mappedAORecords.size() - 1));
            GraphHelper gh = new GraphHelper(mappedAORecords, dataBinding.overviewGraph);
            gh.draw();
        }
    }

    private void setPortfolioValueText(ArrayList<AssetOwnershipRecord> mostRecentAORecords){
        //todo fix this and write a test for it
        BigDecimal sum = new BigDecimal(0);
        for(AssetOwnershipRecord record : mostRecentAORecords){
            sum = sum.add(record.price.multiply(record.amountOwned));
        }
        dataBinding.portfolioValLabel
                .setText(getString(
                        R.string.overview_frag_value,
                        String.valueOf(sum.setScale(2, RoundingMode.HALF_EVEN)))
                );
    }

    @Override
    protected void handleRefresh() {
        initialiseGraph();
    }

}
