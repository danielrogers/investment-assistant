package com.drogers.investmentassistant.ui.newpurchase.assetlist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.drogers.investmentassistant.databinding.AssetDetailsRecyclerTemplateBinding;
import com.drogers.investmentassistant.db.AssetExchange;
import com.drogers.investmentassistant.db.entity.Asset;
import com.drogers.investmentassistant.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Adapter for the recent asset data set, to be displayed when searching for an asset during purchase
 */
public class AssetListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<AssetExchange> assetExchangesToDisplay;
    private LayoutInflater inflater;
    private AssetListItemSelectedHandler assetListItemSelectedHandler;

    public AssetListAdapter(Context context, AssetListItemSelectedHandler assetListItemSelectedHandler) {
        inflater = LayoutInflater.from(context);
        assetExchangesToDisplay = new ArrayList<>();
        this.assetListItemSelectedHandler = assetListItemSelectedHandler;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        AssetDetailsRecyclerTemplateBinding binding = DataBindingUtil.inflate(inflater, R.layout.asset_details_recycler_template, viewGroup, false);
        return new AssetViewHolder(binding);
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        AssetExchange current = assetExchangesToDisplay.get(i);
        // Add the data to the view
        AssetViewHolder assetViewHolder = (AssetViewHolder) viewHolder;
        assetViewHolder.binding.setAsset(current.asset);
        assetViewHolder.binding.setExchange(current.getExchange());
    }

    @Override
    public int getItemCount() {
        return assetExchangesToDisplay.size();
    }

    public void setAssetExchangesToDisplay(List<AssetExchange> newAssetExchanges){
        assetExchangesToDisplay = newAssetExchanges;
        notifyDataSetChanged();
    }

    public class AssetViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        AssetDetailsRecyclerTemplateBinding binding;

        AssetViewHolder(AssetDetailsRecyclerTemplateBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            assetListItemSelectedHandler.onItemSelected(binding.getAsset(), binding.getExchange());
        }
    }
}
