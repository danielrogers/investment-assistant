package com.drogers.investmentassistant.ui.newpurchase.assetsearch;

import android.content.SearchRecentSuggestionsProvider;

public class SearchSuggestionsProvider extends SearchRecentSuggestionsProvider {
    public final static String AUTHORITY = "com.drogers.investmentassistant.ui.newpurchase.assetsearch.SearchSuggestionsProvider";
    public final static int MODE = DATABASE_MODE_QUERIES;

    public SearchSuggestionsProvider(){
        setupSuggestions(AUTHORITY, MODE);
    }
}
