package com.drogers.investmentassistant.ui.indepth;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.drogers.investmentassistant.IABoilerPlateFragment;
import com.drogers.investmentassistant.R;
import com.drogers.investmentassistant.RefreshableFragment;
import com.drogers.investmentassistant.databinding.FragmentInDepthBinding;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;


/**
 * A simple {@link Fragment} subclass.
 */
public class InDepthFragment extends RefreshableFragment {


    private FragmentInDepthBinding binding;
    private TransactionAdapter adapter;
    private InDepthViewModel viewModel;

    public InDepthFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_in_depth, container, false);
        binding.transactionsHeader.setText("Transactions");
        viewModel = ViewModelProviders.of(this).get(InDepthViewModel.class);
        initialiseRecyclerView();
        loadTransactions();
        return binding.getRoot();
    }

    private void initialiseRecyclerView(){
        RecyclerView recyclerView = binding.transactionsRecyclerView;
        adapter = new TransactionAdapter(this, viewModel);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
    }

    private void loadTransactions(){
        compositeDisposable.add(viewModel
                .getAllPurchasesDesc()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(purchases -> {
                            adapter.setPurchaseAssets(purchases);
                        }, throwable -> {
                            Timber.e("Error retrieving transactions from db: " + throwable.getMessage());
                            throwable.printStackTrace();
                        }
                ));
    }

    @Override
    protected void handleRefresh() {

    }
}
