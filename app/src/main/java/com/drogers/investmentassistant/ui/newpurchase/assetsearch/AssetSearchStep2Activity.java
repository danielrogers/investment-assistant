package com.drogers.investmentassistant.ui.newpurchase.assetsearch;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import androidx.test.espresso.idling.CountingIdlingResource;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.drogers.investmentassistant.R;
import com.drogers.investmentassistant.databinding.ActivityAssetSearchStep2Binding;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;
import com.drogers.investmentassistant.ui.Utils;
import com.drogers.investmentassistant.ui.newpurchase.NewPurchaseKeys;
import com.drogers.investmentassistant.ui.newpurchase.assetlist.AssetListFragmentLoader;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Activity which searches on a specified exchange.  The search is performed locally, which may
 * initially seem redundant, however it is needed to support some use cases (see doSearch for more)
 */
public class AssetSearchStep2Activity extends AssetSearchWizardActivity {

    ActivityAssetSearchStep2Binding binding;
    String searchTerm;
    Exchange exchange;
    private final CountingIdlingResource idlingResource = new CountingIdlingResource("AssetSearchStep2Activity");

    public CountingIdlingResource getIdlingResource() {
        return idlingResource;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_asset_search_step2);
        viewModel = ViewModelProviders.of(this).get(SearchResultsViewModel.class);
        loadIntent();
        Utils.initialiseToolbar(this, getString(R.string.asset_search_step_2_title, binding.getExchange().name));
        loadContainerFragment();
        loadSpinnerFragment(binding.fragmentHolderLayout.getId());
        //search remote and load results
        doSearch();
    }

    private void loadIntent(){
        Intent i = getIntent();
        exchange = i.getParcelableExtra(NewPurchaseKeys.REMOTE_ASSET_SEARCH_EXCHANGE_KEY);
        searchTerm = i.getStringExtra(NewPurchaseKeys.REMOTE_ASSET_SEARCH_TERM_KEY);
        binding.setExchange(exchange);
        binding.setSearchTerm(searchTerm);
    }

    /**
     * Search for the asset on local or remote.  Local is first searched to support use case where user searches
     * on one exchange, then goes back to the exchange list, then searches another exchange for which we
     * already have this asset locally.  In that case the local asset should be used
     */
    private void doSearch(){
        idlingResource.increment();
        compositeDisposable.add(viewModel
                .getAssetFromLocalOrRemote(searchTerm, exchange)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(asset -> {
                            AssetListFragmentLoader.loadAssetListFragment(this, asset, R.id.fragment_holder_layout);
                            idlingResource.decrement();
                        },
                        throwable -> {
                            Timber.d("No results found at local or remote, showing No Results fragment");
                            AssetSearchNoResultsFragment fragment = new AssetSearchNoResultsFragment();
                            fragment.setSearchParams(searchTerm, exchange);
                            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                            transaction.replace(binding.fragmentHolderLayout.getId(), fragment);
                            transaction.commitNowAllowingStateLoss();
                            idlingResource.decrement();
                        })
        );
    }
}
