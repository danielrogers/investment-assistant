package com.drogers.investmentassistant.ui.newpurchase.assetlist;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.drogers.investmentassistant.db.AssetExchange;
import com.drogers.investmentassistant.db.entity.Asset;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;
import com.drogers.investmentassistant.ui.newpurchase.NewPurchaseKeys;
import com.drogers.investmentassistant.ui.newpurchase.step2.NewPurchaseActivityStep2;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public class AssetListFragmentLoader {

    private static void moveToStep2WithSelectedAsset(Context context, Asset a, Exchange exchange){
        Intent intent = new Intent(context, NewPurchaseActivityStep2.class);
        intent.putExtra(NewPurchaseKeys.ASSET_EXTRA_KEY, a);
        intent.putExtra(NewPurchaseKeys.EXCHANGE_EXTRA_KEY, exchange);
        context.startActivity(intent);
    }

    public static void loadAssetListFragment(FragmentActivity context, List<AssetExchange> assetExchanges, int fragmentContainer){
        Timber.d("Loading asset list fragment");
        FragmentTransaction transaction = context.getSupportFragmentManager().beginTransaction();
        //setting the on click handler
        AssetsDetailsListFragment assetsDetailsListFragment = AssetsDetailsListFragment.newInstance((asset,exchange) -> {
            moveToStep2WithSelectedAsset(context, asset, exchange);
        });
        transaction.replace(fragmentContainer, assetsDetailsListFragment);
        transaction.commitNowAllowingStateLoss();
        assetsDetailsListFragment.setAssetExchangesToDisplay(assetExchanges);
    }

    public static void loadAssetListFragment(FragmentActivity context, AssetExchange searchResult, int fragmentContainer){
        ArrayList<AssetExchange> assetWrapper = new ArrayList<>();
        assetWrapper.add(searchResult);
        loadAssetListFragment(context, assetWrapper, fragmentContainer);
    }
}
