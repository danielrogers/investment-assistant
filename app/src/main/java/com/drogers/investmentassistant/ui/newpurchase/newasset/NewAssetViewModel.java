package com.drogers.investmentassistant.ui.newpurchase.newasset;

import android.app.Application;
import android.content.Context;

import com.drogers.investmentassistant.R;
import com.drogers.investmentassistant.db.RoomDb;
import com.drogers.investmentassistant.db.entity.Asset;
import com.drogers.investmentassistant.db.entity.AssetRepository;
import com.drogers.investmentassistant.db.entity.AssetType;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;
import com.drogers.investmentassistant.ui.DefaultObservable;

import java.time.LocalDate;
import java.util.Calendar;
import androidx.annotation.NonNull;
import androidx.databinding.Bindable;
import androidx.databinding.PropertyChangeRegistry;
import androidx.lifecycle.AndroidViewModel;
import androidx.room.Room;
import io.reactivex.schedulers.Schedulers;

public class NewAssetViewModel extends AndroidViewModel implements DefaultObservable {

    private PropertyChangeRegistry callbacks = new PropertyChangeRegistry();
    private Asset newAsset;
    private AssetRepository assetRepository;
    private Exchange exchange;

    public NewAssetViewModel(@NonNull Application application) {
        super(application);
        newAsset = new Asset();
        newAsset.assetType = AssetType.STOCK;
        //this might not work in the future, depending on how this field is used.
        //setting it here so it shows up in the recently used list
        newAsset.lastPurchaseDate = LocalDate.now();
        newAsset.isUserDefinedAsset = true;
        assetRepository = new AssetRepository(application, RoomDb.getDatabase(application).assetDAO());
    }

    private void verifyAsset(Context context) throws AssetVerificationException {
        if (newAsset.name == null || newAsset.name.isEmpty()) {
            throw new AssetVerificationException(context.getString(R.string.create_new_asset_blank_name));
        }
        if (newAsset.symbol == null || newAsset.symbol.isEmpty()) {
            throw new AssetVerificationException(context.getString(R.string.create_new_asset_blank_symbol));
        }
        if (newAsset.description == null || newAsset.description.isEmpty()) {
            throw new AssetVerificationException(context.getString(R.string.create_new_asset_blank_description));
        }
    }

    void saveAsset(Context context) throws AssetVerificationException {
        verifyAsset(context);
        assetRepository
                .insert(newAsset)
                .subscribeOn(Schedulers.computation())
                .subscribe();
    }

    public void setAssetName(String s) {
        newAsset.name = s.trim();
    }

    @Bindable
    public String getAssetName() {
        return newAsset.name;
    }

    public void setAssetDescription(String s) {
        newAsset.description = s.trim();
    }

    @Bindable
    public String getAssetDescription() {
        return newAsset.description;
    }

    public void setAssetSymbol(String s) {
        newAsset.symbol = s.trim();
    }

    public String getAssetExchangeName() {
        return exchange.name;
    }

    void setAssetExchange(Exchange e) {
        exchange = e;
        newAsset.exchangeMarketIdentifierCode = e.marketIdentifierCode;
    }

    @Bindable
    public String getAssetType() {
        return newAsset.assetType.toString();
    }

    @Bindable
    public String getAssetSymbol() {
        return newAsset.symbol;
    }

    @Override
    public PropertyChangeRegistry getCallbacks() {
        return callbacks;
    }
}
