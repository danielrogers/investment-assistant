package com.drogers.investmentassistant.ui.newpurchase.assetsearch;

import android.app.Application;

import com.drogers.investmentassistant.db.AssetExchange;
import com.drogers.investmentassistant.db.RoomDb;
import com.drogers.investmentassistant.db.entity.AssetRepository;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import java.util.List;

import io.reactivex.subjects.UnicastSubject;

public class SearchResultsViewModel extends AndroidViewModel {
    private AssetRepository repository;
    public SearchResultsViewModel(@NonNull Application application) {
        super(application);
        repository = new AssetRepository(application, RoomDb.getDatabase(application).assetDAO());
    }

    /**@see AssetRepository#getAssetFromLocalOrRemote(java.lang.String,com.drogers.investmentassistant.db.entity.exchange.Exchange)
     * @param queryString
     * @return
     */
    UnicastSubject<AssetExchange> getAssetFromLocalOrRemote(String queryString, Exchange exchange) {
        return repository.getAssetFromLocalOrRemote(queryString, exchange);
    }

    public UnicastSubject<List<AssetExchange>> getAllAssetsWithSymbolLocally(String queryString) {
        return repository.getAllAssetsWithSymbolLocally(queryString);
    }

    @Override
    protected void onCleared(){
        repository.onDispose();
    }
}
