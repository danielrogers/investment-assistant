package com.drogers.investmentassistant.ui.newpurchase.step2;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;

import com.drogers.investmentassistant.IABoilerPlateActivity;
import com.drogers.investmentassistant.R;
import com.drogers.investmentassistant.databinding.ActivityNewPurchaseStep2Binding;
import com.drogers.investmentassistant.db.entity.Asset;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;
import com.drogers.investmentassistant.network.images.PicassoAssetImageLoader;
import com.drogers.investmentassistant.ui.Utils;
import com.drogers.investmentassistant.ui.newpurchase.NewPurchaseKeys;
import com.drogers.investmentassistant.ui.newpurchase.step3.NewPurchaseActivityStep3;

import java.time.LocalDate;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

/**
 * Screen where user enters purchase details - price, amount, date, etc
 */
public class NewPurchaseActivityStep2 extends IABoilerPlateActivity implements DatePickerFragment.DatePickerListener{

    ActivityNewPurchaseStep2Binding dataBinding;
    Step2ViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_purchase_step2);
        initData();
        Utils.initialiseToolbar(this, R.string.new_purchase_step_2_details);
        new PicassoAssetImageLoader(dataBinding.companyImageView, viewModel.getAssetBeingPurchased(), getApplication()).loadImage();
    }

    /**
     * Parses asset data from the intent, sets up data binding
     */
    private void initData(){
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_new_purchase_step2);
        Intent intent = getIntent();
        viewModel = ViewModelProviders.of(this).get(Step2ViewModel.class);
        Asset assetBeingPurchased = intent.getParcelableExtra(NewPurchaseKeys.ASSET_EXTRA_KEY);
        Exchange exchangeBeingUsed = intent.getParcelableExtra(NewPurchaseKeys.EXCHANGE_EXTRA_KEY);
        viewModel.initNewPurchase(assetBeingPurchased, exchangeBeingUsed);
        dataBinding.setViewmodel(viewModel);
        dataBinding.setActivity(this);
        dataBinding.buyingLabel.setText(getResources().getString(R.string.new_purchase_step_2_buying, assetBeingPurchased.name));
    }

    public void showDatePickerFragment(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        viewModel.setPurchaseDate(LocalDate.of(year, month, dayOfMonth));
    }

    public void onClickRecordPurchaseButton(View v){
        try{
            viewModel.validatePurchase();
            showConfirmationDialog();
        }catch(Exception e){
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Shows dialog which prompts user to confirm purchase details
     */
    private void showConfirmationDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String text = String.format(getResources().getString(R.string.new_purchase_step_2_are_you_sure),
                viewModel.getAmountBeingPurchasedAsString(),
                viewModel.getAssetBeingPurchased().symbol,
                viewModel.getPurchasingUnitPriceAsString(),
                viewModel.getPurchaseDate());
        DialogInterface.OnClickListener onClickListener = buildDialogOnClickListener();
        builder.setMessage(text)
                .setPositiveButton(getResources().getText(R.string.new_purchase_step_2_are_you_sure_y), onClickListener)
                .setNegativeButton(getResources().getText(R.string.new_purchase_step_2_are_you_sure_n), onClickListener)
                .show();
    }

    private DialogInterface.OnClickListener buildDialogOnClickListener(){
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        Intent i = new Intent(getApplicationContext(), NewPurchaseActivityStep3.class);
                        i.putExtra(NewPurchaseKeys.STEP_3_PURCHASE_EXTRA_KEY, viewModel.purchaseTransaction);
                        i.putExtra(NewPurchaseKeys.STEP_3_ASSET_EXTRA_KEY, viewModel.getAssetBeingPurchased());
                        i.putExtra(NewPurchaseKeys.EXCHANGE_EXTRA_KEY, viewModel.getExchangeBeingUsed());
                        startActivity(i);
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };
    }
}
