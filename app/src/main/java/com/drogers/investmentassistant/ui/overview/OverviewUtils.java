package com.drogers.investmentassistant.ui.overview;

import com.drogers.investmentassistant.db.entity.AssetOwnershipRecord;

import org.apache.commons.lang3.time.DateUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OverviewUtils {
    /**
     *  Groups recordsToMap into a collection of collections, based on the asOfDateField
     * @param recordsToMap Expecting this to be sorted based on date, and not to be empty
     * @return
     */
    public static List<ArrayList<AssetOwnershipRecord>> mapAORecords(List<AssetOwnershipRecord> recordsToMap){
        List<ArrayList<AssetOwnershipRecord>> mappedData = new ArrayList<>();
        LocalDate previousDate = recordsToMap.get(0).asOfDate;
        int i = 0;
        ArrayList<AssetOwnershipRecord> recordsOnCurrentDate = new ArrayList<>();
        while(true){
            AssetOwnershipRecord current = recordsToMap.get(i);
            if(!previousDate.isEqual(current.asOfDate)){
                //otherwise we are on to a new date. Store the current collection of dates
                //and create a new collection for the next set of dates
                if(!recordsOnCurrentDate.isEmpty()) {
                    mappedData.add(recordsOnCurrentDate);
                    recordsOnCurrentDate = new ArrayList<>();
                }
            }
            recordsOnCurrentDate.add(current);
            previousDate = current.asOfDate;
            if(++i == recordsToMap.size()){
                mappedData.add(recordsOnCurrentDate);
                return mappedData;
            }
        }
    }


}
