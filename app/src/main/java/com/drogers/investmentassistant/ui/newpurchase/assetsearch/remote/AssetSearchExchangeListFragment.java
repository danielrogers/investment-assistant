package com.drogers.investmentassistant.ui.newpurchase.assetsearch.remote;


import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.drogers.investmentassistant.IABoilerPlateFragment;
import com.drogers.investmentassistant.R;
import com.drogers.investmentassistant.databinding.FragmentAssetSearchExchangeListBinding;
import com.drogers.investmentassistant.db.entity.exchange.Exchange;

import java.util.List;


public class AssetSearchExchangeListFragment extends IABoilerPlateFragment {

    private FragmentAssetSearchExchangeListBinding dataBinding;
    private ExchangeListAdapter adapter;
    private String searchTerm;

    public static AssetSearchExchangeListFragment newInstance(String searchTerm){
        AssetSearchExchangeListFragment instance = new AssetSearchExchangeListFragment();
        instance.searchTerm = searchTerm;
        return instance;
    }

    public AssetSearchExchangeListFragment() {
    }

    public void setExchanges(List<Exchange> exchanges){
        adapter.setExchangesToDisplay(exchanges);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_asset_search_exchange_list, container, false);
        initialiseRecyclerView();
        return dataBinding.getRoot();
    }


    private void initialiseRecyclerView(){
        RecyclerView recyclerView = dataBinding.exchangesRecyclerView;
        adapter = new ExchangeListAdapter(getContext(), searchTerm);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
    }

}
