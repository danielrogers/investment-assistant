package com.drogers.investmentassistant.ui.newpurchase.step1;

import android.app.Application;

import com.drogers.investmentassistant.db.AssetExchange;
import com.drogers.investmentassistant.db.RoomDb;
import com.drogers.investmentassistant.db.entity.Asset;
import com.drogers.investmentassistant.db.entity.AssetRepository;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import io.reactivex.Maybe;

public class Step1ViewModel extends AndroidViewModel {
    private AssetRepository assetRepository;
    public Step1ViewModel(@NonNull Application application) {
        super(application);
        assetRepository = new AssetRepository(application, RoomDb.getDatabase(application).assetDAO());
    }

    Maybe<List<AssetExchange>> getRecentAssets(){
        return assetRepository.getRecentAssets();
    }
}
