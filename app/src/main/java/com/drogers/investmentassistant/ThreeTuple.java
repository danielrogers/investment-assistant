package com.drogers.investmentassistant;

public class ThreeTuple<T1, T2, T3> {
    public T1 value1;
    public T2 value2;
    public T3 value3;

    public void putValues(T1 x, T2 y, T3 z){
        value1 = x;
        value2 = y;
        value3 = z;
    }
}
